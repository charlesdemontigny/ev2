# JLR01 - Real estate market value estimation

This repository contains an experimental setup used to develop and evaluate house price
estimation models.

<!-- TOC -->

- [Pipeline overview](#pipeline-overview)
- [Getting started](#getting-started)
    - [1. Model selection](#1-model-selection)
        - [Scores](#scores)
    - [2. Operation](#2-operation)
- [List of notebooks and scripts](#list-of-notebooks-and-scripts)
- [Computing house image representations](#computing-house-image-representations)
    - [Environment setup](#environment-setup)
    - [Scripts and notebooks](#scripts-and-notebooks)
    - [[Experimental] Matlab code to compute image features using pre-trained models for image retrieval](#experimental-matlab-code-to-compute-image-features-using-pre-trained-models-for-image-retrieval)
- [Image segmentation](#image-segmentation)
- [Developer guide and project organization](#developer-guide-and-project-organization)

<!-- /TOC -->

## Pipeline overview

![JLR01 pipeline](./docs/figures/pipeline_AB.png)

## Getting started

Prerequisites:

- Install [git](https://git-scm.com/) and **[git LFS](https://git-lfs.github.com/)** (we use git LFS to track datasets in the repository)
- Install the [Miniconda](https://conda.io/miniconda.html) python distribution
  (or [Anaconda](https://www.continuum.io/downloads))

Make sure `git-lfs` is enabled:

```bash
git lfs install
```

Clone the repository, open a command line prompt at the project root directory.

Create the conda `jlr` environment:

```bash
conda env create -f environment.yml
```

Activate the environment:

```bash
source activate jlr  # on linux, MacOS, and git bash on windows
activate jlr  # on windows
```

Run test suite

```bash
pytest  # run from project root to recursively find all test_*.py scripts

# to test a specific module:
# pytest src/tests/test_moduleX.py
```

### 1. Model selection

First make a tidy dataset from DB table exports:

```bash
python src/make_dataset.py new
```

This command-line tool will create the following files:

| Datasets                                       | Description                   |
| ---------------------------------------------- |-------------------------------|
| `data/interim/immeubles_sans_tx.csv`           | Orphan buildings with no tx since 1980s |
| `data/processed/2017-XX-XX/train_2015.feather` | training dataset up to 2015 created on date of parent-folder` |
| `data/processed/2017-XX-XX/test_2015.feather`  | testing dataset up to 2015 created on date of parent-folder` |

Models can then be cross-validated using the `run_eval` tool (run `python src/run_eval.py --help` for usage instructions).

For instance, on the baseline model:

```bash
python src/run_eval.py -m baseline_city_median -j 4
```

See the directory `src/config` for a list of supported model configurations.

#### Scores

Model/evaluation on **10% of the training data**, scores on computed on the evalaution set (2015):

* (evia) : scores computed on 2015 transactions with EVIA reports.
* (all) : scores computed on all 2015 transactions.

| Model                            | MAPE (evia) | RMSLE (evia) | MAPE (all) | RMSLE (all)
| ---------------------------------|------|-------|-------|--------|
| **Evia - Historical reports**    | 12.4% | .226 |  -    |  -
| baseline_city_median             | 43.0% | .643 | 46.9% | .648
| baseline_cityYear_median         | 52.0% | .622 | 56.9% | .638
| baseline_RTA_median              | 36.9% | .563 | 40.0% | .570
| rf_prev_price_only               | 35.9% | .550 | 39.2% | .560
| basic_linear_reg                 | 43.2% | .366 | 193.% | .453
| basic_linear_reg_test_impute     | 35.5% | .375 | 43.5% | .472
| basic_linear_reg_converted_units | 21.6% | .356 | 31.6% | .449
| basic_knn                        | 26.5% | .389 | 29.3% | .419
| basic_rf                         | 17.5% | .236 | 22.8% | .313
| basic_catboost                   | 15.2% | **.211** | 20.7% | .286
| basic_xgboost                    | 14.5% | **.204** | 19.8% | .280
| xgboost_median_price_stacked     | 14.5% | **.203** | 19.6% | .276
| catboost_median_price_stacked    | 15.2% | **.210** | 20.4% | .281
| basic_xgboost_geoknn             | 14.0% | **.198** | 18.8% | .267
| xgboost_final                    | 13.9% | **.195** | 18.8% | .266
| **lgbm_final**                   | 13.2% | **.185** | 18.6% | .261

Using **100%** of training data :

| Model                            | MAPE (evia) | RMSLE (evia) | MAPE (all) | RMSLE (all)
| ---------------------------------|------|-------|-------|--------|
| Evia - Historical reports    | 12.9% | .222 |  -    |  -
| **lgbm_final**                   | **12.7%** | **.182** | 17.1% | .245

### 2. Operation

**The "Operation" part of the pipeline is minimally tested and output house price estimations should be used with caution.**

First make sur you have a **training set** in `./data/processed/2017-11-22` (dataset
created with the `python src/make_dataset.py new` command).

**Train model** on all dataset (train + test):

```bash
python src/train_model.py --dataset 2017-11-22 --model baseline_city_median
```

The model will be stored in `./models/2017-11-22/`.

**Prepare input features** (for all building with a fixed hypothetical transaction date):

```bash
python src/make_dataset.py prepare-input -i ./data/raw/2017-11-22 --target-date 2018-01-01
```

Input features are written in `./data/processed/2017-11-22/input/`.

**Compute predictions**:

```bash
python src/batch_predict.py -i ./data/processed/2017-11-22/input/features_2018-01-01.feather -m ./models/2017-11-22/baseline_city_median__2018-03-06T16-56-27.970294+00-00 -f .csv.bz2
```

Output predictions are written in `./data/processed/2017-11-22/output/`.

## List of notebooks and scripts

| Notebook                       | Description                   |
| ------------------------------ |-------------------------------|
| `0.1_RawData_Exploration.ipynb`| Exploration of immeubles.txt, transaction.txt |
| `0.3_permits_review_2017-11-28.ipynb` | Permits data overview
| `0.4_datashader_visualizations.ipynb` | Building spatial visualizations using Datashader
| `Discrepency in LatLong in raw data` | Quality review of coordinates columns

| Main scripts                        |                               |
| ----------------------------------- |-------------------------------|
| `src/make_dataset.py new`           | Run this script to generate a tidy training/testing dataset from raw data files. |
| `src/run_eval.py`                   | Run cross-validation on the validation set. |
| `src/train_model.py`                | Train model on all available data (train + test) |
| `src/make_dataset.py prepare-input` | Create a feature dataset with hypothetical transactions on a target date, for all building (for final export). |
| `src/batch_predict.py`              | Compute house price estimations using a trained model |


| Utilities                      |                               |
| ------------------------------ |-------------------------------|
| `src/data/merge_tables.py`     | Merge `evia_dispo` and `ind_exclusion` indicators into `immeuble` and `transaction` tables, respectively. |
| `src/data/import_images.py`    | Moves building images inside a simple directory structure (requires a copy of the building image dataset). |

Vision scripts and notebooks are documented in the section
[Computing house image representations](#computing-house-image-representations) below.

---

## Computing house image representations

Extracting features from pre-trained convolutionnal neural nets is an easy way to create
a vector representation of any set of images. By using neural networks trained to label
images (for instance the *imagenet* labelling tasks), we hope that the activations of upper
layers neurons will contain an easy to use, information-rich, high-level representations
of the images. These representations can be used for several tasks, for instances they can
be fed to a regression or classification model. Here, we compute the distance between these
features to create a visual similarity metric.

Here we list the steps required to prepare image batches, extract features using
pre-trained neural nets, compress the computed representations and how they can be
used to build a visual similarity metric.

![Feature extraction pipeline](./docs/figures/image_feature_extraction_pipeline.png)

### Environment setup

We use `pytorch`, `torchvision` and `opencv` packages to manipulate images and
computes image representations. Even if `pytorch` can be used on the CPU, we recommend
using a recent Nvidia GPU with at least 4GB of video memory. Make sure you have an
up-to-date Nvidia driver before proceeding.

On **Linux** or **MacOS** the installation is quite straightforward: we start with
the base `jlr` conda `environment.yml`, then install vision package from the
`environment_vision_gpu_linux.yml` file (or `environment_vision_gpu_linux.yml` for
the CPU version of pytorch):

```bash
conda env create -f environment.yml
conda env update -f environment_vision_gpu_linux.yml
# or: conda env update -f environment_vision_cpu_linux.yml
source activate jlr
```

As of February 2018, there is no official **Windows** `pytorch` release. Fortunately the GitHub user
*peterjc123* maintains unofficial [`pytorch` builds for Windows](https://github.com/peterjc123/pytorch-scripts).
Because of an unresolved [issue](https://github.com/peterjc123/pytorch-scripts/issues/3)
the `pytorch`package must be installed manually:

```bash
conda env create -f environment.yml
conda env update -f environment_vison_gpu_windows_partial.yml

# Download pytorch from: https://anaconda.org/peterjc123/pytorch/0.3.0/download/win-64/pytorch-0.3.0-py36_0.3.0cu80.tar.bz2
activate jlr
conda install pytorch-0.3.0-py36_0.3.0cu80.tar.bz2
pip install torchvision==0.2.0
```

### Scripts and notebooks

Embedded in this repository, the `./data/raw/images/vicky_0.8%_sample/` directory contains
6592 house images, randomly sampled for testing purposes.

By default, all notebooks are configured to use this sample set. To encode the whole dataset
edit the `image_dataset` variable in notebooks.

1. Run the `Vision_step1_Preprocessing.ipynb` notebook.

2. Run the `Vision_step2_Features_Production.ipynb` notebook.

   Alternatively, you can use the `` command line tool:

   ```bash
   python ./src/features_extraction/features_extraction.py \
       --model_id RESNET34 \
       --input_images_path_datasets \
         ./data/interim/images/vicky_0.8%_sample/batches/batch=4096/images_batch_000000.csv \
         ./data/interim/images/vicky_0.8%_sample/batches/batch=4096/images_batch_000001.csv \
       --images_base_directory ./data/raw/images/vicky_0.8%_sample \
       --save_dir ./data/interim/images/vicky_0.8%_sample/features/features_batch=4096_model=RESNET34 \
       --grid_xsize 3 --grid_ysize 3 --bottom_cropping 20 \
       --num_workers 4 --batch_size 10 --gpu_mode
   ```

   On a laptop with a Nvidia Quadro M1200 GPU (4GB memory), encoding 6592 images took 20 minutes.
   With a Nvidia GTX 1050Ti, this should take less than 5 minutes.

3. Run the `Vision_step3_Compute_PCA.ipynb` notebook.

   The `max_samples` controls the number of samples used to compute the PCA.

4. Run the `Vision_step4_Reproject_features.ipynb` notebook.

5. Run the `Vision_step5_Compute_similarity.ipynb` notebook.

Output example:

![image similarity sample](./docs/figures/image_similarity_sample.jpg)

Optionally embedding can be viewed in Tensorboard. Follow instructions in `notebooks/Vision_step6_tensorboard.ipynb`.

![tensorboard](./docs/figures/tensorboard_projector.png)

### [Experimental] Matlab code to compute image features using pre-trained models for image retrieval

We also provide the Matlab code used to compute image features using the process described in
[F. Radenovic, G. Tolias, O. Chum, CNN Image Retrieval Learns from BoW: Unsupervised Fine-Tuning with Hard Examples, ECCV 2016](https://arxiv.org/abs/1604.02426)

Instructions (requires Matlab):

1. Install the matconvnet library side by side with the `./src/features_extraction/siaMAC_matlab` directory: [http://www.vlfeat.org/matconvnet/#obtaining-matconvnet](http://www.vlfeat.org/matconvnet/#obtaining-matconvnet)

2. Compile matconvnet: [http://www.vlfeat.org/matconvnet/install/#compiling](http://www.vlfeat.org/matconvnet/install/#compiling)

3. Use `genere_MAC_features.m` to compute representation for all image on a given directory (by default: `./data/raw/images/vicky_0.8%_sample`).
   
   Computed features will be stored in `features_JLR_rmacvLw_N.mat` with the following layout:
   - features : computed features
   - im_names : image file names
   
4. To compute and visualize image similarities, use `inferenceMultiple.m` or `inferenceOne.m`.

## Image segmentation

We use a pre-trained image segmentation model: [hangzhaomit/semantic-segmentation-pytorch](https://github.com/hangzhaomit/semantic-segmentation-pytorch).

Before computing segmentations, images must be split in batches using
the `Vision_step1_Preprocessing.ipynb` notebook.

Run the `segment_images.py` screen for each batch. For instance, on the batch `images_batch_000000.csv`:

```bash
python src/segmentation/semantic-segmentation/segment_images.py --bsave --images_file_relative_path ./data/interim/images/vicky_0.8%_sample/batches/batch=4096/images_batch_000000.csv
```

Output segmentation and label statistics will be written in the
`./data/interim/images/vicky_0.8%_sample/segmentation/output` directory
(can be overwritten with the `--save_dir` parameter).

![segmentation sample](./docs/figures/segmentation_sample.png)

---

## Developer guide and project organization

- The project structure is a simplified version of the ["Cookiecutter Data Science"](https://drivendata.github.io/cookiecutter-data-science/) model.
  ```txt
      ├── data
      │   ├── raw          <- The original, immutable data dump.
      │   │   └── images   <- JLR building image data (folder hierarchy created with `src/data/import_images.py`)
      │   ├── external     <- Data from third party
      │   ├── interim      <- Intermediate data that has been transformed.
      │   │   └── images   <- computed image batches, features, projections, etc.
      │   └── processed    <- The final, canonical data sets for modelling (one directory per dataset).
      ├── docs             <- Project documentation
      │   ├── reports      <- Notebook HMTL exports.
      │   └── figures      <- Generated graphics and figures to be used in reporting.
      ├── models           <- Trained and serialized models
      ├── notebooks        <- Jupyter notebooks.
      ├── src              <- Source code for use in this project.
      ├── environment.yml  <- Conda environment description
      └── setup.cfg        <- python development and build tool configurations
  ```
  Note: zip, bz2, gz, tsv, txt, csv, parquet and feather files in the `data` directory
  will be tracked by git LFS.

- We follow the ["Git feature branch workflow"](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow).

- To update the conda environment (after editing `environment.yml`) use:
  ```bash
  conda env update -f environment.yml
  ```

- We use `nbstripout` to automatically clean notebooks (strip the output) for easier reviewing. Inside the repository, use the following command to setup `nbstripout` as a git filter:
  ```bash
  nbstripout --install
  ```

- We maintain HTML exports of notebooks in the `docs/reports` directory. Use `jupyter nbconvert` to export notebooks:
   ```bash
   jupyter nbconvert --to html --output-dir docs/reports/ notebooks/<00_notebook>.ipynb
   ```

- We follow the [*PEP8* style guide](https://www.python.org/dev/peps/pep-0008/), use `autopep8` to automatically format python scripts:
  ```bash
  autopep8 --recursive --in-place --aggressive --aggressive ./
  ```

- We use `flake8` (lightweight python linter) to check for bugs and style mismatches:
  ```bash
  flake8 .
  ```

- To keep a consistent package import ordering we use `isort` to automatically reorder imports:
  ```bash
  isort -rc .
  ```
