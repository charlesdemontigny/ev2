# -*- coding: utf-8 -*-
"""
Created on 22 dev. 2017 at 11h39
@author: beaulima
"""
import os

def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path


def get_naming_from_path(file_path):

    """
    returns the subdirname and the basename of the file path
    :param file_path:
    :return:
    """

    basename = os.path.basename(file_path).split('.')[0]
    subdir = os.path.basename(os.path.dirname(file_path))
    return subdir, basename