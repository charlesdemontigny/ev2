import pandas as pd
import sys

sys.path.append("src")

from make_dataset import load_data_from_Oracle

def to_parquet():
    """Create parquet file from make_dataset for development with CRIM"""
    connection = ['jlr_website', '1qaz2wsx', 'jlrorap1relv']
    input_train = load_data_from_Oracle(connection, 'input_train')
    input_pred = load_data_from_Oracle(connection, 'input_pred')
    connection_geo = ['jlrprod', 'rlj', 'jlrorap1relv']
    geo = load_data_from_Oracle(connection_geo, 'geo')
    # Export to parquet
    input_train.to_parquet('data/input_train.parquet.gzip',compression='gzip', index = False)
    input_pred.to_parquet('data/input_pred.parquet.gzip',compression='gzip',
                            index = False, engine = 'fastparquet')
    geo.to_parquet('data/geo.parquet.gzip',compression='gzip', index = False)
    print("Terminé!")

if __name__ == "__main__":
    to_parquet()
