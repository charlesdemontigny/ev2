
import pandas as pd
import numpy as np


def parser_best_measure(df, cat_col, threshold = 1000):
    """Parse args of best_measure"""
    if not isinstance(df, pd.DataFrame):
        raise TypeError('df must be a pandas DataFrame.')
    if cat_col not in df.columns:
        raise ValueError("{} must be a columns of the pandas DataFrame".format(cat_col))
    if (not isinstance(threshold, int)) or threshold < 0:
        raise TypeError("threshold must be a integer and > 0.")


def create_geo_catbat(df):
    """Create a variable that is a mix between geographic variable and catbat"""
    geo_dict = {'ville':'MUN_NO_BSQ', 'dr':'DRIDU', 'ra':'MUN_NO_RA'}
    for key, value in geo_dict.items():
        df['catbat_{}'.format(key)] = df['IMM_CAT_BAT'] + '_' + df[value].astype(str)
    return df, geo_dict


def median_errors(df, cat_col, threshold = 1000):
    """
    Compute best median error measure for each HOME_ID. Best is define by the
    smallest geographical region that have at least ``threshold`` transactions
    in the past 2 years. If no region meets the criteria, the largest region is
    selected.

    Args:
        df: A pandas DataFrame with ``cat_col`` and every ``geo_col`` as columns.
        cat_col: The columns that contains the ``Categorie_Batiment``.
        geo_dict: The columns that contains all the geographic variables.
        threshold: The minimum value to keep a median error on.

    Returns: A pandas DataFrame with a HOME_ID columns and a best_measure columns.
    """
    # Parse args
    parser_best_measure(df, cat_col, threshold)

    # Create region/catbat variables
    df, geo_dict = create_geo_catbat(df)

    # Compute absolute percentage error with pred and true
    ape = np.abs((df.pred - df.y_true)/df.y_true)
    df['ape'] = ape

    # Compute median error by region/catbat and merge it to the main df
    error_per_geo = []
    for key, value in geo_dict.items():
        catbat_geo = 'catbat_{}'.format(key)
        tmp_df = (df[[catbat_geo, 'ape']].groupby([catbat_geo]).agg(['median', 'count']))['ape']
        tmp_df.reset_index(inplace = True)
        tmp_df = tmp_df.rename(columns={'catbat_{}'.format(key): 'geo_catbat'})
        error_per_geo.append(tmp_df)

    median_errors = pd.concat([error_per_geo[0], error_per_geo[1], error_per_geo[2]])
    return median_errors


def fork(pred, median_errors, geo):
    """
    Merge the best error measure DataFrame to the prediction DataFrame in order
    to compute the fork.

    Args:
        pred: The prediction DataFrame.
        best_measure: The pandas DataFrame that contains median error per property.

    Returns: A pandas DataFrame with the prediction interval added.
    """
    # Add geographic variables
    df = pd.merge(pred, geo, how = 'left', left_on = 'HOME_ID', right_on = 'IMM_NO_ID_UEF')

    # Create region/catbat variables
    df, geo_dict = create_geo_catbat(df)

    # Merge pred and median errors based on each geo_catbat
    for key, value in geo_dict.items():
        catbat_geo = 'catbat_{}'.format(key)
        df = pd.merge(df, median_errors, how = 'left', left_on = catbat_geo,
                                                        right_on = 'geo_catbat')
        df = df.rename(columns={'median': 'median_{}'.format(key),
                                                'count':'count_{}'.format(key)})
    #Choose best measure
    best_measure = df['median_ra'].copy()
    best_measure[df['count_dr'] >= 1000] = df['median_dr'].copy()
    best_measure[df['count_ville'] >= 1000] = df['median_ville'].copy()
    df['error'] = best_measure

    # Prediction interval
    df['lowerbound'] = df['estimation'] - df['estimation'] * df['error']
    df['upperbound'] = df['estimation'] + df['estimation'] * df['error']

    # Drops unncessary columns
    columns_to_drop = ['IMM_NO_ID_UEF','IMM_CAT_BAT','MUN_NO_BSQ','DRIDU',
                        'MUN_NO_RA','catbat_ville','catbat_dr','catbat_ra',
                        'geo_catbat_x','geo_catbat_y','geo_catbat',
                        'median_dr', 'median_ra', 'median_ville', 'count_ville',
                        'count_dr', 'count_ra', 'error']
    df.drop(columns_to_drop, axis = 1, inplace = True)

    return df


def confidence_score(df, thresholds):
    """
    Compute a confidence score of the estimation based on the median error of the
    choosen region/catbat pair.

    Args:
        df: A pandas DataFrame with the estimation for every building.

    Raises: ValueError if ``upperbound`` is not in df columns.

    Returns: Export a pandas DataFrame with ``confidence_score`` column added.
    """
    # Raise error
    if 'upperbound' not in df.columns:
        raise ValueError("``upperbound`` must be in the DataFrame.")
    if not isinstance(thresholds, list):
        raise TypeError("``thresholds`` must be a list.")
    if len(thresholds) != 5:
        raise ValueError("Length of the ``thresholds`` arg is {} and it must be 5.".format(len(thresholds)))

    # Generate error variable
    df = df.assign(error = (df['upperbound'] - df['estimation'])/df['estimation'])

    # Split DataFrame based on error
    top99 = df.query('error <= {}'.format(thresholds[0])).copy()
    top99['confidence_score'] = 99

    top90 = df.query('error > {0} & error <= {1}'.format(thresholds[0], thresholds[1])).copy()
    top90['confidence_score'] = (90 + top90.error.rank(pct = True) * 10 - 1).astype(int)

    top80 = df.query('error > {0} & error <= {1}'.format(thresholds[1], thresholds[2])).copy()
    top80['confidence_score'] = (80 + top80.error.rank(pct = True) * 10 - 1).astype(int)

    top70 = df.query('error > {0} & error <= {1}'.format(thresholds[2], thresholds[3])).copy()
    top70['confidence_score'] = (70 + top70.error.rank(pct = True) * 10 - 1).astype(int)

    top60 = df.query('error > {0} & error <= {1}'.format(thresholds[3], thresholds[4])).copy()
    top60['confidence_score'] = (60 + top60.error.rank(pct = True) * 10 - 1).astype(int)

    bottom59 = df.query('error > {}'.format(thresholds[4])).copy()
    bottom59['confidence_score'] = (20 + bottom59.error.rank(pct = True) * 40).astype(int)

    output_df = pd.concat([top99, top90, top80, top70, top60, bottom59])
    output_df.drop('error', axis = 1, inplace = True)

    return output_df
