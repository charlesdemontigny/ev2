import os
import fnmatch
import time
import pandas as pd
import argparse

# Application basic description
APPLICATION_DESCRIPTION = 'Generate path list files'

parameters_test = {

    'input_directory_path': './data/raw/images/vicky_0.8%_sample',
    'output_directory_path': './data/interim/images/vicky_0.8%_sample/batches2',
    'match_pattern': '*.jpg'
}

DO_TEST = False


def parse_args():
    '''
    Parse arguments
    :return arguments
    '''

    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--input_directory_path', type=str,
                        default='./data/raw/images/vicky_0.8%_sample',
                        help='Path to input images')
    parser.add_argument('--output_directory_path', type=str,
                        default='./data/interim/images/vicky_0.8%_sample/batches2',
                        help='Output directory path')
    parser.add_argument('--match_pattern', type=str,
                        default='*.jpg',
                        help='Filter match pattern')

    return parser.parse_args()


def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)
    return directory_path


def generate_path_files(parameters):
    """
    List the input directory to retrieve all files that match the filter pattern.  Dave the result in
    a CSV file named jlr_images_file_paths.csv.
    :param parameters:
    :return:
    """

    if DO_TEST:
        parameters = parameters_test

    print(parameters)
    col1 = []
    col2 = []
    for directory_path, dirs, file_paths in os.walk(parameters['input_directory_path']):
        sub_directory_name = os.path.basename(directory_path)
        file_paths_list = []
        for file_path in file_paths:
            if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                relative_path = os.path.join(os.path.basename(directory_path), file_path)
                #print('%s (%s)' % (relative_path, parameters['output_directory_path']))
                file_paths_list.append(relative_path + '\n')
                col2.append(sub_directory_name)
                col1.append(os.path.basename(file_path).split('.')[0])
        n_files = len(file_paths_list)
        print('Number of files: %i (%s) ' % (n_files, directory_path))
        if n_files > 0:
            file_paths_list.sort()

    cols = sorted(zip(col1, col2))

    df = pd.DataFrame(data=cols, columns=['IMAGE_ID', 'SUBDIR'])
    path_or_buf = os.path.join(parameters['output_directory_path'], 'jlr_images_file_paths.csv')
    print(df)
    print(path_or_buf)
    force_create_directory(parameters['output_directory_path'])
    df.to_csv(path_or_buf=path_or_buf, mode='w')


# Entry point
if __name__ == '__main__':

    start = time.time()

    print('----- %s' % APPLICATION_DESCRIPTION)

    args = parse_args()
    parameters = vars(args)
    ################################
    generate_path_files(parameters)
    ################################
    end = time.time()

    print('----- Elapsed time: %i secs' % (end - start))
