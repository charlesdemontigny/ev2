import os
import argparse
import time
import pandas as pd


# Application basic description
APPLICATION_DESCRIPTION = 'Generate batch images file path'


def parse_args():
    """
    Parse arguments
    :return arguments
    """

    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--input_images_path_dataset',
                        type=str,
                        default='',
                        required=True,
                        help='Path to the CVS images path file')
    parser.add_argument('--output_directory_path',
                        type=str,
                        default='',
                        required=True,
                        help='Output directory path')
    parser.add_argument('--batch_size',
                        type=int,
                        default='',
                        required=True,
                        help='Batch size')
    parser.add_argument('--prefix',
                        type=str,
                        default='images',
                        help='Prefix to add to the output file name')


    return parser.parse_args()


def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path


def generate_batch_files_path(parameters):

    """
    Partition the main images path file in n batches file with the same structure.
    """

    print(parameters)
    batch_size = parameters['batch_size']

    force_create_directory(parameters['output_directory_path'])
    fn = parameters['input_images_path_dataset']
    df = pd.read_csv(filepath_or_buffer=fn, usecols=['IMAGE_ID',  'SUBDIR'],
                     dtype={'IMAGE_ID': object, 'SUBDIR': object})

    k = 0
    for i in range(0, df.shape[0], batch_size):
        i0 = i
        i1 = i + batch_size
        print(i0, i1)
        if i1 > df.shape[0]:
            i1 = df.shape[0]
        sub_df = df[i0:i1]
        print(sub_df)
        path_or_buf = os.path.join(parameters['output_directory_path'],
                                   '%s_batch_%06d.csv' % (parameters['prefix'], k))

        print(path_or_buf)
        sub_df.to_csv(path_or_buf=path_or_buf, mode='w')
        k += 1


# Entry point
if __name__ == '__main__':

    start = time.time()

    print('----- %s' % APPLICATION_DESCRIPTION)

    args = parse_args()
    parameters = vars(args)
    ################################
    generate_batch_files_path(parameters)
    ################################
    end = time.time()

    print('----- Elapsed time: %i secs' % (end - start))
