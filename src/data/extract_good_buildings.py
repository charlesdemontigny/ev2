#!/usr/bin python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 10:02:57 2017
tstamp=20171221-4pm
@author: savardmi
"""
import sys
import os
import pandas as pd
import numpy as np
from datetime import datetime
import time

sys.path.append("./src")

from var import ignored_dataset_stumps, rename_columns, ignored_columns


def import_datasets(data_path, datasets=[]):
    """
    loads all relevant datasets from the given directory and delivers in a Dict

    Parameters
    ----------
    data_path:
        relative path of the dataset files stored as either .txt
        or compressed with .bz2
    dataset:
        list of filenames to load.  If none is provided, all files will be
        loaded *except those found in the ignored_dataset_stumps*

    Returns
    -------
    Dict of pandas.DataFrame
        Dict of DataFrame where the key is the file name

    """

    # searches the data_path to extract all relevant datasets
    filename_list = {f[:f.find(".")]: f for f in os.listdir(data_path) if f[-4:] in ['.txt', ".bz2"]}

    # default case where no stumps are provided
    if not datasets:
        datasets = [ds for ds in filename_list.keys() if ds not in ignored_dataset_stumps]

    dfs = {}

    # import each dataset of interest
    for stump in datasets:
        # don't load if already present
        if stump not in dfs.keys():
            if filename_list[stump].endswith(".bz2"):
                df_in = pd.read_csv(data_path
                                    + "/{}.txt.bz2".format(stump),
                                    dtype=str,
                                    sep='~',
                                    header=0,
                                    encoding='ISO-8859-1',
                                    compression='bz2')
                # cleaning process
                dfs[stump] = df_in[df_in.columns.difference(
                    ignored_columns[stump])
                ].rename(mapper=rename_columns[stump], axis=1)

            else:
                df_in = pd.read_csv(data_path
                                    + "/{}.txt".format(stump),
                                    sep='~',
                                    header=0,
                                    encoding='ISO-8859-1')
                dfs[stump] = df_in[df_in.columns.difference(
                    ignored_columns[stump])
                ].rename(mapper=rename_columns[stump], axis=1)
        else:
            print("dataset '{}' has already been loaded previously".format())
    return dfs


def preprocess_immeuble(imm):
    """
    Parameters
    ----------
    imm: pandas.DataFrame
        raw dataset of buildings with sparse details describing the building
        the Unique identifier of the buildings 'Home_ID' should be used to
        merge with other datasets.

    Returns
    -------
    pandas.DataFrame
        building dataset with Home_ID column cast to int

    """
    imputer = {'Annee_Const': 0,
               'N_commerces': 0,
               'N_etages': 0,
               'Superficie': 0.0,
               'Code_Utilisation': 0}
    imm.fillna(value=imputer, inplace=True)

    imm[['Home_ID',
         "Annee_Const",
         'Code_Utilisation',
         'N_etages']] = imm[['Home_ID',
                             "Annee_Const",
                             'Code_Utilisation',
                             'N_etages']].astype(np.int64)

    return imm[(imm.Annee_Const > 1900) &
               (imm.Annee_Const < 2018) &
               (imm.Code_Utilisation < 1380) &
               (imm.N_etages <= 50)][['Home_ID', 'Code_Utilisation_txt', 'Nb_Logements']]


def preprocess_transaction(tx):
    """
    Light pre-processing of the transaction raw dataset

    Parameters
    ----------
    tx: pandas.DataFrame
        raw dataset of transactions containing at least information on the transaction ID, the building ID and the
        'Home_ID', 'Tx_ID', 'Date_Tx'
    Returns
    -------
    pandas.DataFrame
       A tidy dataset
    """
    print("preprocessing of transactions dataset with shape :", tx.shape)
    imputer = {'Vente_Intra_Famille': 0,
               'Vente_Multiple': 0,
               'Montant': 0,
               'Comptant': 0,
               'Date_Tx': 10000101,
               'Date_Created': 19700101,
               'Date_Modified': 19700101}

    tx.fillna(value=imputer, inplace=True)
    tx['Date_Tx'] = pd.to_datetime(tx.Date_Tx, format='%Y%m%d')
    tx['Date_Created'] = pd.to_datetime(tx.Date_Created, format='%Y%m%d')
    tx['Date_Modified'] = pd.to_datetime(tx.Date_Modified, format='%Y%m%d')
    tx[['Comptant', 'Tx_ID', 'Montant']] = tx[['Comptant', 'Tx_ID', 'Montant']].astype(float)
    tx[['Tx_ID', 'Home_ID', 'Vente_Intra_Famille', 'Vente_Multiple']] = tx[[
        'Tx_ID', 'Home_ID', 'Vente_Intra_Famille', 'Vente_Multiple']].astype(np.int64)

    print("transaction raw dataset", tx.shape)
    print(tx.loc[:20, ['Vente_Type', 'Vente_Intra_Famille', 'Vente_Multiple', 'Montant', 'Home_ID']])
    return tx[(tx.Vente_Type == 'VENTE') &
              (tx.Vente_Intra_Famille == 0) &
              (tx.Vente_Multiple == 0) &
              (tx.Montant > 50000)]


def preprocess_eval(ev):
    """
    Parameters
    ----------
    ev: pandas.DataFrame

    Returns
    -------
    pandas.DataFrame
       A tidy dataset
    """
    imputer = {'Date_Cree': "19000101"}
    ev.fillna(value=imputer, inplace=True)

    ev['Date_Eval'] = pd.to_datetime(ev.Date_Cree, format='%Y%m%d')
    ev.fillna(0)

    ev[["Eval_Valeur_Batisse", "Eval_Valeur_Lot"]] = ev[["Eval_Valeur_Batisse", "Eval_Valeur_Lot"]].astype(float)
    ev['Home_ID'] = ev['Home_ID'].astype(np.int64)
    return ev


def merge_datasets(dfs):
    """
    Merge immeuble, image annotations and transactions.

    Parameters
    ----------
    dfs : Dict[pandas.DataFrame]
       A dictionary of pandas.DataFrames that we wish to merge together
       we expect the following keys : 'transaction', 'immeuble' and 'annotation_photo'

    Returns
    -------
    pandas.DataFrame
       A tidy dataset
    """

    buildings = dfs['immeuble'].set_index('Home_ID').sort_index()
    transactions = dfs['transaction'].set_index('Home_ID').sort_index()
    #annotations = dfs['annotation_photo'].set_index('Home_ID').sort_index()
    print("trnasaction shape during merge_Dataset: ", transactions.shape)
    # Building - transaction left outer join:
    merged = buildings.join(transactions[['Tx_ID', 'Date_Tx',  'Montant']], how='left')

    # There are several +400k buildings without a transaction, these will be kept separate:
    is_without_tx = merged.Tx_ID.isna()
    imm_with_no_tx = merged[is_without_tx]
    print("Shape of the orphan buildings: ", imm_with_no_tx.shape)
    merged = merged[~is_without_tx]

    # Merge building with image annotations :
    #merged = merged.join(annotations, how='left')

    return merged.reset_index()


def make_dataset(input_path, output_path):
    """
    Loads and merges the raw datasets found in input_path
    This directory should contain .bz2 or .txt files
    The filenames should remain constant, otherwise the variables in var.py
    should be modified accordingly.
    """

    # Import datasets found in the folder of interest
    d = import_datasets(input_path)

    # Pre-Processing the raw datasets
    imm_pp = preprocess_immeuble(d['immeuble_merged'])
    tx_pp = preprocess_transaction(d['transaction_merged'])
    #annot_pp = preprocess_annotations(d['annotation_photo'])
    print("shapes after preprocessing of imm and tx ", imm_pp.shape, tx_pp.shape)

    # merging the first 3 datasets
    mgd = merge_datasets({'transaction': tx_pp,
                          'immeuble': imm_pp})  # , 'annotation_photo': annot_pp
    print("length of merged dataset is : ", mgd.shape)
    mgd.to_csv("./data/interim/good_buildings_for_visi.csv")
    return


if __name__ == "__main__":

    start = time.time()

    now = datetime.now()
    args = sys.argv[1:]

    if len(args) > 0:
        input_path = args[0]
        output_path = args[1]
    else:
        input_path = './data/raw/2017-11-22'
        output_path = './data/processed/{}/'.format(now.strftime("%Y-%m-%d"))

    # make sure the output folder exists
    if now.strftime("%Y-%m-%d") not in (os.listdir(path="./data/processed/")):
        os.mkdir(output_path)

    # run
    make_dataset(input_path, output_path)

    end = time.time()

    print('----- Elapsed time: %i secs' % (end - start))
