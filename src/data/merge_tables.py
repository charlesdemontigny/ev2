#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from pandas.util.testing import assert_frame_equal


def merge_table(lhs_file: str, rhs_file: str, lhs_key: str, rhs_key: str,
                output_file: str, encoding: str, sep: str, compression: str):
    """Merge two csv tables (left-join) and save the result in a same format."""

    print(f"merging '{lhs_file}' with '{rhs_file}' into '{output_file}'...")

    lhs = pd.read_csv(lhs_file, dtype='str', encoding=encoding, compression=compression, sep=sep)
    rhs = pd.read_csv(rhs_file, dtype='str', encoding=encoding, compression=compression, sep=sep)

    rhs_duplicate_keys = rhs[rhs_key].duplicated(keep='first')

    if rhs_duplicate_keys.sum() > 0:
        print(f"Warning: dropping {rhs_duplicate_keys.sum()} ({rhs_duplicate_keys.mean() * 100}%)"
              f"duplicated keys in '{rhs_file}'")
        rhs = rhs[~rhs_duplicate_keys]

    result = lhs.merge(rhs, how='left', left_on=lhs_key, right_on=rhs_key)

    # Drop RHS key to avoid having two key columns:
    if (lhs_key != rhs_key):
        result = result.drop(columns=rhs_key)

    assert_frame_equal(result[lhs.columns], lhs)

    result.to_csv(output_file, encoding=encoding,
                  compression=compression, sep=sep, index=False)


if __name__ == '__main__':

    sep = '~'
    encoding = 'ISO-8859-1'  # Latin-1
    compression = 'bz2'

    merge_table(
        lhs_file='./data/raw/2017-11-22/immeuble.txt.bz2',
        rhs_file='./data/raw/2017-11-22/evia_dispo.txt.bz2',
        lhs_key='PRO_NO_ID_UEF',
        rhs_key='IMM_NO_ID_UEF',
        output_file='./data/raw/2017-11-22/immeuble_merged.txt.bz2',
        sep=sep,
        compression=compression,
        encoding=encoding)

    merge_table(
        lhs_file='./data/raw/2017-11-22/transaction.txt.bz2',
        rhs_file='./data/raw/2017-11-22/ind_exclusion.txt.bz2',
        lhs_key='NUMERO',
        rhs_key='TRA_NUMERO',
        output_file='./data/raw/2017-11-22/transaction_merged.txt.bz2',
        sep=sep,
        compression=compression,
        encoding=encoding)
