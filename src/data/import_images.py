#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Moves building image files into a standard directory structure.
"""

import os
import os.path
import shutil

import pandas as pd
import numpy as np
from tqdm import tqdm

input_directory = './VICKY/'  # flat directory with building images
ouptut_directory = './raw/images/vicky'  # structured output directory

nb_digits = 10
nb_digit_per_directory = 4

if __name__ == "__main__":

    df = pd.DataFrame({"filename": os.listdir(input_directory)})

    print("Found {:,} files".format(len(df)))

    assert df.filename.str.endswith('.jpg').all()

    name_and_ext = df.filename.str.rsplit('.', 1).str
    df['file_id'] = name_and_ext[0].astype(int)
    df['file_ext'] = name_and_ext[1]

    assert df['file_id'].is_unique

    assert nb_digits >= int(np.ceil(np.log10(df['file_id'].max())))

    df['file_id_padded'] = df['file_id'].astype('str').str.zfill(nb_digits)
    df['file_path_padded'] = df['file_id_padded'] + '.' + df['file_ext']
    df['directory'] = df['file_id_padded'].str[:-nb_digit_per_directory]

    print("Making {} sub-directories in {}...".format(df['directory'].nunique(), ouptut_directory))

    for directory in df['directory'].unique():
        os.makedirs(os.path.join(ouptut_directory, directory), exist_ok=True)

    print("Moving {} files in {} directories...".format(len(df), df['directory'].nunique()))

    for row in tqdm(df[['directory', 'filename', 'file_path_padded']].itertuples(), total=len(df)):
        src = os.path.join(input_directory, row.filename)
        dst = os.path.join(ouptut_directory, row.directory, row.file_path_padded)
        # print("Moving {} to {}".format(src, dst))
        shutil.move(src, dst)
