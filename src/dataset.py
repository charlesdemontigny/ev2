# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import click


def load_dataset(path, target_column='MONTANT'):
    '''Loads a dataset feather file. Returns a feature dataframe, X,
    and target array, y.

    This function drops some unused columns. Columns that may leak part
    of the target (additionnal information on future transactions, not
    available at runtime) should be dropped here. For instance: `Comptant`,
    uniques ids, 'tx_exclue' may reveal some information on transaction prices.
    '''

    df = pd.read_feather(path)

    # Columns to drop : for diagnostic only, duplicated columns, target leaks, etc.
    # meta_data_columns = ['Arrondissement_Nom', 'CAT_BAT_DESC', 'Code_Utilisation_txt',
    #                      'DESC_VENTE_TAXABLE', 'Matricule', 'UNIQUE_ID', 'Tx_Type_Desc',
    #                      'EVIADISPO', 'Ville', 'DESCRIPTION_CLASSE', 'tx_exclue', 'IDUEF_NB',
    #                      'PHOT_AVEC_VISAGE', 'Lot_ID', 'Cadastre', 'Comptant']

    # df.drop(columns=meta_data_columns, inplace=True)

    print(f'Initial dataset: {df.shape[0]} samples, {df.shape[1]} columns.')

    X = df.drop(columns=target_column)
    y = df[target_column].values

    assert len(y) == len(X)
    assert y.ndim == 1

    return X, y

def prepare_dataset(df, target_column = "MONTANT"):
    '''
    Takes a unique DataFrame. Returns a feature dataframe, X,
    and target array, y.
    '''

    print(f'Initial dataset: {df.shape[0]} samples, {df.shape[1]} columns.')

    X = df.drop(columns=target_column)
    y = df[target_column].values

    assert len(y) == len(X)
    assert y.ndim == 1

    return X, y

def filter_dataset(X, y, low_prices_filter=25000,
                   drop_multiple_house_sales=True,
                   drop_within_family_sales=True,
                   drop_tx_before_year=None,
                   use_code_threshold=1380):
    '''Filters out transactions and buildings to ignore.
    This filter should be used both for training and
    evaluating regression performances.'''

    # Drop transaction with NaN prices:
    y_nan = np.isnan(y)
    print(f'Dropping {y_nan.sum()} samples ({y_nan.mean():.2%}) with NaN price')
    X = X[~y_nan]
    y = y[~y_nan]

    # Drop transactions with low prices:
    if low_prices_filter:
        y_too_low = y < low_prices_filter
        print('Dropping {} samples ({:.2%}) with price < {}'.format(
            y_too_low.sum(), y_too_low.mean(), low_prices_filter))
        X = X[~y_too_low]
        y = y[~y_too_low]

    # Drop multiple houses sales
    if drop_multiple_house_sales:
        multiple_sales = X['VENTE_MULTIPLE'] != 0
        print('Dropping {} samples ({:.2%}) with `Vente_Multiple` flag not set to 0'.format(
            multiple_sales.sum(), multiple_sales.mean()))
        X = X[~multiple_sales]
        y = y[~multiple_sales]

    # Drop "within family sales"
    if drop_within_family_sales:
        within_family = X['VENTE_INTRA_FAMILLE'] != 0
        print('Dropping {} samples ({:.2%}) with `Vente_Intra_Famille` flag not set to 0'.format(
            within_family.sum(), within_family.mean()))
        X = X[~within_family]
        y = y[~within_family]

    # Drop invalid buildings
    # if use_code_threshold:
    #     invalid_bldg = X['CODE_UTILISATION'] > use_code_threshold
    #     print('Dropping {} samples ({:.2%}) with `Code_Utilisation` above {} (non-residences)'.format(
    #         invalid_bldg.sum(), invalid_bldg.mean(), use_code_threshold))
    #     X = X[~invalid_bldg]
    #     y = y[~invalid_bldg]

    # Drop invalid buildings
    if drop_tx_before_year:
        old_tx = X['DATE_TX'] < pd.datetime(drop_tx_before_year, 1, 1)
        print('Dropping {} samples ({:.2%}) with `Date_Tx` before {}'.format(
            old_tx.sum(), old_tx.mean(), drop_tx_before_year))
        X = X[~old_tx]
        y = y[~old_tx]

    return X, y


def sample_dataset(X: pd.DataFrame, y: np.ndarray, proportion, seed, strat_cols=['VILLE_BSQ']):
    """Sample a random subset of the dataset."""

    assert len(X) == len(y)
    assert (proportion >= 0.) and (proportion <= 1.)
    assert len(strat_cols) > 0
    # print("Speed-up Shortcut the sample_dataset by filtering for single city!!! ;)")
    # ville_bsq = 72005
    # return X[X.Ville_BSQ == ville_bsq], y[X.Ville_BSQ == ville_bsq]

    np.random.seed(seed)

    # Sample withing each strats:
    idx = X[strat_cols].copy().reset_index(drop=True).rename_axis('indices')
    # Note: when grouping on categorical variables, we must handle empty groups
    # int the groupby aggregation function.
    idx = idx.groupby(strat_cols).apply(
        lambda g: g.sample(frac=proportion) if len(g) > 0 else pd.DataFrame())
    idx = idx.index.get_level_values('indices').values

    # Suffle all selected indices:
    np.random.shuffle(idx)

    # If the strats are smalls, the real proportion may not match exactly the desired proportion:
    sampling_tolerance = 0.0001
    assert np.abs(len(idx) - len(X) * proportion) <= np.ceil(sampling_tolerance * len(X))

    X_sampled = X.iloc[idx].reset_index(drop=True)
    y_sampled = y[idx]

    assert X_sampled.columns.equals(X.columns)

    # Check number of sample per strat
    original_props = X.groupby(strat_cols).size().rename('orignal')
    sampled_props = X_sampled.groupby(strat_cols).size().rename('sampled')
    joined_props = original_props.to_frame().join(sampled_props).fillna(0)
    missing_strats = (joined_props.sampled < 1).sum()
    if missing_strats > 0:
        click.secho(f"Warning: {missing_strats} strats ({strat_cols}) with no samples.", fg='red')

    return X_sampled, y_sampled
