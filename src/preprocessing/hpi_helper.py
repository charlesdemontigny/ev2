# Functions -------------------------------------------------------------------
import numpy as np
import pandas as pd
import os
import cx_Oracle
import click
import statsmodels.api as sm

# Load dataset
def load_dataset():
    requete = """select TRA_TRANSACTION.TRA_MNT_TRANSACTION, TRA_TRANSACTION.TRA_DT_PUBLIE, TRA_TRANSACTION.TRA_ID_UEF, TRA_TRANSACTION.TRA_CAT_BAT, AD, DRIDU, SDRIDU, REIDU
                from tra_transaction
                join imm_immeuble on TRA_TRANSACTION.TRA_ID_UEF = IMM_IMMEUBLE.IMM_NO_ID_UEF
                join census.correspondance_ad_2011 on AD = ADIDU
                where tra_type=4
                and tra_cat_bat in ('2A', '3C', '2B', '3B')
                and to_char(tra_dt_publie,'YYYYmmdd')> '20070101'
                and to_char(tra_dt_publie, 'YYYYmmdd') <= '20171231'
                and TRA_IND_VENTE_MULTIPLE=0
                and TRA_IND_VENTE_REPRISE=0
                and TRA_IND_VENTE_LIE=0
                and TRA_MNT_TRANSACTION > 50000"""

    connection = cx_Oracle.connect( "jlrprod", 'rlj','azuorap3', encoding = 'UTF-8')

    click.secho(f"\nDebut du telechargement des donnees a partir de Oracle...", bold=True)
    return pd.read_sql(requete.encode('utf-8'), con = connection)

# cleanSQLoutput
def cleanSQLoutput(df, id_name = "TRA_ID_UEF",
                   datevar = "TRA_DT_PUBLIE"):

    idd = df[id_name][df.duplicated(id_name)]
    df = df[df[id_name].isin(idd)]

    id_ = df[id_name]
    year = df[datevar].astype(str).str.slice(start=0, stop=4).astype(str)
    month = pd.Series(
            year + "-" +
            df[datevar].astype(str).str.slice(start=5, stop=7).astype(str) +
            "-01")

    result =  pd.DataFrame({'id_': id_,
                         'date' : df.TRA_DT_PUBLIE,
                         'price' : df.TRA_MNT_TRANSACTION,
                         'year' : year,
                         'month' : month})

    result.reset_index(inplace = True)

    #result = result[(result.price >= minprice)]

    return(result)



# repsaledata
def repsaledata(id_, price, timevar, priceLog = True):

    """ Cette fonction transforme un dataset avec une vente par ligne a un dataset de ventes pairees"""

    df = pd.DataFrame({"id_":id_, "time": timevar, "price":price})
    df = df[['id_', 'time', 'price']]
    df = df.sort_values(by = ['id_', 'time'])
    df.reset_index(inplace = True)

    if priceLog:
        df.price = np.log(df.price)

    n = df.shape[0]

    data0 = df.loc[np.arange(0, n-1), ]
    data0.reset_index(inplace = True)
    data1 = df.loc[np.arange(1, n), ]
    data1.time.sort_index(inplace = True)
    data1.reset_index(inplace = True)

    rsale = (data0.id_ == data1.id_) & (data0.time < data1.time)

    data0 = data0[rsale]; data1 = data1[rsale]

    return pd.DataFrame({'id_':data1.id_,
                         'time0' : data0.time,
                         'time1' : data1.time,
                         'price0' : data0.price,
                         'price1' : data1.price})

# design_matrix
def design_matrix(df):
   time0 = np.array(df.time0, dtype = pd.Series)
   time1 = np.array(df.time1, dtype = pd.Series)
   timevar = np.unique(np.concatenate((time0, time1), axis = 0))
   nt = len(timevar)
   n = df.shape[0]
   xmat = np.zeros((n, nt - 1))
   for j in range(1, n):
       xmat[j, time1[j] == timevar[1:(np.shape(timevar)[0]+1)]] = 1
       xmat[j, time0[j] == timevar[1:np.shape(timevar)[0]]] = -1

   return(xmat)

# linear_model
def linear_model(xmat, dy):
    click.secho(f"\nLinear model...", bold=True)
    return(np.matmul(np.matmul(np.linalg.inv(
            np.matmul(np.transpose(xmat), xmat)), np.transpose(xmat)), dy))



# wrs_model
def wrs_model(dfinput, id_ = "id_", price = "price", time = "month", priceLog = True):

    df = cleanSQLoutput(dfinput)
    dateseq = np.sort(df[time].unique())

    """ Set variables """
    # Variables de base
    price = df[price]
    timevar = df[time]
    id_ = df[id_]

    # DataFrame de vente pairees
    rsp = repsaledata(id_, price, timevar)

    # On drop les inf qui peuvent etre creer si un prix est zero.
    rsp = rsp.replace([np.inf, -np.inf], np.nan).dropna()

    # Set matrix
    xmat = design_matrix(rsp)
    dy = rsp.price1 - rsp.price0

    #return xmat.shape, dy.shape

    """ Three stage regression """

    # First stage
    #beta = linear_model(xmat, dy)
    beta = sm.OLS(dy, xmat).fit().params
    #beta = fit0.params

    # Second stage
    u = dy - np.matmul(xmat, beta)
    x = (pd.to_datetime(rsp.time1) - pd.to_datetime(rsp.time0)).dt.days
    x = sm.add_constant(x)
    model = sm.OLS(np.absolute(u), x).fit()
    wgt = model.predict()
    weight = np.empty([len(wgt), ])
    weight[wgt > 0] = wgt[wgt > 0]

    # Third stage
    fit = sm.WLS(dy, xmat, weights = weight).fit()
    coef = pd.DataFrame({'date':dateseq, 'index':np.exp(np.append([0], fit.params))})

    return coef
