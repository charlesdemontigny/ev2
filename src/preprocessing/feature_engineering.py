# -*- coding: utf-8 -*-

from collections import OrderedDict
from typing import List, Optional

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import FunctionTransformer
from sklearn.neighbors import KNeighborsRegressor
from pandas.api.types import is_categorical, CategoricalDtype

class GroupByImputer(BaseEstimator, TransformerMixin):
    """
    performs a groupby column `by` prior to filling missing values using `strategy`
    strategy : str in ['mean', 'median']
                currently limited to `mean` and `median`.  Explicitely ignores np.nan.
    by: str
                must be a valid column name par of the DataFrame.columns.  This column should probably be categorical.
    cols_to_impute: list
                list of column names containing Nan that you want to fill with imputed values.
                All names have to be within DataFrame.columns and be numeric.
    Returns:
        The transform() method returns a DataFrame with filled values for each column specified in `cols_to_impute`

        Warning: It is possible some Nan remain if all values within a group were missing.
        It is recommended that another Imputer be used to fillna the remaining ones

    Missing: returning overall aggregation when specific `by` category has all-Nan
    """

    def __init__(self, strategy='median', cols_to_impute=['LATTITUDE_FRESH', 'LONGITUDE_FRESH'], by='Ville'):
        self.strategy = strategy
        self.cols_to_impute = cols_to_impute
        self.by = by

    def fit(self, X, y=None):
        strategy_funcs_ = {'mean': np.nanmean, 'median': np.nanmedian}
        if self.strategy not in ['median', 'mean']:
            return self

        "Stores the aggregate function's results per grouping for each column-to-impute"
        g = X.groupby(self.by)[self.cols_to_impute].agg(strategy_funcs_.get(self.strategy))
        self.agg_ = {c: {k: m for k, m in g[c].items()} for c in self.cols_to_impute}
        return self

    def transform(self, X):
        X = X.copy()
        for c in self.cols_to_impute:
            nans = X[c].isna()
            X.loc[nans, c] = X.loc[nans, [self.by, c]].apply(lambda row: self.agg_.get(
                c).get(row[self.by]), axis=1)
        return X


class AddSimpleFeatures(BaseEstimator, TransformerMixin):
    '''Simple feature extraction from building and transaction dataset.

    |---------------|---------------------------------------------|
    | Feature       | Description                                 |
    |---------------|---------------------------------------------|
    | tx_year       | transaction year, extracted from Date_Tx    |
    | cur_date_eval_year |Start year of the latest available evaluation  |
    |---------------|---------------------------------------------|
    '''

    def __init__(self):
        pass

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        x = x.copy()
        x['tx_year'] = x.DATE_TX.dt.year
        x['CUR_DATE_EVAL'] = pd.to_datetime(x['CUR_DATE_EVAL'])
        x['cur_date_eval_year'] = x.CUR_DATE_EVAL.dt.year
        return x


class SelectColumns(BaseEstimator, TransformerMixin):
    """
    Simple Transformer that can return a subset of columns from a pandas DataFrame.
    Designed to be used in a Pipeline, for e.g. within a FeatureUnion that requires different pre-processing
    for distinct features
    """

    def __init__(self, columns):
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x.loc[:, self.columns]


_DEFAULT_CATEGORIES = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10)
])


class DummyEncodeCategoricals(BaseEstimator, TransformerMixin):
    ''' Dummy-encoding of categorical columns

    parameters
    ----------
    columns : OrderedDict[str, Optional[int]]
        List of columns to encode, with optional limit on the number of categories
        to encode.
    '''

    def __init__(self, columns=_DEFAULT_CATEGORIES):
        assert isinstance(columns, OrderedDict)
        self.columns = columns
        self._categories = None

    def _top_categories(self, col: pd.Series, top: Optional[int]=None):
        if not is_categorical(col):
            col = col.astype(CategoricalDtype())
        top_categories = list(col.value_counts(sort=True)[:top].index)
        top_categories_sorted = [cat for cat in col.cat.categories if cat in top_categories]
        return top_categories_sorted + ['other-NA']

    def _dummies(self, col: pd.Series, categories: List[str]):
        if is_categorical(col):
            filtered_categoricals = col.cat.set_categories(categories)
        else:
            filtered_categoricals = col.astype(CategoricalDtype(categories=categories))
        filtered_categoricals = filtered_categoricals.fillna('other-NA')
        return pd.get_dummies(filtered_categoricals, sparse=False, drop_first=False)

    def fit(self, x: pd.DataFrame, y=None):
        self._categories = OrderedDict([(col, self._top_categories(x[col], top))
                                        for col, top in self.columns.items()])
        return self

    def transform(self, x: pd.DataFrame):
        assert self._categories is not None

        res = np.hstack((self._dummies(x[col], categories).values for col, categories in self._categories.items()))

        assert res.shape[1] == sum([len(cat) for cat in self._categories.values()])
        assert res.shape[0] == len(x)

        return res


class PrevPrice(BaseEstimator, TransformerMixin):
    """Return latest transaction price and year.

    Input
    -----
        x : Dataframe with 'Home_ID', 'Date_Tx' columns
        y : Numpy array with transaction prices.

    Output
    -----
        Numpy array with two numerical columns: 'prev_price' and 'days_since_last_tx'.
        (NaN if there is any previous transaction).

    Parameters
    ----------
        convert_to_log_price: If true, apply log transform to the returned prev_price column.
    """

    def __init__(self, convert_to_log_price=False):
        self.prev_prices_: pd.DataFrame = None
        self.convert_to_log_price = convert_to_log_price

    def fit(self, x, y):

        self.prev_prices_ = x.loc[:, ['Home_ID', 'Date_Tx']].copy(deep=False)
        self.prev_prices_.reset_index(inplace=True, drop=True)
        self.prev_prices_['prev_price'] = y
        self.prev_prices_['prev_date'] = self.prev_prices_['Date_Tx']
        self.prev_prices_.sort_values(['Date_Tx', 'Home_ID'], inplace=True)
        self.prev_prices_.copy(deep=True)

        return self

    def transform(self, x):

        res = x[['Home_ID', 'Date_Tx']]

        # pd.merge_asof requires sorted keys. We use `reset_index()` to recall the original order.
        res = res.reset_index(drop=True).sort_values(['Date_Tx', 'Home_ID'])
        orignal_index = res.index.copy()

        res = pd.merge_asof(res, self.prev_prices_, on='Date_Tx', by='Home_ID', direction='backward',
                            allow_exact_matches=False)

        # Recover orignal transaction order:
        res = res.set_index(orignal_index).sort_index().reset_index(drop=True)

        res['days_since_last_tx'] = (res['Date_Tx'] - res['prev_date']).dt.days

        assert not (res['days_since_last_tx'] <= 0).any()

        #month = pd.DataFrame({'prev_date_month':res['prev_date'].dt.month,
                            #'prev_date_year': res['prev_date'].dt.year})

        res = res[['prev_price', 'days_since_last_tx']]

        if self.convert_to_log_price:
            res['prev_price'] = np.log(res['prev_price'])

        res = res.values

        return res#, month


def UnitConverter(df, cols_to_convert='LOT_SUPERFICIE', unit_col='Aire_Unite', unit='PI2'):
    """Converts values of a DataFrame column based on a category found in another column

    Input
    -----
        df : Dataframe with column to convert, a unit column containing `str` categories
        cols_to_convert : `str` column name found in df.columns
        unit_col : `str` column name where you can find the caterogy `unit`
        unit :  `str` for which you want the  conversion to happen

    Output
    -----
        pandas.DataFrame of same size as input with `cols_to_convert` modified

    Warning:
    ------
        Currently limited to "PI2"
    """
    assert isinstance(df, pd.DataFrame)

    if unit == 'PI2':
        conv_factor = 1/10.7639
    else:
        raise ValueError(f"Unsupported unit: {unit}")

    df.loc[:, cols_to_convert] = df[cols_to_convert].mask(df[unit_col] == unit, lambda x: x * conv_factor)
    return df


class MaxCap(BaseEstimator, TransformerMixin):
    # caps Series with either a percentile of that column values or max_cap `int` value
    def __init__(self, cols_to_cap=None, max_cap=None, percentile=None):
        self.cols_to_cap = cols_to_cap
        self.max_cap = max_cap
        self.percentile = percentile

    def fit(self, X, y=None):
        assert isinstance(X, pd.DataFrame)
        assert self.cols_to_cap in X.columns.values

        if self.max_cap is not None:
            assert isinstance(self.max_cap, int)

        if not self.max_cap and self.percentile:
            self.max_cap = np.percentile(X[self.cols_to_cap].values, self.percentile)
        return self

    def transform(self, X):
        X = X.copy()
        X.loc[:, self.cols_to_cap] = X[self.cols_to_cap].where(X[self.cols_to_cap] < self.max_cap, other=self.max_cap)
        return X


class GeoKNN(BaseEstimator, TransformerMixin):
    """
    Return latest transaction price and year.

    Input
    -----
        k : number of neighbors to use for weighted average
        latlong : list of `str` names of columns for latitude and Longitude_Fresh
        target : list of `str` column names you want to use for data creation.
        metric : any `str` (or user defined `callable()`) part of sklearn's DistanceMetrics.
                 Default of 'l2' is ok, 'haversine' makes sense for geolocations, 'l1' may be argued for as well
                 since building distances are perceived as following roads by homebuyers
        weights : 'distance' or 'uniform'. Whether the average is weighted by
                 the distance of points w.r.t. validation data

    Output
    -----
        Numpy array with two numerical columns: 'prev_price' and 'days_since_last_tx'.
        (NaN if there is any previous transaction).

    Parameters
    ----------
        convert_to_log_price: If true, apply log transform to the returned prev_price column.
    must use [lat, long] for geolocation in order for haversine distance to make sense.
    """

    def __init__(self, k=50, latlong=['LATTITUDE_FRESH_Fresh', 'LONGITUDE_FRESH'], target=['SUPERFICIE'], metric='l2',
                 weights='distance'):
        self.k = k
        self.latlong = latlong
        self.target = target
        self.metric = metric
        self.weights = weights

    def fit(self, X, y):
        self.geocodes_ = X[self.latlong].copy().values

        target = X[self.target].copy()  # potentially multi-columns
        target['y_'] = y
        self.NN_transf_ = KNeighborsRegressor(n_neighbors=self.k, weights=self.weights, metric=self.metric)
        self.NN_transf_.fit(self.geocodes_, target.values)
        return self

    def transform(self, valid):
        valid = valid.copy()

        avg_targets = self.NN_transf_.predict(valid[self.latlong])
        assert avg_targets.shape[1] == len(self.target) + 1
        return avg_targets


def _set_dtype(df, dtype):
    return df.apply(lambda col: col.astype('str'))


def _fillna(df, fill_value):
    return df.fillna(fill_value)


def AsTypeDF(dtype):
    """Transformer change the dtype of a Dataframe columns."""
    return FunctionTransformer(_set_dtype, kw_args={'dtype': dtype}, validate=False)


def FillNaDF(fill_value):
    """Transformer that fills NA values in a Dataframe with a constant value."""
    return FunctionTransformer(_fillna, kw_args={'fill_value': fill_value}, validate=False)
