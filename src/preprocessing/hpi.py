# -*- coding: utf-8 -*-
"""
Created on 2018-03-27
@title: JLR hpi
@author: Charles Demontigny
@version: Python 3.6
"""

""" Import packages -------------------------------------------------------- """
import numpy as np
import cx_Oracle
import click
import pandas as pd
from hpi_helper import load_dataset, cleanSQLoutput, wrs_model
import time

""" Wrapper function to create the JLR HPI --------------------------------- """
def run_hpi():

    time0 = time.time()
    click.secho(f"\nDebut: {time.ctime()}")
    """ Etape 1: Telecharger toutes les donnees necessaires de Oracle ---------- """
    click.secho(f"\nInitialisation de la connection et creation de la requete...", bold=True)
    df = load_dataset()

    """ Etape 2: Split by group and apply wrs_model"""
    click.secho(f"\nDiviser par groupe", bold = True)
    g = df.groupby(['REIDU'])
    uniq = df['REIDU'].unique()
    output = pd.DataFrame()

    click.secho(f"\nCalcule de l'indice de prix JLR en cours...", bold=True)
    for cd in uniq:
        temp = g.get_group(cd)
        hpi_temp = wrs_model(temp)
        hpi_temp['geo'] = cd
        output = output.append(hpi_temp)

    """ Etape 3: Sauvegarder le tout. """
    click.secho(f"\nSauvegarde des resultats...", bold=True)
    output.to_csv("test1.csv", index = False)
    time1 = time.time()
    delta = np.round(time1 - time0, decimals = 0)
    click.secho(f"\n Temps total: {delta}")

# Run --------------------------------------------------------------------------
if __name__ == '__main__':
    run_hpi()
