# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer, Imputer

from preprocessing.feature_engineering import GroupByImputer, SelectColumns


def Imputer_df(df: pd.DataFrame, cols=None, strategy='mean'):
    # wrapper around Imputer allowing use with pandas.DataFrame
    df = df.copy()
    imp = Imputer(strategy=strategy)
    if cols is None:
        cols = list(df.columns)  # BEWARE: if nothing passed, apply on ALL columns
    df_array = imp.fit(df[cols]).transform(df[cols])
    df.loc[:, cols] = df_array
    return df


params = {
    'log_regression': True
}

_strategy = 'mean'

model = Pipeline([
    ('imputeLatLong_byCity',
     GroupByImputer(
         strategy='median',
         by='Ville_BSQ',
         cols_to_impute=['Lattitude_Fresh', 'Longitude_Fresh'])),
    ('reimpute_latlong',
     FunctionTransformer(
         Imputer_df,
         kw_args={'cols': ['Lattitude_Fresh', 'Longitude_Fresh'], 'strategy': _strategy},
         validate=False)
     ),
    ('get_latlong', SelectColumns(['Longitude_Fresh', 'Lattitude_Fresh'])),
    ('regression', KNeighborsRegressor(n_neighbors=10, weights='uniform', metric='euclidean'))
])
