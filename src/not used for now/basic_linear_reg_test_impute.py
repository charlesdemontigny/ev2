# -*- coding: utf-8 -*-

from sklearn.linear_model import LinearRegression  # , Lasso, Ridge
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import Imputer, StandardScaler

from preprocessing.feature_engineering import AddSimpleFeatures, SelectColumns, GroupByImputer

params = {
    'log_regression': False
}

_reg_cols = ['Annee_Const', 'Superficie', 'Code_Utilisation',
             'cur_eval_valeur_batisse', 'N_etages', 'tx_year',  'Lot_Superficie']

_reg_model = LinearRegression(fit_intercept=True)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)


model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('imputer', GroupByImputer(strategy='median', by='Ville_BSQ', cols_to_impute=[
                'Annee_Const',
                'Code_Utilisation',
                'Superficie'])
     ),
    ('reduce_features', SelectColumns(_reg_cols)),
    ('impute_rest', Imputer(strategy='mean', axis=0, copy=True)),
    ('scaling', StandardScaler()),
    ('LinearRegression', _reg_model)]
)
