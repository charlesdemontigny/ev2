# -*- coding: utf-8 -*-
from collections import OrderedDict

from lightgbm import LGBMRegressor
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import FunctionTransformer

from preprocessing.feature_engineering import (AddSimpleFeatures, PrevPrice,
                                               SelectColumns)

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'cur_date_eval_year', 'N_etages',
                       'Longitude_Fresh', 'Lattitude_Fresh', 'tx_year', 'Lot_Superficie']

_categorical_features = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10),
    ('PRO_VOIE_C', 10),
])

_regressor = LGBMRegressor(objective='regression',
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=200,
                           silent=False,
                           random_state=1234, n_jobs=-1)


def to_codes(df):
    """Utility function to convert categories to int"""
    return df.apply(lambda col: col.cat.codes)


model = Pipeline([
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features))]
        )),
        ('prev_price', PrevPrice())
    ])),
    ('regression', _regressor)
])

params = {
    'log_regression': True,
    'fit_params': {
        # indices of categorical columns (encoding performed by LGBM):
        'regression__categorical_feature': list(range(len(_categorical_features)))
    }
}
