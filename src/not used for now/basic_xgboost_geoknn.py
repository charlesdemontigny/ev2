# -*- coding: utf-8 -*-

from collections import OrderedDict
import pandas as pd

from sklearn.preprocessing import (FunctionTransformer, Imputer)
from sklearn.pipeline import FeatureUnion, Pipeline
from xgboost import XGBRegressor

from preprocessing.feature_engineering import (AddSimpleFeatures,
                                               DummyEncodeCategoricals,
                                               SelectColumns, GroupByImputer,
                                               GeoKNN)

_reg_cols = ['Annee_Const', 'tx_year',
             'Superficie', 'Lot_Superficie',
             'N_etages', 'Nb_Logements', 'Lattitude_Fresh', 'Longitude_Fresh',
             'cur_eval_valeur_batisse', 'Code_Utilisation']


def Imputer_df(df: pd.DataFrame, cols=None, strategy='mean'):
    # wrapper around Imputer allowing use with pandas.DataFrame
    df = df.copy()
    imp = Imputer(strategy=strategy)
    if cols is None:
        cols = list(df.columns)  # BEWARE: if nothing passed, apply on ALL columns
    df_array = imp.fit(df[cols]).transform(df[cols])
    df.loc[:, cols] = df_array
    return df


params = {
    'log_regression': True
}

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'N_etages', 'Longitude_Fresh', 'Lattitude_Fresh',
                       'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

_knn_cols = ['Superficie', 'Lot_Superficie', 'Annee_Const',
             'N_etages', 'Nb_Logements',
             'first_prev_eval_valeur_batisse', 'first_prev_eval_valeur_lot', 'cur_eval_valeur_batisse',
             'cur_eval_valeur_lot']

_strategy = 'mean'

_categorical_features = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10),
    ('PRO_VOIE_C', 10)
])

_xgb = XGBRegressor(n_estimators=250,
                    learning_rate=0.1,
                    max_depth=9,
                    reg_lambda=.5,  # L2 leaf regularization
                    reg_alpha=0.3,  # L1 leaf regularization
                    gamma=0,  # minimum loss reduction required to make a further partition on a leaf node of the tree.
                    objective="reg:linear",  # optimize for RMSE
                    seed=1234,
                    nthread=3)

_knn = Pipeline([
             ('imputeLatLong_byCity',
                GroupByImputer(
                    strategy='median',
                    by='Ville_BSQ',
                    cols_to_impute=_knn_cols + ['Lattitude_Fresh', 'Longitude_Fresh'])),
             ('reimpute_latlong',
              FunctionTransformer(Imputer_df,
                                  kw_args={'cols': _knn_cols + ['Lattitude_Fresh', 'Longitude_Fresh'],
                                           'strategy': _strategy},
                                  validate=False)),
             (f'NN_{_strategy}', GeoKNN(k=30,
                                        latlong=['Lattitude_Fresh', 'Longitude_Fresh'],
                                        metric='l2',
                                        weights='uniform',
                                        target=_knn_cols))
         ])


model = Pipeline([
    ('union', FeatureUnion([
        ('categories', DummyEncodeCategoricals(_categorical_features)),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))])
         ),
        ('pipe_latlong', _knn)
    ])
    ),
    ('regression', _xgb)
]
)
