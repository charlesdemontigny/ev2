# -*- coding: utf-8 -*-

from sklearn.pipeline import Pipeline

from models.median_RTA_autre import MedianByRTA

params = {
    'log_regression': True
}

model = Pipeline([
    ('yearlyMedianBy', MedianByRTA())])
