# -*- coding: utf-8 -*-

from sklearn.pipeline import Pipeline

from models.median_baseline import MedianByCity

params = {
    'log_regression': False
}

model = Pipeline([
    ('yearlyMedianByCity', MedianByCity())])
