#!/usr/bin python3
# -*- coding: utf-8 -*-

import datetime
import os
from pathlib import Path

import click
import numpy as np
import pandas as pd
import gc

from var import ignored_columns, ignored_dataset_stumps, rename_columns


def import_datasets(data_path, datasets=[]):
    """
    loads all relevant datasets from the given directory and delivers in a Dict

    Parameters
    ----------
    data_path:
        relative path of the dataset files stored as either .txt
        or compressed with .bz2
    dataset:
        list of filenames to load.  If none is provided, all files will be
        loaded *except those found in the ignored_dataset_stumps*

    Returns
    -------
    Dict of pandas.DataFrame
        Dict of DataFrame where the key is the file name

    """

    # searches the data_path to extract all relevant datasets
    filename_list = {f[:f.find(".")]: f for f in os.listdir(data_path) if f[-4:] in ['.csv', '.txt']}

    # default case where no stumps are provided
    if not datasets:
        datasets = [ds for ds in filename_list.keys() if ds not in ignored_dataset_stumps]

    dfs = {}

    # import each dataset of interest
    for stump in datasets:
        # don't load if already present
        if stump not in dfs.keys():
            print(f"  {stump}...")

            if filename_list[stump].endswith(".txt"):
                df_in = pd.read_csv(data_path +
                                "/{}.txt".format(stump),
                                sep='~',
                                header=0,
                                encoding='ISO-8859-1')
                dfs[stump] = df_in[df_in.columns.difference(
                        ignored_columns[stump])
                        ].rename(mapper=rename_columns[stump], axis=1)

            else:

                df_in = pd.read_csv(data_path +
                            "/{}.csv".format(stump),
                            sep='~',
                            header=0,
                            encoding='ISO-8859-1')
                dfs[stump] = df_in[df_in.columns.difference(
                        ignored_columns[stump])
                        ].rename(mapper=rename_columns[stump], axis=1)
        else:
            print("dataset '{}' has already been loaded previously".format())

    # special import for geo_codes.csv table because of ';' separator instead of ~
    #print("  geo_code...")
    #df_in = pd.read_csv(data_path + "/{}.csv".format('geo_code'),
                                    #sep=';',
                                    #header=0,
                                    #encoding='ISO-8859-1')
    #dfs['geo_codes'] = df_in.rename(mapper=rename_columns['geo_codes'], axis=1)

    return dfs


def preprocess_immeuble(imm):
    """
    Parameters
    ----------
    imm: pandas.DataFrame
        raw dataset of buildings with sparse details describing the building
        the Unique identifier of the buildings 'Home_ID' should be used to
        merge with other datasets.

    Returns
    -------
    pandas.DataFrame
        building dataset with Home_ID column cast to int

    """
    imm.Home_ID = imm.Home_ID.astype(np.int64)
    return imm


def preprocess_transaction(tx):
    """
    Light pre-processing of the transaction raw dataset

    Parameters
    ----------
    tx: pandas.DataFrame
        raw dataset of transactions containing at least information on the transaction ID, the building ID and the
        'Home_ID', 'Tx_ID', 'Date_Tx'
    Returns
    -------
    pandas.DataFrame
       A tidy dataset
    """
    imputer = {'Vente_Intra_Famille': -1,
               'Vente_Multiple': -1}

    tx.fillna(value=imputer, inplace=True)

    tx['Date_Tx'] = pd.to_datetime(tx.Date_Tx, format='%Y%m%d')
    tx['Date_Created'] = pd.to_datetime(tx.Date_Created, format='%Y%m%d')
    tx['Date_Modified'] = pd.to_datetime(tx.Date_Modified, format='%Y%m%d')
    tx[['Comptant', 'Tx_ID', 'Montant']] = tx[['Comptant', 'Tx_ID', 'Montant']].astype(float)
    tx[['Tx_ID', 'Home_ID', 'Vente_Intra_Famille']] = tx[['Tx_ID', 'Home_ID', 'Vente_Intra_Famille']].astype(np.int64)

    return tx


def preprocess_annotations(annot):
    """
    Simple processing of the annotation dataset for easier merging to immeubles
    and transactions.  The name of the fields is not yet added to the
    rename_columns variable in var.py
    """
    annot.Home_ID = annot.Home_ID.astype(np.int64)
    return annot


def preprocess_geocodes(gc):
    gc.Home_ID = gc.Home_ID.astype(np.int64)
    return gc


def preprocess_eval(ev):
    """
    Parameters
    ----------
    ev: pandas.DataFrame

    Returns
    -------
    pandas.DataFrame
       A tidy dataset

    """

    # assume ~3 months lag before JLR gets eval datasets
    ev['Annee_Debut'] = ev.Annee_Debut.astype(str)
    ev['Date_Eval'] = pd.to_datetime(ev.Annee_Debut + "03" + "01")

    ev[["Eval_Valeur_Batisse", "Eval_Valeur_Lot"]] = ev[["Eval_Valeur_Batisse", "Eval_Valeur_Lot"]].astype(float)
    ev['Home_ID'] = ev['Home_ID'].astype(np.int64)
    return ev


def merge_datasets(dfs):
    """
    Merge immeuble, image annotations and transactions.

    Parameters
    ----------
    dfs : Dict[pandas.DataFrame]
       A dictionary of pandas.DataFrames that we wish to merge together
       we expect the following keys : 'transaction', 'immeuble' and 'annotation_photo'

    Returns
    -------
    pandas.DataFrame
       A tidy dataset
    """

    buildings = dfs['immeuble'].set_index('Home_ID').sort_index()
    geocodes = dfs['geocodes'].set_index('Home_ID').sort_index()
    transactions = dfs['transaction'].set_index('Home_ID').sort_index()
    annotations = dfs['annotation_photo'].set_index('Home_ID').sort_index()

    # Buildings complement the geocodes
    merged = buildings.join(geocodes, how='left')

    # Building - transaction left outer join:
    merged = merged.join(transactions, how='left')

    # There are several +400k buildings without a transaction, these will be kept separate:
    is_without_tx = merged.Tx_ID.isna()
    imm_with_no_tx = merged[is_without_tx]
    if not imm_with_no_tx.empty:
        print(f"Found {imm_with_no_tx.shape[0]} buildings without a transaction,")
        print(" these will be stored in .data/interim/immeubles_sans_transactions.csv")

    # SAVE temporarily the orphan buildings
    imm_with_no_tx.reset_index().to_csv("./data/interim/immeubles_sans_transactions.csv")

    # filter proper buildings-with-transactions
    merged = merged[~is_without_tx]

    # Merge building with image annotations :
    merged = merged.join(annotations, how='left')

    return merged.reset_index()


def merge_last_eval(left, right, left_on, prefix, right_on='Date_Eval', allow_exact_matches=True, by='Home_ID',
                    dummy_date=pd.to_datetime('1971-01-01')):
    """
       Merges the DataFrame 'left' with a second DataFrame 'right' that contains a
       datetime column we wish to filter by, similar to a SQL  WHERE val1 <= val2.
       This function is called repeatedly to get the nth previous evaluation
       of a given transaction.
       It is assumed that the left DataFrame is a merged immeubles-transaction.

    """
    # Replace null keys with a dummy date:
    if left[left_on].isna().any():
        left = left.copy()
        left[left_on].fillna(dummy_date, inplace=True)
        left.sort_values(left_on, inplace=True)

    # Merge with nearest evaluation:
    res = pd.merge_asof(left, right, left_on=left_on, right_on=right_on, by=by,
                        allow_exact_matches=allow_exact_matches, direction='backward')

    # Rename columns (lowercase + add prefix):
    eval_cols = ['Date_Eval', 'Eval_Valeur_Batisse', 'Eval_Valeur_Lot']
    res = res.rename(columns={col: prefix + col.lower() for col in eval_cols})

    # Remove dummy dates:
    res.loc[res[left_on] == dummy_date, left_on] = np.nan

    return res


def merge_eval(mgd, ev_pp):
    """
    Merge immeuble, image annotations and transactions.

    Parameters
    ----------
    mgd:
        merged pandas.DataFrame of transaction and immeubles.  Should contain
        at minimum a 'Home_ID' unique to each building, as well as a timestamp
        for each distinct transaction.  The timestamp of transactions 'Date_Tx'
        is used as the latest valid date to use in the evaluation dataset.

    Returns
    -------
    mgd DataFrame with additional columns representing the previous



    """
    buildings = mgd[['Date_Tx', 'Home_ID', 'Tx_ID']].sort_values(['Date_Tx', 'Home_ID'])

    eval_cols = ['Date_Eval', 'Eval_Valeur_Batisse', 'Eval_Valeur_Lot']
    evals = ev_pp[['Home_ID'] + eval_cols].sort_values(['Date_Eval', 'Home_ID'])
    evals.Home_ID = evals.Home_ID.astype(np.int64)

    res = merge_last_eval(buildings, evals, left_on='Date_Tx', allow_exact_matches=True, prefix='cur_')

    del buildings
    gc.collect()

    res = merge_last_eval(res, evals, left_on='cur_date_eval', allow_exact_matches=False, prefix='first_prev_')
    res = merge_last_eval(res, evals, left_on='first_prev_date_eval', allow_exact_matches=False, prefix='second_prev_')
    res = merge_last_eval(res, evals, left_on='second_prev_date_eval', allow_exact_matches=False, prefix='third_prev_')

    del evals
    gc.collect()

    return mgd.merge(res.drop(columns=['Date_Tx']), on=['Home_ID', 'Tx_ID'], how='left')


def fix_dtypes(df):
    numeric_cols = ['Montant', 'EVIADISPO', 'Longitude', 'Lattitude', 'Tx_Type', 'tx_exclue', 'Tx_ID',
                    'Vente_Multiple', 'Achat_Sans_Hypot', 'CODE_VENTE_TAXABLE', 'Ville_BSQ',
                    'Lot_Superficie', 'PRO_CIVIC1', 'Nb_Logements', 'Unite_Aire_code', 'Secteur',
                    'N_etages', 'Annee_Const', 'Superficie', 'Code_Utilisation', 'Lot_Largeur',
                    'MUR_FACADE_ID', 'ABRI_AUTO', 'ETAGE', 'PLACE_GARAGE', 'Lot_Profondeur',
                    'N_commerces', 'PLACE_ABRI_AUTO', 'PHOT_AVEC_VISAGE', 'Arrondissement_ID', 'PRO_CIVIC2',
                    'Construction_Neuve', 'Vente_Intra_Famille', 'third_prev_eval_valeur_batisse',
                    'third_prev_eval_valeur_lot', 'second_prev_eval_valeur_batisse', 'second_prev_eval_valeur_lot',
                    'first_prev_eval_valeur_batisse', 'first_prev_eval_valeur_lot', 'cur_eval_valeur_batisse',
                    'cur_eval_valeur_lot', 'Comptant']

    categorical_cols = ['Arrondissement_Nom', 'CAT_BAT_DESC', 'Code_Utilisation_txt',
                        'Ville', 'Paroisse', 'DESC_VENTE_TAXABLE', 'NOM_SECTEUR', 'PRO_VOIE',
                        'Code_Postal', 'Tx_Type_Desc', 'Vente_Type', 'DESCRIPTION_CLASSE',
                        'Categorie_Batiment', 'PRO_VOIE_C', 'Unite_Aire', 'CLASSE_BAT', 'TYPE',
                        'MUR_FACADE', 'PRO_VOIE_L', 'PRO_VOIE_O', 'PRO_LOT_ORIG_TYPE',
                        'PRO_SUFFIXE2', 'PRO_SUFFIXE1', 'GARAGE']
    """
    str_columns = ['UNIQUE_ID', 'Tx_ID', 'Matricule', 'Lot_Renove', 'IDUEF_NB', 'PRO_SUIT1',
                   'Lot_ID', 'Cadastre', 'Home_ID']
    """
    datetime_cols = {'Date_Creation_enreg': '%Y%m%d',
                     'Date_Modif_enreg': '%Y%m%d',
                     # 'DATE_AJOUT' : '%Y-%m-%d',
                     'Date_Modified': '%Y-%m-%d',
                     'Date_Created': '%Y-%m-%d'}
    # Already converted: Date_Tx, third_prev_date_eval, second_prev_date_eval, first_prev_date_eval, cur_date_eval

    for col in numeric_cols:
        print(f'converting: {col}')
        if col in df.columns:
            df[col] = pd.to_numeric(df[col], errors='raise')

    for col in categorical_cols:
        print(f'converting: {col}')
        if col in df.columns:
            df[col] = pd.Categorical(df[col])

    for col, date_format in datetime_cols.items():
        print(f'converting: {col}')
        if col in df.columns:
            df[col] = pd.to_datetime(df[col], format=date_format)

    # TODO: Fix DATE_AJOUT format
    # TODO: clean MUR_FACADE columns
    # TODO: clean PRO_SUIT1, PRO_VOIE_O, PRO_VOIE_L, PRO_LOT_ORIG_TYPE ?

    return df


@click.group()
def cli():
    pass


@cli.command('new', help="Create a new train and test dataset from raw building, transcation and evaluation files")
@click.option('-i', '--input-path', default='./data/raw/2018-04-03',
              help='Path of the directory containing the raw data')
@click.option('-o', '--output-path', default=None,
              help='Output directory path (inferred from input data by default)')
@click.option('-th', '--thresh-yr', default=2015, type=int,
              help='Train-Test split year')
def make_dataset(input_path, output_path, thresh_yr):
    """
    Loads and merges the raw datasets found in input_path
    This directory should contain .bz2 or .txt files
    The filenames should remain constant, otherwise the variables in var.py
    should be modified accordingly.
    """

    if output_path is None:
        output_path = './data/processed/{}/'.format(Path(input_path).name)

    # make sure the output folder exists
    os.makedirs(output_path, exist_ok=True)

    input_file_to_read = [
        'annotation_photo',
        'evaluation',
        'immeuble_merged',
        'transaction_merged',
        'geo_codes'
    ]

    print(f"Loading raw datasets ({', '.join(input_file_to_read)})...")

    d = import_datasets(input_path, datasets=input_file_to_read)
    print("Pre-Processing the raw datasets...")

    tx_pp = preprocess_transaction(d['transaction_merged'])
    imm_pp = preprocess_immeuble(d['immeuble_merged'])
    gc_pp = preprocess_geocodes(d['geo_codes'])
    annot_pp = preprocess_annotations(d['annotation_photo'])
    ev_pp = preprocess_eval(d['evaluation'])

    del d
    gc.collect()

    print("Merging transaction, building, geocode and image annotation data...")

    # merging the first 4 datasets,
    # please note the subtle change of key names w.r.t. filenames
    mgd = merge_datasets({'transaction': tx_pp,
                          'immeuble': imm_pp,
                          'geocodes': gc_pp,
                          'annotation_photo': annot_pp})
    # NOTE: 'geo_codes' file is always implicitly loaded.

    del tx_pp, imm_pp, gc_pp, annot_pp
    gc.collect()

    print("Merging latest city evaluation data...")

    mgd = merge_eval(mgd, ev_pp)  # merging the 4 latest evaluations, if available
    print("Type of mgd is %s" % type(mgd))
    print("Verifying columns data types...")
    #print("La taille du df %s" % mgd.shape)
    mgd_final = fix_dtypes(mgd)  # cast the columns into proper dtypes
    del mgd
    gc.collect()

    print("here is the list of columns currently in mgd_final : \n", mgd_final.columns)

    # train is before <= 2015, test is after >= 2015

    # save training set
    mgd_final[mgd_final.Date_Tx.dt.year <= thresh_yr].reset_index(drop=True).to_csv(
        os.path.join(output_path, f"train_{int(thresh_yr)}.feather"))

    # save test set
    print("saving test_dataset at " + output_path)
    mgd_final[mgd_final.Date_Tx.dt.year > thresh_yr].reset_index(drop=True).to_csv(
        os.path.join(output_path, f"test_{int(thresh_yr)}.feather"))

    return



@cli.command('prepare-input', help="Create a feature dataset with hypothetical transactions on a target date, for all building.")
@click.option('-i', '--input-path', default='./data/raw/2018-04-03',
              help='Path of the directory containing the raw data')
@click.option('-o', '--output-path', default=None,
              help='Output directory path (inferred from input data by default)')
@click.option('-d', '--target-date', type=str, required=True,
              help='Train-Test split year (format: "%Y-%m-%d")')
def prepare_input_features_for_final_export(input_path, output_path, target_date):
    """
    Variant of `make_dataset`, used to create an input features dataset for the final
    price estimation export. Instead of using real transactions, we generate hypothetical
    transactions (one transaction per building on the target date).

    Parameters
    ----------
    input_path : str
        Path of the directory containing input raw data files (building, evaluations,
        etc.). This directory should contain .bz2 or .txt files. The filenames should
        remain constant, otherwise the variables in `var.py` should be modified
        accordingly.

    target_date: str
        Date of the hypothetical transactions to be generated for each building.

    output_path: str
        Output directory path, used to store the prepared. The computed features will be
        stored in a file named "features_{yyyy}-{mm}-{dd}.feather" using the target date.

    Notes
    -----
    - The output datasets will note no 'Comptant' and 'Montant' columns.
    - The output datasets will contains one line per building appearing in the input building
      dataset. There is no filtering (old non-existant building may be used).
    """

    target_date = datetime.datetime.strptime(target_date, '%Y-%m-%d')

    if output_path is None:
        output_path = './data/processed/{}/input'.format(Path(input_path).name)

    # make sure the output folder exists
    os.makedirs(output_path, exist_ok=True)

    input_file_to_read = [
        'annotation_photo',
        'evaluation',
        'immeuble_merged',
        'transaction_merged',
        'geo_codes'
    ]

    print(f"Loading raw datasets ({', '.join(input_file_to_read)})...")

    datasets = import_datasets(input_path, datasets=input_file_to_read)
    # NOTE: 'geo_codes' file is always implicitly loaded.

    print("Pre-Processing the raw datasets...")

    imm_pp = preprocess_immeuble(datasets['immeuble_merged'])
    gc_pp = preprocess_geocodes(datasets['geo_codes'])
    annot_pp = preprocess_annotations(datasets['annotation_photo'])
    ev_pp = preprocess_eval(datasets['evaluation'])
    del datasets
    gc.collect()
    #########################################################
    print(f"Generating hypothetical transaction on {target_date.strftime('%Y-%m-%d')}...")

    building_ids = imm_pp.Home_ID.unique()

    tx_pp = pd.DataFrame({
        'Home_ID': building_ids,
        'Tx_ID': range(-1, -(len(building_ids) + 1), -1),  # Negative IDs to prevent clashes
    })

    tx_pp['Date_Tx'] = tx_pp['Date_Created'] = tx_pp['Date_Modified'] = target_date
    tx_pp['UNIQUE_ID'] = tx_pp['Tx_ID']

    tx_pp['Vente_Type'] = 4
    tx_pp['Tx_Type_Desc'] = "VENTE"

    tx_pp['Vente_Intra_Famille'] = 0
    tx_pp['Achat_Sans_Hypot'] = 0
    tx_pp['Vente_Multiple'] = 0
    tx_pp['tx_exclue'] = 0
    tx_pp['CODE_VENTE_TAXABLE'] = 0
    tx_pp['DESC_VENTE_TAXABLE'] = "NE S'APPLIQUE PAS"
    tx_pp['Construction_Neuve'] = np.nan

    tx_pp['Secteur'] = np.nan
    tx_pp['NOM_SECTEUR'] = "--Not Generated--"

    # Unfilled columns : Comptant, Montant

    print(f'{len(tx_pp)} transaction generated.')

    #########################################################

    print("Merging transaction, building, geocode and image annotation data...")

    # merging the first 4 datasets,
    # please note the subtle change of key names w.r.t. filenames
    mgd = merge_datasets({'transaction': tx_pp,
                          'immeuble': imm_pp,
                          'geocodes': gc_pp,
                          'annotation_photo': annot_pp})
    del tx_pp, imm_pp, gc_pp, annot_pp
    gc.collect()
    print("Merging latest city evaluation data...")

    mgd = merge_eval(mgd, ev_pp)  # merging the 4 latest evaluations, if available

    print("Verifying columns data types...")

    mgd_final = fix_dtypes(mgd)  # cast the columns into proper dtypes
    del mgd
    gc.collect()
    print("Data preparation done !")

    print(f"{len(mgd_final)} samples, {mgd_final.shape[1]} Columns: \n  {','.join(mgd_final.columns)}")

    print(f"Attempting to save the resulting datasets into feather format...")

    output_file = Path(output_path) / f"features_{target_date.strftime('%Y-%m-%d')}.feather"
    print("Output file: ", output_file)

    mgd_final.reset_index(drop=True).to_feather(output_file)


if __name__ == '__main__':
    cli(obj={})
