# -*- coding: utf-8 -*-

from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer

from preprocessing.feature_engineering import PrevPrice

params = {
    'log_regression': True
}

model = Pipeline([
    ('Prev_price', PrevPrice()),
    ('mean_imputation', Imputer(strategy="mean")),
    ('rf', RandomForestRegressor(n_estimators=18, criterion="mse",
                                 max_depth=10, min_samples_split=2, min_samples_leaf=1,
                                 min_weight_fraction_leaf=0., max_features="auto",
                                 max_leaf_nodes=None, min_impurity_split=None, bootstrap=True))]
)
