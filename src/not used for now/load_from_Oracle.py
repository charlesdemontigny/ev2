# -*- coding: utf-8 -*-
"""
Date de creation: 2018-03-29
@uteur: Charles Demontigny
Titre: Charger les donnees de Oracle
"""

# Import packages --------------------------------------------------------------
import pandas as pd
import cx_Oracle
import click
import os
from datetime import datetime
from var import ignored_columns, ignored_dataset_stumps, rename_columns
from query import geo_code, eval, immeuble, transaction

def load_new_data():
    # Setup connection & query -----------------------------------------------------
    conn = cx_Oracle.connect( "jlrprod", 'rlj','jlrorap1relv', encoding = 'UTF-8')
    # Load Data --------------------------------------------------------------------
    click.secho(f'\nChargement de la geographique...', bold = True)
    geo = pd.read_sql(geo_code.query.encode('utf-8'), con = conn)

    click.secho(f'\nChargement des immeubles...', bold = True)
    imm0 = pd.read_sql(immeuble.query0.encode('utf-8'), con = conn)
    imm1 = pd.read_sql(immeuble.query1.encode('utf-8'), con = conn)

    click.secho(f'\nChargement des transaction...', bold = True)
    transac = pd.read_sql(transaction.query.encode('utf-8'), con = conn)

    click.secho(f'\nChargement des evaluations...', bold = True)
    evaluation = pd.read_sql(eval.query.encode('utf-8'), con = conn)

    # Clean data -------------------------------------------------------------------
    click.secho(f'\nNettoyage des donnees...', bold = True)
    imm = imm0.merge(imm1, how = 'left', on = 'PRO_NO_ID_UEF')

    # Save data --------------------------------------------------------------------
    click.secho(f'\nSauvegarde des fichiers...', bold = True)

    now = datetime.now()
    month = str(now.month)
    day = str(now.day)

    if len(month) == 1:
        month = "0" + month
    if len(day) == 1:
        day = "0" + day

    folder_path = "../data/raw/" + str(now.year) + "-" + month + "-" + day
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    print(folder_path)

    geo.to_csv(folder_path + "/geo_codes.csv", sep = "~", index = False)
    imm.to_csv(folder_path + "/immeuble_merged.csv", sep = "~", index = False)
    transac.to_csv(folder_path + "/transaction_merged.csv", sep = "~", index = False)
    evaluation.to_csv(folder_path + "/evaluation.csv", sep = "~", index = False)
    click.secho(f'\nTerminé!', bold=True)

# Run load_new_data() ----------------------------------------------------------
if __name__ == '__main__':
    load_new_data()
