# -*- coding: utf-8 -*-

from collections import OrderedDict
from models.median_multilevel import MedianByCityYear
from sklearn.pipeline import FeatureUnion, Pipeline
from xgboost import XGBRegressor

from preprocessing.feature_engineering import (AddSimpleFeatures,
                                               DummyEncodeCategoricals,
                                               SelectColumns)

params = {
    'log_regression': True
}

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'N_etages', 'Longitude_Fresh', 'Lattitude_Fresh',
                       'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

_categorical_features = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10),
    ('PRO_VOIE_C', 10)
])

_med_price = MedianByCityYear()

_xgb = XGBRegressor(n_estimators=200,
                    learning_rate=0.1,
                    max_depth=8,
                    reg_lambda=1,  # L2 leaf regularization
                    reg_alpha=0,  # L1 leaf regularization
                    gamma=0,  # minimum loss reduction required to make a further partition on a leaf node of the tree.
                    objective="reg:linear",  # optimize for RMSE
                    seed=1234, nthread=3)


model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('union', FeatureUnion([
        ('median_price', _med_price),
        ('categories', DummyEncodeCategoricals(_categorical_features)),
        ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))
    ])),
    ('regression', _xgb)
]
)
