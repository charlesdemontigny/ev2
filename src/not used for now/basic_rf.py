# -*- coding: utf-8 -*-

from collections import OrderedDict

from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import Imputer

from preprocessing.feature_engineering import (AddSimpleFeatures,
                                               DummyEncodeCategoricals,
                                               GroupByImputer, SelectColumns)

params = {
    'log_regression': True
}

_categorical_vars = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10)
])

_numerical_vars = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                   'cur_eval_valeur_batisse', 'N_etages', 'Longitude_Fresh',
                   'Lattitude_Fresh', 'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

model = Pipeline([
    ('imputer', GroupByImputer(strategy='median', by='Ville_BSQ', cols_to_impute=[
        'Annee_Const',
        'Code_Utilisation',
        'Superficie'])
     ),
    ('union', FeatureUnion([
        ('categories', DummyEncodeCategoricals(_categorical_vars)),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_vars + _num_feats_eng))]
        ))
    ])),
    ('impute_mean', Imputer(strategy="mean")),
    ('regression', RandomForestRegressor(n_estimators=18, criterion="mse",
                                         max_depth=10, min_samples_split=2, min_samples_leaf=1,
                                         min_weight_fraction_leaf=0., max_features="auto",
                                         max_leaf_nodes=None, min_impurity_split=None, bootstrap=True))
])
