# -*- coding: utf-8 -*-
from collections import OrderedDict

import pandas as pd
from lightgbm import LGBMRegressor
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import FunctionTransformer, Imputer

from preprocessing.feature_engineering import (AddSimpleFeatures, GeoKNN,
                                               GroupByImputer, MaxCap,
                                               PrevPrice, SelectColumns,
                                               UnitConverter, Debug)

_numerical_features = ['ANNEE_CONST', 'SUPERFICIE', 'CODE_UTILISATION', 'NB_LOGEMENTS', 'N_COMMERCES',
                       'CUR_EVAL_VALEUR_BATISSE', 'N_ETAGES',
                       'FIRST_PREV_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT', 'LONGITUDE_FRESH',
                       'FIRST_PREV_EVAL_VALEUR_LOT', 'LATTITUDE_FRESH', 'LOT_SUPERFICIE']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['TX_YEAR', 'CUR_DATE_EVAL_YEAR']

_knn_cols = ['SUPERFICIE', 'LOT_SUPERFICIE', 'ANNEE_CONST',
             'N_ETAGES', 'NB_LOGEMENTS',
             'FIRST_PREV_EVAL_VALEUR_BATISSE', 'FIRST_PREV_EVAL_VALEUR_LOT',
             'CUR_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT']

_categorical_features = OrderedDict([
    ('CATEGORIE_BATIMENT', 10),
])

_strategy = 'mean'

_regressor_l10 = LGBMRegressor(objective='quantile',
                           alpha = 0.1,
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

_regressor_l30 = LGBMRegressor(objective='quantile',
                           alpha = 0.3,
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

_regressor_l50 = LGBMRegressor(objective='quantile',
                           alpha = 0.5,
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

_regressor_l70 = LGBMRegressor(objective='quantile',
                           alpha = 0.7,
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

_regressor_l90 = LGBMRegressor(objective='quantile',
                           alpha = 0.9,
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

_regressor_mean = LGBMRegressor(objective='regression',
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           fraction_feature = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

def to_codes(df):
    return df.apply(lambda col: col.cat.codes)

def Imputer_df(df: pd.DataFrame, cols=None, strategy='mean'):
    # wrapper around Imputer allowing use with pandas.DataFrame
    df = df.copy()
    imp = Imputer(strategy=strategy)
    if cols is None:
        cols = list(df.columns)  # BEWARE: if nothing passed, apply on ALL columns
    df_array = imp.fit(df[cols]).transform(df[cols])
    df.loc[:, cols] = df_array
    return df


_knn = Pipeline([
    ('imputeLatLong_byCity', GroupByImputer(
        strategy='median',
        by='VILLE_BSQ',
        cols_to_impute=_knn_cols + ['LATTITUDE_FRESH', 'LONGITUDE_FRESH'])),
    ('reimpute_latlong', FunctionTransformer(
        Imputer_df,
        kw_args={'cols': _knn_cols + ['LATTITUDE_FRESH', 'LONGITUDE_FRESH'],
                 'strategy': _strategy},
        validate=False)),
    (f'NN_{_strategy}', GeoKNN(k=30,
                               latlong=['LATTITUDE_FRESH', 'LONGITUDE_FRESH'],
                               metric='l2',
                               weights='uniform',
                               target=_knn_cols))
])


model_l10 = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_l10)
])

model_l30 = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_l30)
])

model_l50 = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_l50)
])

model_l70 = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_l70)
])

model_l90 = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_l90)
])

model_mean = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            #('dbg', Debug()),
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('prev_price', PrevPrice())
    ])),
    ('regression', _regressor_mean)
])

params = {
    'log_regression': True,
    'fit_params': {
        # indices of categorical columns (encoding performed by LGBM):
        'regression__categorical_feature': list(range(len(_categorical_features)))
    }
}
