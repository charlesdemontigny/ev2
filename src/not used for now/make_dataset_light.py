#!/usr/bin python3
# -*- coding: utf-8 -*-

import datetime
import os
from pathlib import Path

import click
import numpy as np
import pandas as pd

from var import ignored_columns, ignored_dataset_stumps, rename_columns
from make_dataset import import_datasets, preprocess_transaction, preprocess_immeuble
from make_dataset import preprocess_geocodes, preprocess_annotations, preprocess_eval, merge_datasets

@click.group()
def cli():
    pass

@cli.command('new', help="Create a new train and test dataset from raw building, transcation and evaluation files")
@click.option('-i', '--input-path', default='./data/raw/2018-04-05',
              help='Path of the directory containing the raw data')
@click.option('-o', '--output-path', default=None,
              help='Output directory path (inferred from input data by default)')
def make_dataset(input_path, output_path):
    """
    Loads and merges the raw datasets found in input_path
    This directory should contain .bz2 or .txt files
    The filenames should remain constant, otherwise the variables in var.py
    should be modified accordingly.
    """

    if output_path is None:
        output_path = './data/interim/{}/'.format(Path(input_path).name)

    # make sure the output folder exists
    os.makedirs(output_path, exist_ok=True)

    input_file_to_read = [
        'annotation_photo',
        'evaluation',
        'immeuble_merged',
        'transaction_merged',
        'geo_codes'
    ]

    print(f"Loading raw datasets ({', '.join(input_file_to_read)})...")

    d = import_datasets(input_path, datasets=input_file_to_read)
    print("Pre-Processing the raw datasets...")

    tx_pp = preprocess_transaction(d['transaction_merged'])
    imm_pp = preprocess_immeuble(d['immeuble_merged'])
    gc_pp = preprocess_geocodes(d['geo_codes'])
    annot_pp = preprocess_annotations(d['annotation_photo'])
    ev_pp = preprocess_eval(d['evaluation'])

    del d

    print("Merging transaction, building, geocode and image annotation data...")

    # merging the first 4 datasets,
    # please note the subtle change of key names w.r.t. filenames
    mgd = merge_datasets({'transaction': tx_pp,
                          'immeuble': imm_pp,
                          'geocodes': gc_pp,
                          'annotation_photo': annot_pp})
    # NOTE: 'geo_codes' file is always implicitly loaded.

    del tx_pp, imm_pp, gc_pp, annot_pp
    ev_pp.to_pickle(os.path.join(output_path, f"eval.pkl"))
    mgd.to_pickle(os.path.join(output_path, f"mgd.pkl"))
    del ev_pp, mgd
    return




if __name__ == '__main__':
    cli(obj={})
