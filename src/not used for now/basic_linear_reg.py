# -*- coding: utf-8 -*-

from sklearn.linear_model import LinearRegression  # , Lasso, Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer, StandardScaler

from preprocessing.feature_engineering import AddSimpleFeatures, SelectColumns

params = {
    'log_regression': True
}

_reg_cols = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements',
             'cur_eval_valeur_batisse', 'N_etages', 'tx_year', 'Lot_Superficie']

_reg_model = LinearRegression(fit_intercept=True)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)


model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('basic_numerical_features', SelectColumns(_reg_cols)),
    ('fill_na_zero', Imputer(strategy="mean")),
    ('scaling', StandardScaler()),
    ('LinearRegression', _reg_model)]
)
