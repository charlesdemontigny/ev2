# -*- coding: utf-8 -*-
from collections import OrderedDict

import pandas as pd
from lightgbm import LGBMRegressor
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import FunctionTransformer, Imputer

from preprocessing.feature_engineering import (AddSimpleFeatures, GeoKNN,
                                               GroupByImputer, MaxCap,
                                               PrevPrice, SelectColumns,
                                               UnitConverter)

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'N_etages',
                       'first_prev_eval_valeur_batisse', 'cur_eval_valeur_lot', 'Longitude_Fresh',
                       'first_prev_eval_valeur_lot', 'Lattitude_Fresh', 'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

_knn_cols = ['Superficie', 'Lot_Superficie', 'Annee_Const',
             'N_etages', 'Nb_Logements',
             'first_prev_eval_valeur_batisse', 'first_prev_eval_valeur_lot',
             'cur_eval_valeur_batisse', 'cur_eval_valeur_lot']

_categorical_features = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10),
    ('PRO_VOIE_C', 10),
])

_strategy = 'mean'

_regressor = LGBMRegressor(objective='regression',
                           learning_rate=0.1,
                           num_leaves = 63,
                           n_estimators=200,
                           silent=False,
                           random_state=1234, n_jobs=-1)

def to_codes(df):
    return df.apply(lambda col: col.cat.codes)


def Imputer_df(df: pd.DataFrame, cols=None, strategy='mean'):
    # wrapper around Imputer allowing use with pandas.DataFrame
    df = df.copy()
    imp = Imputer(strategy=strategy)
    if cols is None:
        cols = list(df.columns)  # BEWARE: if nothing passed, apply on ALL columns
    df_array = imp.fit(df[cols]).transform(df[cols])
    df.loc[:, cols] = df_array
    return df


_knn = Pipeline([
    ('imputeLatLong_byCity', GroupByImputer(
        strategy='median',
        by='Ville_BSQ',
        cols_to_impute=_knn_cols + ['Lattitude_Fresh', 'Longitude_Fresh'])),
    ('reimpute_latlong', FunctionTransformer(
        Imputer_df,
        kw_args={'cols': _knn_cols + ['Lattitude_Fresh', 'Longitude_Fresh'],
                 'strategy': _strategy},
        validate=False)),
    (f'NN_{_strategy}', GeoKNN(k=30,
                               latlong=['Lattitude_Fresh', 'Longitude_Fresh'],
                               metric='l2',
                               weights='uniform',
                               target=_knn_cols))
])


    model = Pipeline([
        ('converting_unit', FunctionTransformer(
            UnitConverter,
            kw_args={'cols_to_convert': ['Lot_Superficie'],
                     'unit_col': ['Unite_Aire'],
                     'unit': 'PI2'},
            validate=False)
         ),
        ('lot_superf_cap', MaxCap(cols_to_cap=['Lot_Superficie'], max_cap=10000)),
        ('union', FeatureUnion([
            ('cat_features', Pipeline([
                ('select_cat', SelectColumns(_categorical_features)),
                ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
            ])),
            ('numerical_cols', Pipeline([
                ('simple_features', AddSimpleFeatures()),
                ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
            )),
            ('pipe_latlong', _knn),
            ('prev_price', PrevPrice())
        ])),
        ('regression', _regressor)
    ])

params = {
    'log_regression': True,
    'fit_params': {
        # indices of categorical columns (encoding performed by LGBM):
        'regression__categorical_feature': list(range(len(_categorical_features)))
    }
}
