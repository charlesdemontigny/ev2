#!/usr/bin python3
# -*- coding: utf-8 -*-

import datetime
import os
from pathlib import Path

import click
import numpy as np
import pandas as pd

from var import ignored_columns, ignored_dataset_stumps, rename_columns
from make_dataset import fix_dtypes, merge_eval


@click.command()
@click.option('-i', '--input-path', default='./data/interim/2018-04-05',
              help='Path of the directory containing the raw data')
@click.option('-o', '--output-path', default=None,
              help='Output directory path (inferred from input data by default)')
@click.option('-th', '--thresh-yr', default=2015, type=int,
              help='Train-Test split year')
def make_dataset(input_path, output_path, thresh_yr):
    """
    Loads and merges the raw datasets found in input_path
    This directory should contain .csv or .txt files
    The filenames should remain constant, otherwise the variables in var.py
    should be modified accordingly.
    This is the second part of the function since there was memory problem doing
    everything at once.
    """

    if output_path is None:
        output_path = './data/processed/{}/'.format(Path(input_path).name)

    # Load both datasets
    print("Load evaluation...")
    ev_pp = pd.read_pickle(os.path.join(input_path, f"eval.pkl"))
    print("Load merged file...")
    mgd = pd.read_pickle(os.path.join(input_path, f"mgd.pkl"))

    # Merge both datasets
    print("Merge both files together...")
    mgdeval = merge_eval(mgd, ev_pp)
    del mgd, ev_pp

    # Fix types
    print("Verifying columns data types...")
    print("La taille du df")
    #print(mgdeval.shape)
    mgd_final = fix_dtypes(mgdeval)  # cast the columns into proper dtypes
    #del mgdeval

    # save training set
    print("here is the list of columns currently in mgd_final : \n", mgd_final.columns)
    mgd_final[mgd_final.Date_Tx.dt.year <= thresh_yr].reset_index(drop=True).to_pickle(
        os.path.join(output_path, f"train_{int(thresh_yr)}.pkl"))

    # save test set
    print("saving test_dataset at " + output_path)
    mgd_final[mgd_final.Date_Tx.dt.year > thresh_yr].reset_index(drop=True).to_pickle(
        os.path.join(output_path, f"test_{int(thresh_yr)}.pkl"))

    return


if __name__ == '__main__':
    make_dataset()
