# -*- coding: utf-8 -*-

from sklearn.linear_model import LinearRegression  # , Lasso, Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import (Imputer, StandardScaler, FunctionTransformer)

from preprocessing.feature_engineering import (AddSimpleFeatures, SelectColumns, UnitConverter, MaxCap)

params = {
    'log_regression': True
}

_reg_cols = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements',
             'cur_eval_valeur_batisse', 'N_etages', 'tx_year', 'Lot_Superficie']

_reg_model = LinearRegression(fit_intercept=True)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)
# _reg_model = Lasso(alpha=0.01, fit_intercept=True, random_state=1234)


model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('converting_unit',
        FunctionTransformer(
            UnitConverter,
            kw_args={'cols_to_convert': ['Lot_Superficie'],
                     'unit_col': 'Unite_Aire',
                     'unit': 'PI2'},
            validate=False)
     ),

    ('superf_cap', MaxCap(cols_to_cap='Superficie', percentile=99)),
    ('lot_superf_cap', MaxCap(cols_to_cap='Lot_Superficie', max_cap=10000)),
    ('select', SelectColumns(_reg_cols)),
    ('fill_na_zero', Imputer(strategy="mean")),
    ('scaling', StandardScaler()),
    ('LinearRegression', _reg_model)]
)
