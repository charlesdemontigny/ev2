# -*- coding: utf-8 -*-

from collections import OrderedDict

from sklearn.pipeline import FeatureUnion, Pipeline
from xgboost import XGBRegressor
from sklearn.preprocessing import (Imputer, StandardScaler, FunctionTransformer)
from preprocessing.feature_engineering import (AddSimpleFeatures,
                                               DummyEncodeCategoricals,
                                               SelectColumns, UnitConverter, MaxCap)

params = {
    'log_regression': True
}

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'N_etages', 'Longitude_Fresh', 'Lattitude_Fresh',
                       'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

_categorical_features = OrderedDict([
    ('CLASSE_BAT', 10),
    ('Categorie_Batiment', 10),
    ('TYPE', 10),
    ('PRO_VOIE_C', 10)
])

_xgb = XGBRegressor(n_estimators=200,
                    learning_rate=0.1,
                    max_depth=8,
                    reg_lambda=1,  # L2 leaf regularization
                    reg_alpha=0,  # L1 leaf regularization
                    gamma=0,  # minimum loss reduction required to make a further partition on a leaf node of the tree.
                    objective="reg:linear",  # optimize for RMSE
                    seed=1234, nthread=3)


model = Pipeline([
    ('converting_unit',
        FunctionTransformer(
            UnitConverter,
            kw_args={
                'cols_to_convert': ['Lot_Superficie'],
                'unit_col': 'Unite_Aire',
                'unit': 'PI2'},
            validate=False)
     ),

    ('superf_cap', MaxCap(cols_to_cap='Superficie', percentile=99)),
    ('lot_cap', MaxCap(cols_to_cap='Lot_Superficie', max_cap=10000)),
    ('union', FeatureUnion([
        ('categories', DummyEncodeCategoricals(_categorical_features)),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        ))
    ])),
    ('regression', _xgb)
]
)
