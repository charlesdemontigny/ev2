# -*- coding: utf-8 -*-
"""
Date de creation: 2018-05-30
@uteur: Charles Demontigny
Titre: Creer l'input pour la prevision
"""

# Import packages --------------------------------------------------------------
import pandas as pd
import numpy as np
import cx_Oracle
import os
from pathlib import Path
from datetime import datetime

# Helper function --------------------------------------------------------------
def create_output_path():
    import datetime

    # Create variables
    year = str(datetime.datetime.now().year)
    month = str(datetime.datetime.now().month)
    day = str(datetime.datetime.now().day)

    if len(month) == 1:
        month = "0" + month
    if len(day) == 1:
        day = "0" + day

    return "./data/processed/{}/input".format(Path(year + "-" + month + "-" + day).name)

# Prepare input for final export -----------------------------------------------

def prepare_input(target_date = None, light = False, query = None):
        """
        Variant of `make_dataset`, used to create an input features dataset for the final
        price estimation export. Instead of using real transactions, we generate hypothetical
        transactions (one transaction per building on the target date).

        Parameters
        ----------
        target_date: str
            Date of the hypothetical transactions to be generated for each building.

        Notes
        -----
        - The output datasets will note no 'Comptant' and 'Montant' columns.
        - The output datasets will contains one line per building appearing in the input building
          dataset. There is no filtering (old non-existant building may be used).
        """

        # Create target date variable if necessary
        if target_date is None:
            target_date = datetime.now()

        # Output path
        output_path = create_output_path()
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        # Load data --------------------------------------------------------------------
        print("Loading dataset from Oracle...")
        # Setup connection
        conn = cx_Oracle.connect('jlr_website', '1qaz2wsx', 'jlrorap1relv', encoding = 'utf-8')

        # Create query
        if query is None:
            query = """
                    select JLR_EVIA_PREDINPUT.*, tra_mnt_transaction as prec_price
                    from JLR_EVIA_PREDINPUT
                    left join tra_transaction on prec_no = tra_numero
                    where TX_EXCLUE = 0
                    and Categorie_Batiment in ('2A', '3C', '2B', '2C')
                    """

        # Load dataset into df
        df = pd.read_sql(query.encode('utf-8'), con = conn)

        # Propress -------------------------------------------------------------
        df['DATE_TX'] = pd.to_datetime(df.DATE_TX, format='%Y%m%d')
        df['DATE_CREATED'] = pd.to_datetime(df.DATE_CREATED, format='%Y%m%d')
        df['DATE_MODIFIED'] = pd.to_datetime(df.DATE_MODIFIED, format='%Y%m%d')
        df[['COMPTANT', 'TX_ID', 'MONTANT']] = df[['COMPTANT', 'TX_ID', 'MONTANT']].astype(float)
        try:
            df[['TX_ID', 'HOME_ID', 'VENTE_INTRA_FAMILLE']] = df[['TX_ID', 'HOME_ID', 'VENTE_INTRA_FAMILLE']].astype(np.int64)
        except ValueError:
            df[['HOME_ID']] = df[['HOME_ID']].astype(np.int64)

        # Fix le dtype ------------------------------------------------------------------
        print("Modification des dtypes...")
        numeric_cols = ['MONTANT', 'EVIADISPO', 'LONGITUDE', 'LATTITUDE', 'TX_TYPE',
                        'TX_EXCLUE', 'TX_ID', 'VENTE_MULTIPLE', 'ACHAT_SANS_HYPOT',
                        'CODE_VENTE_TAXABLE', 'VILLE_BSQ', 'LOT_SUPERFICIE', 'PRO_CIVIC1',
                        'NB_LOGEMENTS', 'UNITE_AIRE_CODE', 'SECTEUR', 'N_ETAGES',
                        'ANNEE_CONST', 'SUPERFICIE', 'CODE_UTILISATION', 'LOT_LARGEUR',
                        'MUR_FACADE_ID', 'ABRI_AUTO', 'ETAGE', 'PLACE_GARAGE', 'LOT_PROFONDEUR',
                        'N_COMMERCES', 'PLACE_ABRI_AUTO', 'PHOT_AVEC_VISAGE', 'ARRONDISSEMENT_ID',
                        'PRO_CIVIC2', 'CONSTRUCTION_NEUVE', 'VENTE_INTRA_FAMILLE',
                        'THIRD_PREV_EVAL_VALEUR_BATISSE', 'THIRD_PREV_EVAL_VALEUR_LOT',
                        'SECOND_PREV_EVAL_VALEUR_BATISSE', 'SECOND_PREV_EVAL_VALEUR_LOT',
                        'FIRST_PREV_EVAL_VALEUR_BATISSE', 'FIRST_PREV_EVAL_VALEUR_LOT',
                        'CUR_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT', 'COMPTANT']

        categorical_cols = ['ARRONDISSEMENT_NOM', 'CAT_BAT_DESC', 'CODE_UTILISATION_TXT',
                            'VILLE', 'PAROISSE', 'DESC_VENTE_TAXABLE', 'NOM_SECTEUR',
                            'PRO_VOIE', 'CODE_POSTAL', 'TX_TYPE_DESC', 'VENTE_TYPE',
                            'DESCRIPTION_CLASSE', 'CATEGORIE_BATIMENT', 'PRO_VOIE_C',
                            'UNITE_AIRE', 'CLASSE_BAT', 'TYPE', 'MUR_FACADE', 'PRO_VOIE_L',
                            'PRO_VOIE_O', 'PRO_LOT_ORIG_TYPE', 'PRO_SUFFIXE2', 'PRO_SUFFIXE1',
                            'GARAGE']

        datetime_cols = {'DATE_CREATION_ENREG': '%Y%m%d',
                         'DATE_MODIF_ENREG': '%Y%m%d',
                         'DATE_MODIFIED': '%Y-%m-%d',
                         'DATE_CREATED': '%Y-%m-%d'}

        for col in numeric_cols:
            print(f'converting: {col}')
            if col in df.columns:
                df[col] = pd.to_numeric(df[col], errors='raise')

        for col in categorical_cols:
            print(f'converting: {col}')
            if col in df.columns:
                df[col] = pd.Categorical(df[col])

        for col, date_format in datetime_cols.items():
            print(f'converting: {col}')
            if col in df.columns:
                df[col] = pd.to_datetime(df[col], format=date_format)

        # Create input DataFrame -----------------------------------------------
        input_df = df.sort_values(['HOME_ID', 'DATE_TX'])
        input_df.drop_duplicates('HOME_ID', keep = 'last', inplace = True)

        input_df['DATE_TX'] = target_date
        input_df['TX_ID'] = -1
        input_df['VENTE_MULTIPLE'] = 0
        input_df['CODE_VENTE_TAXABLE'] = 0
        input_df['DESC_VENTE_TAXABLE'] = "NE S'APPLIQUE PAS"
        input_df['CONSTRUCTION_NEUVE'] = np.nan
        input_df['SECTEUR'] = np.nan
        input_df['NOM_SECTEUR'] = "--Not Generated--"

        # Export input_df
        if not light:
            output_file = Path(output_path) / f"features_{target_date.strftime('%Y-%m-%d')}.feather"
            print("Output file: ", output_file)

            input_df.reset_index(drop = True).to_feather(output_file)

        if light:
            return input_df


if __name__ == "__main__":
    prepare_input()
