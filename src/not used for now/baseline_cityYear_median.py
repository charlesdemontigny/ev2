# -*- coding: utf-8 -*-

from sklearn.pipeline import Pipeline
from preprocessing.feature_engineering import AddSimpleFeatures
from models.median_multilevel import MedianByCityYear

params = {
    'log_regression': True
}

model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('yearlyMedianByCity', MedianByCityYear())])
