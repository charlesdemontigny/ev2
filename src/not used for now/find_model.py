# Helper function
def find_model(model, model_path):

    try:
        files = os.listdir(model_path)
        i = 0
        startswith = False
        while not startswith:
            if files[i].startswith(model):
                model_name = files[i]
                startswith = True
            i += 1

        model_code = re.sub(".meta.json", "", model_name)
        output = "".join([model_path, "/", model_code])

        return output

    except FileNotFoundError:

        output = None
        return output
