# -*- coding: utf-8 -*-

from catboost import CatBoostRegressor

from models.median_multilevel import MedianByCityYear

from sklearn.pipeline import FeatureUnion, Pipeline

from preprocessing.feature_engineering import (AddSimpleFeatures, AsTypeDF,
                                               FillNaDF, PrevPrice,
                                               SelectColumns)

_train_on_GPU = False  # Requires CUDA8, NOTE: enabling GPU training may impact model scores

_categorical_features = ['CLASSE_BAT', 'Categorie_Batiment', 'TYPE', 'Arrondissement_ID',
                         'Ville_BSQ', 'Code_Postal', 'Annee_Const', 'Lot_Renove', 'PRO_SUIT1',
                         'PRO_VOIE_C', 'PRO_VOIE_O', 'Code_Utilisation', 'PRO_CIVIC1',
                         'PRO_LOT_ORIG_TYPE', 'PRO_SUFFIXE2']

_numerical_features = ['Annee_Const', 'Superficie', 'Code_Utilisation', 'Nb_Logements', 'N_commerces',
                       'cur_eval_valeur_batisse', 'N_etages',
                       'Longitude_Fresh', 'Lattitude_Fresh', 'Lot_Superficie']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['tx_year', 'cur_date_eval_year']

_med_price = MedianByCityYear()

_preprocessing = FeatureUnion([
    ('cat_features', Pipeline([
        ('select_cat', SelectColumns(_categorical_features)),
        ('cat_to_str', AsTypeDF(str)),
        ('fillna_cat', FillNaDF('_NA')),
    ])),
    ('median_price', _med_price),
    ('numerical_cols', Pipeline([
        ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng)),
        ('replace_nan', FillNaDF(-1000))
        # Note: Here we assumes that missing values are not randomly distributed in the dataset and
        # that the fact that a value is missing is itself an useful information for the regression.
        # Instead of performing univariate (mean/mode) or multivariate (KNN, etc.) imputation,
        # we replace missing values with an out of domain value (ex: large negative value). If
        # missing values truly have a predive power the boosted decision trees should learn
        # region boundaries that take into accounts missing values.
        #
        # Ideally this assumption should be tested, and they may be better ways to handle missing
        # values (eg: ternary decision trees instead of binary decision trees). Also we may want
        # to weight traning samples depending on their proportion of missing values. However, here just
        # replacing NAN with a negative values is the quickest way to alleviate the effects of missing
        # values and does not seems to performs worse than imputation by mean.
    ])),
    ('prev_price', PrevPrice())
])

# See "Training parameters" : https://tech.yandex.com/catboost/doc/dg/concepts/python-reference_parameters-list-docpage/
_catboost = CatBoostRegressor(
    iterations=200, learning_rate=0.2, depth=6, l2_leaf_reg=10,
    loss_function='RMSE',
    eval_metric='RMSE',  # for display and early stopping only,
    one_hot_max_size=2,  # Max nb of categories for one-hot encoding (otherwise ctr encoding is used)
    border_count=128,  # number of splits for numerical features binarization
    rsm=1.,  # The percentage of features to use at each split selection
    random_seed=1234,
    task_type='GPU' if _train_on_GPU else 'CPU',
    allow_writing_files=False)

params = {
    'log_regression': True,
    'fit_params': {
        # Indices of categorical columns (ctr or dummy encoding performed by  by catboost)
        # This list of columns indices is required for catboost to properly encode categorical
        # variables. See catboost CTR encoding method:
        # https://tech.yandex.com/catboost/doc/dg/concepts/algorithm-main-stages_cat-to-numberic-docpage/
        'catboost__cat_features': list(range(len(_categorical_features)))
    }
}

model = Pipeline([
    ('simple_features', AddSimpleFeatures()),
    ('preprocessing', _preprocessing),
    ('catboost', _catboost)
])
