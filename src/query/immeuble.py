query0 = """
SELECT DISTINCT
           a.pro_no_id_uef,
           pro_civic1,
           UPPER (pro_suffixe1)                    pro_suffixe1,
           pro_civic2,
           UPPER (pro_suffixe2)                    pro_suffixe2,
           UPPER (pro_voie_c)                      pro_voie_c,
           UPPER (pro_voie_l)                      pro_voie_l,
           TRANSLATE (UPPER (pro_voie),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               pro_voie,
           UPPER (pro_voie_o)                      pro_voie_o,
           UPPER (pro_suit1)                       pro_suit1,
           TRANSLATE (UPPER (pro_postal),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               pro_postal,
           pro_nblogres,
           pro_cubf,
           TRANSLATE (UPPER (cubf.cge_description),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               cubf_desc,
           TRANSLATE (UPPER (cat_bat.cge_description),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               cat_bat_desc,
           SUBSTR (c.pro_no_lot_renove, 1, 12)     pro_lot_renove,
           c.pro_div_cad,
           TRANSLATE (
               UPPER (
                      pac_type
                   || DECODE (SUBSTR (pac_type, LENGTH (pac_type)),
                              '''', '',
                              ' ')
                   || pac_nom),
               'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
               'AAAAEEEEIIOOUUUC')
               pac_nom,
           UPPER (c.pro_lot_orig)                  pro_lot_orig,
           UPPER (c.pro_no_lot_subd)               pro_lot_subd,
           UPPER (pro_abrev_lot_orig_type)         pro_abrev_lot_orig_type,
           UPPER (c.pro_lot_orig_type)             pro_lot_orig_type,
           pro_lot_supe,
           pro_lot_fron,
           pro_lot_prof,
           TRIM (
                  SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          1,
                          4)
               || ' '
               || SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          5,
                          2)
               || ' '
               || SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          7,
                          4)
               || ' '
               || SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          11,
                          1)
               || ' '
               || SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          12,
                          3)
               || ' '
               || SUBSTR (RPAD (REPLACE (pro_matricule, '-', ''), 18, '0'),
                          15))
               pro_matricule,
           pro_bsq,
           TRANSLATE (UPPER (mun_nom_municipalite),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               mun_nom_municipalite,
           pro_nbetages,
           pro_autres_loc,
           PRO_AIRE_ETAGE pro_superf_bat,
           PRO_UM_AIRE_ETAGE pro_um,
           TRANSLATE (UPPER (um.cge_description),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               um_desc,
           x                                       pro_x,
           y                                       pro_y,
           pro_arv_id,
           TRANSLATE (UPPER (arv_description),
                      'ÀÂÄÄÉÈÊËÎÏÔÖÙÛÜÇ',
                      'AAAAEEEEIIOOUUUC')
               arv_description,
           TO_CHAR (pro_date_created, 'yyyymmdd')  pro_date_created,
           TO_CHAR (pro_date_modified, 'yyyymmdd') pro_date_modified
      FROM prospect        a,
           id_uef_x_y      b,
           prospect_lot    c,
           codes_generaux  secteur,
           codes_generaux  cat_bat,
           codes_generaux  um,
           codes_generaux  cubf,
           arv_arrondissement_ville,
           municipalites_bsq,
           pac_paroisse_cad

     WHERE     a.pro_no_id_uef = b.pro_no_id_uef(+)
           AND a.pro_no_id_uef = c.pro_lot_id_uef(+)
           AND to_number_cb (secteur.cge_code) = pro_secteur
           AND secteur.cge_numero_table = 24
           AND cat_bat.cge_code = pro_cat_bat
           AND cat_bat.cge_numero_table = 1
           AND cubf.cge_code(+) = pro_cubf
           AND cubf.cge_numero_table(+) = 27
           AND um.cge_code(+) = pro_um
           AND um.cge_numero_table(+) = 29
           AND pro_arv_id = arv_id(+)
           AND mun_no_bsq = pro_bsq
           AND to_number_cb (c.pro_div_cad) = pac_code(+)
           and pro_cat_bat in ('2A','2B','2C','3C')
"""

query1 = """
SELECT DISTINCT(TRA_ID_UEF) AS PRO_NO_ID_UEF,TRA_CAT_BAT AS PRO_CAT_BAT, IMM_AN_CONSTRUCTION AS PRO_D_REEL
FROM TRA_TRANSACTION
WHERE TRA_CAT_BAT IN ('2A','2B','2C','3C')
"""
