query = """
SELECT TRA_TRANSACTION.TRA_ID_UEF AS ID_UEF,
        TRA_TRANSACTION.TRA_MNT_TRANSACTION AS MONTANT,
        TO_CHAR(TRA_TRANSACTION.TRA_DT_PUBLIE, 'YYYYMMDD') AS DT_TRAN,
        TRA_TRANSACTION.TRA_NUMERO AS NUMERO,
        CASE
               WHEN    TRA_TRANSACTION.TRA_IND_VENTE_LIE = 1
                    OR TRA_TRANSACTION.TRA_IND_VENTE_REPRISE = 1
                    OR TRA_TRANSACTION.TRA_IND_VENTE_MULTIPLE = 1
                    OR TRA_TRANSACTION.TRA_PCT_TRANSFERT <> 100
                    OR TRA_TRANSACTION.tra_mnt_transaction <= 5000
               THEN
                   1
               ELSE
                   0
           END
               TRA_EXCLUE,
        TRA_TYPE AS T_TYPE,
        TRA_IND_VENTE_MULTIPLE,
        TRA_NOUVELLE_CONSTRUCTION AS CONSTRUCTION_NEUVE,
        ACL_VENTE_LIE, ACL_TYPE_ORIGINAL,

        CASE
               WHEN     t_type = 4
                    AND dt_tran >= '20140101'
                    AND NOT EXISTS
                            (SELECT 1
                               FROM hypotheque_vente
                              WHERE NUMEROVENTE = acl_numero_lng)
               THEN
                   1
               ELSE
                   0
           END
               achat_sans_hyp,

        TRANSAC.COMPTANT AS COMPTANT,
        TO_CHAR(TRANSAC.DATE_MODIFIED, 'YYYYMMDD') AS DATE_MODIFIED,
        TO_CHAR(TRANSAC.DATE_CREATED, 'YYYYMMDD') AS DATE_CREATED,
        PROSPECT_RECH.PRO_SECTEUR AS PRO_SECTEUR,
           NVL (ACL_VENTE_TAXABLE_INDIC, 0)    CODE_VENTE_TAXABLE,
           DECODE (NVL (ACL_VENTE_TAXABLE_INDIC, 0),
                   0, 'NE S''APPLIQUE PAS',
                   1, 'VENTE TAXABLE',
                   2, 'EXONERE',
                   3, 'FORT PROBABLEMENT TAXABLE')
               desc_vente_taxable

FROM TRA_TRANSACTION

LEFT JOIN ACL_ACTE_LIVRE on TRA_TRANSACTION.TRA_NUMERO = ACL_ACTE_LIVRE.ACL_NUMERO_LNG
LEFT JOIN TRANSAC on TRA_TRANSACTION.TRA_NUMERO = TRANSAC.NUMERO
LEFT JOIN PROSPECT_RECH on TRA_TRANSACTION.TRA_ID_UEF = PROSPECT_RECH.PRO_NO_ID_UEF

WHERE TRA_TYPE = 4
AND TRA_CAT_BAT IN ('2A','2B', '2C','3C','2G','3A','2I','2D','2F','3B')
"""
