query = """
SELECT a.GEO_ID_UEF, geo_geocodes.GEO_X_LNG, geo_geocodes.GEO_Y_LAT
FROM GEO_GEOCODES
LEFT JOIN (SELECT MAX(GEO_ACCURACY) as maxAccuracy, GEO_ID_UEF
FROM GEO_GEOCODES
GROUP BY GEO_ID_UEF) a ON geo_geocodes.GEO_ID_UEF = a.geo_id_uef
WHERE GEO_ACCURACY = maxAccuracy
"""
