query = """
SELECT hev_id_uef,
           hev_valeur_t,
           hev_valeur_b,
           hev_annee_deb,
           hev_annee_fin,
           TO_CHAR (hev_date_created, 'yyyymmdd')  hev_date_created,
           TO_CHAR (hev_date_modified, 'yyyymmdd') hev_date_modified
      FROM his_eval, prospect_rech
      where pro_no_id_uef = hev_id_uef and pro_cat_bat in ('2A','2B','2C','3C')
"""
