# -*- coding: utf-8 -*-

from sklearn.base import BaseEstimator
import numpy as np
import pandas as pd

class MedianByRTA(BaseEstimator):
    '''Return the median house price by city (over all training dataset).
    Fallback to the global median price if the city is unknown.

    Required features : 'Ville_BSQ'
    '''

    def __init__(self):
        pass

    def fit(self, x, y):
        x = x.copy()
        x['y'] = y
        x['RTA'] = x.Code_Postal.str.strip().str[:3].astype(str)
        self._city_lookup = pd.DataFrame(columns = ['y'])  # x.groupby(['RTA']).y.mean().dropna(axis=0)
        self._fallback = np.mean(y)  # possiblement groupby(city)
        return self

    def transform(self, x):
        x = x.copy()
        x['RTA'] = x.Code_Postal.str.strip().str[:3].astype(str)

        city = x[['RTA']].set_index(['RTA'])
        temp = city.join(self._city_lookup)
        print('count of missings in transform dataset', temp.shape[0], temp.y.isna().mean())
        return temp.fillna(self._fallback) \
            .reset_index(drop=True).values

    def fit_transform(self, x, y):
        self.fit(x, y)
        return self.transform(x)

    def predict(self, x):
        return self.transform(x).ravel()

    def fit_predict(self, x, y):
        return self.fit_transform(x, y).ravel()
