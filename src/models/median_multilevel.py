# -*- coding: utf-8 -*-

from sklearn.base import BaseEstimator
import numpy as np


class MedianByCityYear(BaseEstimator):
    '''Return the median house price by city (over all training dataset).
    Fallback to the global median price if the city is unknown.

    Required features : 'Ville_BSQ'
    '''

    def __init__(self):
        pass

    def fit(self, x, y):
        x = x.copy()
        x['y'] = y
        x['tx_year'] = x.DATE_TX.astype(str).str.strip().str[:4]
        self._city_lookup = x.groupby(['CATEGORIE_BATIMENT', 'DRIDU', 'tx_year']).y.median().dropna(axis=0)
        self._fallback = np.median(y)  # possiblement groupby(city)
        return self

    def transform(self, x):
        x = x.copy()
        x['tx_year'] = x.DATE_TX.astype(str).str.strip().str[:4]

        city = x[['CATEGORIE_BATIMENT', 'DRIDU', 'tx_year']].set_index(
                                    ['CATEGORIE_BATIMENT', 'DRIDU', 'tx_year'])
        temp = city.join(self._city_lookup)
        print('count of missings in transform dataset', temp.shape[0], temp.y.isna().mean())
        x['median_by_dr'] = temp.fillna(self._fallback).reset_index(drop=True).values
        return x

    def fit_transform(self, x, y):
        self.fit(x, y)
        return self.transform(x)

    def predict(self, x):
        return self.transform(x).ravel()

    def fit_predict(self, x, y):
        return self.fit_transform(x, y).ravel()
