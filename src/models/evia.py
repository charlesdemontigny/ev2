# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype
from sklearn.base import BaseEstimator

_DEFAULT_EVIA_COLUMNS = {
    'IDUEF': 'home_id',
    'NO_VENTE': 'tx_id',
    'EVIAFINAL': 'pred',
    'INDICE_CONFIANCE': 'confidence'
}


class EviaHistoricalPrediction(BaseEstimator):
    """Regressor that returns historical EVIA price estimations
     (loaded from a dataset).

     Returned predictions will contains NaNs for building without
     EVIA reports.

     Parameters
     ----------
        evia_dataset_path: str
            Path of evia historical prediction dataset.
        sep: str
            Column separator in the evia historical prediction dataset.
        encoding : str
            Evia historical prediction dataset file encoding.
        evia_columns: Dict[str, str]
            Evia historical prediction columns name mapping (see `_DEFAULT_EVIA_COLUMNS`)
        verbose: int
            If > 0, print some quality and diagnostic information.
     """

    def __init__(self, evia_dataset_path: str, sep='~', encoding='ISO-8859-1',
                 evia_columns=_DEFAULT_EVIA_COLUMNS, verbose=0):
        self.evia_dataset_path = evia_dataset_path
        self.sep = sep
        self.encoding = encoding
        self.evia_columns = evia_columns
        self.verbose = verbose

    def fit(self, x, y):

        evia = pd.read_csv(self.evia_dataset_path, sep=self.sep, encoding=self.encoding,
                           usecols=list(self.evia_columns.keys()))

        evia = evia.rename(columns=self.evia_columns)

        assert set(evia.columns) == set(_DEFAULT_EVIA_COLUMNS.values())
        assert is_numeric_dtype(evia['pred'])
        assert is_numeric_dtype(evia['confidence'])

        # Drop 0 or NaN predictions:
        evia['pred'] = evia['pred'].replace(0, np.NaN)
        diag_nb_NA_or_zero = evia['pred'].isna().sum()
        evia = evia.dropna(subset=['pred'])

        # Drop duplicates, keeping only the highest confidence predictions.
        # TODO: find a proper way to handle duplicated evia reports
        # Warning: it seems that there are some transaction IDs mapped
        # to more than one building !

        diag_nb_duplicate_reports = evia.duplicated('tx_id', keep='first').sum()
        diag_nb_nb_building_missmatches = (evia.groupby('tx_id')['home_id'].nunique() > 1).sum()

        evia = evia.sort_values(['confidence'], ascending=False).groupby('tx_id').pred.first().reset_index()

        self.evia_ = evia

        assert self.evia_['tx_id'].is_unique
        assert (self.evia_['pred'] > 0).all()

        if self.verbose > 0:
            print(f"Evia reports loaded: {diag_nb_NA_or_zero} NA/0 predictions,"
                  f"{diag_nb_duplicate_reports} duplicate reports,"
                  f"{diag_nb_nb_building_missmatches} report transaction IDs matching more than one building,"
                  f"{len(self.evia_)} remaining reports.")

        return self

    def predict(self, x, tx_id_col='TX_ID'):
        y_pred = x[[tx_id_col]].merge(self.evia_, how='left', left_on=tx_id_col, right_on='tx_id')['pred'].values
        assert len(y_pred) == len(x)
        return y_pred

    def fit_predict(self, x, y):
        self.fit(x, y)
        return self.predict(x)
