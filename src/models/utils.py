# -*- coding: utf-8 -*-

import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin, clone
from sklearn.preprocessing import FunctionTransformer
from sklearn.utils.validation import check_is_fitted


class TransformedTargetRegressor(BaseEstimator, RegressorMixin):
    """Meta-estimator to regress on a transformed target.

    This class is a  simplified version of the TransformTargetRegressor class
    to be released in scikit-learn 0.20.
    See: http://scikit-learn.org/dev/modules/generated/sklearn.preprocessing.TransformedTargetRegressor.html

    Original version (Andreas Mueller and Guillaume Lemaitre, BSD 3 clause):
    https://github.com/glemaitre/scikit-learn/blob/bbee2be7d7321bf8a2f6b7f0129ceb6e57784c14/sklearn/preprocessing/_target.py
    """

    def __init__(self, regressor=None, func=None, inverse_func=None):
        self.regressor = regressor
        self.func = func
        self.inverse_func = inverse_func

    def fit(self, X, y, sample_weight=None, **fit_params):
        """Fit the model according to the given training data."""

        # transformers are designed to modify X which is 2d dimensional, we
        # need to modify y accordingly.
        y_2d = y.reshape(-1, 1)

        self.transformer_ = FunctionTransformer(
            func=self.func, inverse_func=self.inverse_func, validate=False)

        self.transformer_.fit(y_2d)

        self.regressor_ = clone(self.regressor)

        # transform y and convert back to 1d array if needed
        y_trans = self.transformer_.fit_transform(y_2d)

        if y_trans.ndim == 2 and y_trans.shape[1] == 1:
            y_trans = y_trans.squeeze(axis=1)

        if sample_weight is None:
            self.regressor_.fit(X, y_trans, **fit_params)
        else:
            self.regressor_.fit(X, y_trans, sample_weight=sample_weight, **fit_params)

        return self

    def predict(self, X):
        """Predict using the base regressor, applying inverse."""
        check_is_fitted(self, "regressor_")
        pred = self.regressor_.predict(X)
        pred_trans = self.transformer_.inverse_transform(pred.reshape(-1, 1))

        if (pred_trans.ndim == 2 and pred_trans.shape[1] == 1):
            pred_trans = pred_trans.squeeze(axis=1)

        return pred_trans


def log_target_transform(model):
    return TransformedTargetRegressor(model, func=np.log1p, inverse_func=np.expm1)
