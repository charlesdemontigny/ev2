# -*- coding: utf-8 -*-

from sklearn.base import BaseEstimator
import numpy as np


class MedianByCity(BaseEstimator):
    '''Return the median house price by city (over all training dataset).
    Fallback to the global median price if the city is unknown.

    Required features : 'VILLE_BSQ'
    '''

    def __init__(self):
        pass

    def fit(self, x, y):
        x = x.copy()
        x['y'] = y
        self._city_lookup = x.groupby('VILLE_BSQ').y.median()
        self._fallback = np.median(y)
        return self

    def predict(self, x):
        city = x[['VILLE_BSQ']].set_index(['VILLE_BSQ'])

        return city.join(self._city_lookup).fillna(self._fallback) \
            .reset_index(drop=True).values.ravel()

    def fit_predict(self, x, y):
        self.fit(x, y)
        return self.predict(x)
