#!/usr/bin python3
# -*- coding: utf-8 -*-

import datetime
import importlib
import json
import os
import pickle
import time
from pathlib import Path, PurePosixPath

import click
import numpy as np
import pandas as pd

from dataset import filter_dataset, load_dataset, sample_dataset, prepare_dataset
from models.utils import log_target_transform


def pprint_json(dict):
    # Attempt to pretty print dictionnary into a json string:
    print(json.dumps(dict, sort_keys=True, indent=2,
                     default="{0.__class__.__module__}.{0.__class__.__name__}".format))


@click.command()
@click.option('-d', '--dataset', default='2018-06-06',
              help='Name of the dataset to use for training (subdirectory within ./data/processed/)')
@click.option('-m', '--model', default='baseline_city_median',
              help='Model name (name of the python module inside the src/config directory).')
@click.option('-p', '--sampling-prop', default=None, type=float,
              help='Proportion of the dataset ([0; 1] range) to sample for quick testing. Set this to None to disable sampling.')
def train_model_cli(dataset, model, sampling_prop):
    train_model(dataset, model, sampling_prop)

def train_model(df, model, sampling_prop):
    """
    Train model with dataset

    Args:
        df: A pandas DataFrame that contains X, y.
        model: A model with the sklearn architecture.
        sampling_prop: Subset dataset [0, 1].

    Returns: A trained model.

    Exports: Model's metadata.
    """
    model_name = model

    model_out_dir = './models'

    # Split DataFrame into X, y ------------------------------------------------
    X, y = prepare_dataset(df)

    # --------------------------------------------------------------------------
    # if sampling_prop is not None:
    #     click.secho(f"Warning: sampling {sampling_prop:.0%} of the train/valid dataset.", fg='red')
    #     X, y = sample_dataset(X, y, sampling_prop, seed=1234)

    # Important: filter parameters should match parameters used for model-evaluation:
    filter_params = {
        'low_prices_filter': 50000,
        'drop_multiple_house_sales': True,
        'drop_within_family_sales': True,
        'use_code_threshold': 1380,
        'drop_tx_before_year': 2004
    }

    X, y = filter_dataset(X, y, **filter_params)

    assert not np.isnan(y).any()
    assert (y >= 0).all()

    print(f'Filtered dataset: {X.shape[0]} samples, {X.shape[1]} columns.')

    # ----------------------------------
    click.secho(f"\nInitializing {model} model...", bold=True)
    # ----------------------------------

    config = importlib.import_module(f'.{model_name}', 'config')
    model = config.model

    print(f"Regressions parameters:")
    pprint_json(config.params)

    if config.params['log_regression']:
        model = log_target_transform(model)

    print('model parameters:')
    pprint_json(model.get_params())

    fit_params = config.params.get('fit_params', {})

    # ----------------------------------
    click.secho(
        f"\nTraining model...", bold=True)
    # ----------------------------------

    utc_training_date = datetime.datetime.now(datetime.timezone.utc)
    start = time.time()
    model = model.fit(X, y, **fit_params)
    elapsed_time = time.time() - start

    print('Training finished in {:.0f} s'.format(elapsed_time))

    model_out_path = os.path.join(model_out_dir, model_name)
    trained_model_id = f"{model_name}__{utc_training_date.isoformat()}"
    model_out_file = os.path.join(model_out_path, trained_model_id + '.pkl')
    meta_out_file = os.path.join(model_out_path, trained_model_id + '.meta.json')

    # ----------------------------------
    click.secho(f"\nExport model into {model_out_file}...", bold=True)
    # ----------------------------------

    metadata = {
        'model_name': model_name,
        'trained_model_id': trained_model_id,
        'sampling_proportion': sampling_prop,
        'n_training_samples': len(X),
        'n_input_features': X.shape[1],
        'most_recent_transaction_date': X.DATE_TX.max().isoformat(),
        'first_transaction_date': X.DATE_TX.min().isoformat(),
        'training_set_median_price': np.median(y),
        'filter_params': filter_params,
        'training_date': utc_training_date.isoformat(),
        'training_runtime': elapsed_time,
        'training_params': config.params,
        'model_file_path': str(PurePosixPath(Path(model_out_file))),
        'meta_out_file': str(PurePosixPath(Path(meta_out_file)))
    }

    pprint_json(metadata)


    os.makedirs(model_out_path, exist_ok=True)
    #json.dump(metadata, open(meta_out_file, 'w'), sort_keys=True, indent=2)

    # BZ2File can be used to compress model files but this is much slower.

    click.secho(f"\nDone !", bold=True)
    print(f"  Metadata file: {meta_out_file}")
    print(f"  Model file: {model_out_file}")

    return model, metadata

if __name__ == '__main__':
    train_model_cli()
