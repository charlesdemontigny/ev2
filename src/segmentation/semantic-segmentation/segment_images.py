# System libs
import argparse
import csv
import json
import os
import time

# Numerical libs
import numpy as np
import pandas as pd
import torch
from scipy.misc import imread, imresize
from scipy.ndimage import zoom
from torch.autograd import Variable
from torchvision import transforms

import cv2

# Our libs
from colour_spec import COLOUR_SPEC
from models import ModelBuilder
from utils import colorEncode

DEFAULT_BATCH_PATH = './data/interim/images/vicky_0.8%_sample/batches/batch=4096/images_batch_000000.csv'
DEFAULT_IMAGE_DIR = './data/raw/images/vicky_0.8%_sample'
DEFAULT_OUTPUT_DIR = './data/interim/images/vicky_0.8%_sample/segmentation/output'
DEFAULT_PRETRAINED_MODEL_DIR = './data/external/segmentation/ADE20K/pretrained_models/baseline-resnet34_dilated8-psp_bilinear'
OBJECT150_CVS_PATH = './data/external/segmentation/ADE20K/pretrained_models/baseline-resnet34_dilated8-psp_bilinear/object150_info.csv'

APPLICATION_DESCRIPTION = 'Image Semantic Segmentation'


def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


def parse_args():

    parser = argparse.ArgumentParser()

    parser.add_argument('--images_file_relative_path', type=str, default=DEFAULT_BATCH_PATH,
                        help='Images path (.txt) file path')
    parser.add_argument('--images_base_directory', type=str,
                        default=DEFAULT_IMAGE_DIR, help='Path to input images')
    parser.add_argument('--bsave', action='store_true', help='Save results')
    parser.add_argument('--bshow', action='store_true', help='Show results')
    parser.add_argument('--save_dir', default=DEFAULT_OUTPUT_DIR, help='Output directory')

    # Model related arguments
    parser.add_argument('--pretrained_model_directory', default=DEFAULT_PRETRAINED_MODEL_DIR,
                        help="a name for identifying the model to load")
    parser.add_argument('--suffix', default='_best.pth',
                        help="which snapshot to load")
    parser.add_argument('--arch_encoder', default='resnet34_dilated8',
                        help="architecture of net_encoder")
    parser.add_argument('--arch_decoder', default='psp_bilinear',
                        help="architecture of net_decoder")
    parser.add_argument('--fc_dim', default=512, type=int,
                        help='number of features between encoder and decoder')

    # Path related arguments
    parser.add_argument('--test_img', type=str, default="")

    # Data related arguments
    parser.add_argument('--num_val', default=-1, type=int,
                        help='number of images to evalutate')
    parser.add_argument('--num_class', default=150, type=int,
                        help='number of classes')
    parser.add_argument('--batch_size', default=1, type=int,
                        help='batchsize')
    parser.add_argument('--imgSize', default=225, type=int,
                        help='resize input image')
    parser.add_argument('--segSize', default=-1, type=int,
                        help='output image size, -1 = keep original')

    # Misc arguments
    parser.add_argument('--ckpt', default='./ckpt',
                        help='folder to output checkpoints')
    parser.add_argument('--visualize', default=0,
                        help='output visualization?')

    return parser.parse_args()


# forward func for testing
def forward_test_multiscale(nets, img, args):
    (net_encoder, net_decoder) = nets

    pred = torch.zeros(1, args.num_class, img.size(2), img.size(3))
    pred = Variable(pred, volatile=True).cuda()

    for scale in args.scales:
        img_scale = zoom(img.numpy(),
                         (1., 1., scale, scale),
                         order=1,
                         prefilter=False,
                         mode='nearest')

        # feed input data
        input_img = Variable(torch.from_numpy(img_scale),
                             volatile=True).cuda()

        # forward
        encoded = net_encoder(input_img)
        pred_scale = net_decoder(encoded,
                                 segSize=(img.size(2), img.size(3)))

        # average the probability
        pred = pred + pred_scale / len(args.scales)

    return pred


def visualize_result(img, pred, args, w, h):
    img = img[0]

    with open(OBJECT150_CVS_PATH, 'r') as f:
        reader = csv.reader(f)
        cvs_list = list(reader)

    pred = pred.data.cpu()[0]
    for t, m, s in zip(img,
                       [0.485, 0.456, 0.406],
                       [0.229, 0.224, 0.225]):
        t.mul_(s).add_(m)
    img = (img.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)

    # prediction
    pred_ = np.argmax(pred.numpy(), axis=0) + 1
    class_ids = np.unique(pred_)
    pred_color = colorEncode(pred_, COLOUR_SPEC)

    pred_ = cv2.resize(pred_, dsize=(w, h), interpolation=cv2.INTER_NEAREST)

    pred_color = cv2.resize(pred_color, dsize=(w, h), interpolation=cv2.INTER_NEAREST)

    legend = []
    legend_ids = []
    for class_id in class_ids:

        img2 = np.zeros((64, 64*4), dtype='uint8')
        img2[:, :] = class_id
        img_legend = colorEncode(img2, COLOUR_SPEC)
        infos = cvs_list[class_id]
        cv2.putText(img=img_legend, text='%s - %s' % (int(class_id), infos[5]), org=(int(16), int(32)),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0, 0, 0))

        legend.append(img_legend)

    legend_img = np.vstack(legend)

    pred_color = cv2.merge([pred_color[:, :, 2], pred_color[:, :, 1], pred_color[:, :, 0]])

    legend = [legend_img[:, :, 2], legend_img[:, :, 1], legend_img[:, :, 0]]

    return pred_, pred_color, legend


def do_segmentation(nets, args):
    # switch to eval mode
    for net in nets:
        net.eval()

    # loading image, resize, convert to tensor
    img = imread(args.test_img, mode='RGB')

    if args.bshow:
        cv2.imshow('INPUT', cv2.merge([img[:, :, 2], img[:, :, 1], img[:, :, 0]]))
        cv2.waitKey(10)

    h, w = img.shape[0], img.shape[1]
    print("Image size: sx=%i, sh=%i" % (w, h))
    s = 1. * args.imgSize / min(h, w)
    img = imresize(img, s)

    # apply pre-processing
    img_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])])
    img = img_transform(img)
    img = img.view(1, img.size(0), img.size(1), img.size(2))

    # forward pass
    pred = forward_test_multiscale(nets, img, args)

    # create outputs for visualization
    predict, coloured_predict, legend = visualize_result(img, pred, args, w, h)

    if args.bshow:
        cv2.imshow('LEGEND', cv2.merge(legend))
        cv2.imshow('RESULTS', coloured_predict)
        cv2.waitKey(10)

    with open(OBJECT150_CVS_PATH, 'r') as f:
        reader = csv.reader(f)
        cvs_list = list(reader)

    uclass_ids = np.unique(predict)
    print(uclass_ids)
    legend_ids = []
    for class_id in uclass_ids:
        area = np.count_nonzero(predict[predict == class_id]) / (predict.shape[0] * predict.shape[1])
        line = str("%i, %f, %s" % (class_id, area, cvs_list[class_id][5]))
        print(line)
        legend_ids.append({'label_id': str(class_id), 'area': str(area), 'label_name': cvs_list[class_id][5]})

    if args.bshow:
        cv2.waitKey(0)

    return predict, coloured_predict, legend, legend_ids


def main(args):

    # Network Builders
    builder = ModelBuilder()
    net_encoder = builder.build_encoder(arch=args.arch_encoder,
                                        fc_dim=args.fc_dim,
                                        weights=args.weights_encoder)
    print_network(net_encoder)
    net_decoder = builder.build_decoder(arch=args.arch_decoder,
                                        fc_dim=args.fc_dim,
                                        segSize=args.segSize,
                                        weights=args.weights_decoder,
                                        use_softmax=True)
    print_network(net_decoder)

    nets = (net_encoder, net_decoder)
    for net in nets:
        net.cuda()

    # single pass
    images_df = pd.read_csv(args.images_file_relative_path, sep=',', dtype=str)
    images_path = list(images_df.apply(
        lambda row: os.path.join(args.images_base_directory, row.SUBDIR, row.IMAGE_ID + '.jpg'), axis=1))

    print('Number of images: %i' % len(images_path))

    for image_path in images_path:

        try:
            start = time.time()
            args.test_img = image_path
            print(args.test_img)

            # do segmentation
            predict, coloured_predict, legend, legend_ids = do_segmentation(nets, args)

            if args.bsave:

                basename = os.path.basename(image_path).split('.')[0]
                subdirectory = os.path.basename(os.path.dirname(image_path))
                ofdir = os.path.join(args.save_dir, subdirectory)

                if not os.path.exists(ofdir):
                    os.makedirs(ofdir)

                fn = '%s_prediction.png' % basename
                ofn = os.path.join(ofdir, fn)

                cv2.imwrite(ofn, predict)

                fn = '%s_mask.png' % basename
                ofn = os.path.join(ofdir, fn)
                cv2.imwrite(ofn, coloured_predict)
                ofn = os.path.join(ofdir, '%s_legend.png' % basename)

                ofn = os.path.join(ofdir, '%s_legend.json' % basename)
                legend_ids.append({'image_path:': fn})

                legend_ids_json = json.dumps(legend_ids)
                with open(ofn, 'w') as fp:
                    fp.write(legend_ids_json)

                print('Done! Output is saved in {}'.format(ofn))
            end = time.time()
            print('------- Elapsed time: %f secs' % (end - start))
        except Exception as inst:
            print('An error occured : %s' % image_path)
            print(inst)


if __name__ == '__main__':

    start = time.time()

    print('----- %s' % APPLICATION_DESCRIPTION)

    args = parse_args()
    print(vars(args))

    # scales for evaluation
    args.scales = (0.5, 0.75, 1, 1.25, 1.5, 1.75)

    # absolute paths of model weights
    args.weights_encoder = os.path.join(args.pretrained_model_directory,
                                        'encoder' + args.suffix)
    args.weights_decoder = os.path.join(args.pretrained_model_directory,
                                        'decoder' + args.suffix)

    main(args)
    end = time.time()
    print('----- Elapsed time: %f secs' % (end - start))
