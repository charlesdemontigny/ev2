#!/usr/bin python3
# -*- coding: utf-8 -*-

import importlib
import json
import time
import click
import os
import numpy as np
import pandas as pd
import datetime

from datetime import datetime as dt

from eval.metrics import MAE, MAPE, RMSE, RMSLE, MedAPE
from eval.model_selection import FreqBasedEventSplit, cross_val_predict_df
from models.utils import log_target_transform
from dataset import load_dataset, filter_dataset, sample_dataset, prepare_dataset
from models.evia import EviaHistoricalPrediction

# Metrics
metrics = {
    'RMSLE': RMSLE,
    'MAPE': MAPE,
    'MedAPE': MedAPE,
    'MAE': MAE,
    "RMSE": RMSE}

# Helper functions -------------------------------------------------------------
def compute_metrics(df):
    return pd.Series({
        name: metric(df['y_true'], df['pred'])
        for name, metric in metrics.items()
    })


def parse_run_cross_validation_args(df, model_name, test_split, n_jobs):
    """
    Verify that every arguments of ``run_cross_validation`` respect the format
    and the type required.
    """
    # df
    if not isinstance(df, pd.DataFrame):
        raise TypeError("{} must be a pandas DataFrame".format(df))
    if 'MONTANT' not in df.columns:
        raise KeyError("The columns ``MONTANT`` must be a columns of the pandas DataFrame")
    # model
    if not any([file.startswith(model_name) for file in os.listdir('src/config/')]):
        raise FileNotFoundError('{} is not in the config folder'.format(model_name))
    # test_split
    try:
        datetime.datetime.strptime(test_split, '%Y-%m-%d')
    except ValueError:
        raise ValueError("Incorrect date format, should be YYYY-MM-DD")
    # n_jobs
    if not isinstance(n_jobs, int):
        raise TypeError("n_jobs must be an integer")
    if (n_jobs < -1 or n_jobs > 22):
        raise ValueError("n_jobs value is {}. It must be between -1 and 22".format(n_jobs))

def load_model(model_name):
    """Load model from the config folder"""
    config = importlib.import_module(f'.{model_name}', 'config')
    model = config.model

    print(f"Regressions parameters: {config.params}")

    if config.params['log_regression']:
        model = log_target_transform(model)

    fit_params = config.params.get('fit_params', {})

    print(json.dumps(model.get_params(), sort_keys=True, indent=2,
                     default="{0.__class__.__module__}.{0.__class__.__name__}".format))

    return model, fit_params, config._number_of_features

# Mains function ---------------------------------------------------------------
def run_cross_validation(df, geo, model_name, output_dir, test_split = None, n_jobs = 1):
    """
    Run a cross-validation. The model is initialy train on all transaction prior
    to ``test_split``. Then predictions is perform on the month after ``test_split``
    for every building that at been sold in that month. The accuracy is compute
    by comparison with the actual sale price. The model is subsequently train on
    a dataset including the month just tested.

    Args:
        df: pandas DataFrame
            A DataFrame that contains the target variable and features.
        model: str
            A model based on a sklearn architecture. It must be in the config folder.
        test_split: str
            A string in the format ('yyyy-mm-dd') representing the date,
            in the dataset, when to start testing the model.
        n_jobs: int
            Number of CPU cores to use for cross validation.

    Export: A text file including summary of the cross-validation evaluation.
    """
    # Set test_split if needed
    if test_split is None:
        test_split = (dt.now() - datetime.timedelta(days=2*365)).strftime("%Y-%m-%d")

    # Parse arguments
    parse_run_cross_validation_args(df, model_name, test_split, n_jobs)

    # prepare dataset (split X, y)
    X, y = prepare_dataset(df)

    # filter on min year to use
    filter_params = {
        'low_prices_filter':100000,
        'drop_multiple_house_sales': True,
        'drop_within_family_sales': True,
        'use_code_threshold': 1380,
        'drop_tx_before_year': 2004
    }
    X, y = filter_dataset(X, y, **filter_params)

    assert not np.isnan(y).any()
    assert (y >= 0).all()

    # load model
    model, fit_params, _number_of_features = load_model(model_name)

    # FreqBasedEventSplit regarder ce que cette fonction fait
    cv = FreqBasedEventSplit(test_split, '1MS', 'DATE_TX')

    # cross-val-predict
    start = time.time()
    predictions = cross_val_predict_df(model, X, y, cv=cv, n_jobs=n_jobs, verbose=10, fit_params=fit_params)
    elapsed_time = time.time() - start

    click.secho('Training finished in {:.0f} s'.format(elapsed_time), bold = True)
    predictions['y_true'] = y[predictions.index]

    # compute pred_test dataset
    test_pred = predictions.copy()
    test_pred['HOME_ID'] = X.iloc[predictions.index]['HOME_ID'].values
    test_pred = pd.merge(test_pred, geo, how = 'left', left_on = 'HOME_ID', right_on = 'IMM_NO_ID_UEF')
    test_pred.drop('IMM_NO_ID_UEF', axis = 1, inplace = True)

    # Compute metrics
    scores = predictions.groupby('fold').apply(compute_metrics)
    score_overall = compute_metrics(predictions)

    # create and export cross-validation metadata and accuracy
    metadata = {
        'date': dt.now().strftime('%Y-%m-%d %H:%M:%S'),
        'model': model_name,
        'nb_obs': str(len(X)),
        'nb_features': str(_number_of_features),
        'most_recent_transaction_date': X.DATE_TX.max().isoformat(),
        'first_transaction_date': X.DATE_TX.min().isoformat(),
        'training_set_median_price': str(np.median(y)),
        'eval_runtime': " ".join([str(round(elapsed_time, 2)), 'secondes'])
    }

    # Export of metadata and score
    utc_date = dt.now(datetime.timezone.utc).isoformat()
    if not os.path.exists(output_dir):
        try:
            os.mkdir(output_dir)
        except:
            os.mkdir('data')
            os.mkdir(output_dir)
    with open(output_dir + "ev2_metadata.txt", 'w') as file:
        for key, val in metadata.items():
            file.write(" ".join([key + ':', val, '\n']))

    scores_string = scores.to_string()
    score_overall_string = score_overall.to_string()

    with open(output_dir + "ev2_metadata.txt", 'a') as file:
        file.write("\n")
        file.write(score_overall_string)
        file.write("\n")
        file.write("\n")
        file.write(scores_string)

    # Returns pred_test DataFrame
    return test_pred

if __name__ == '__main__':
    run_cross_validation()
