#!/usr/bin python3
# -*- coding: utf-8 -*-

import json
import os
import pickle
import time
import click
import pandas as pd

from pathlib import Path

from fork import fork, confidence_score

# Helper function
def pprint_json(dict):
    # Attempt to pretty print dictionnary into a json string:
    print(json.dumps(dict, sort_keys=True, indent=2,
                     default="{0.__class__.__module__}.{0.__class__.__name__}".format))

# Main function
def batch_predict(df, model, median_errors, geo, output_dir, output_format,output_name = 'ev2_pred',
                    model_metadata = 'None'):
    """
    Predict for all building in the JLR's dataset.

    Args:
        df: A pandas DataFrame with a single line for every building.
        model: A trained model.
        output_dir: The directory where to export the prediction file.
        output_format: The format in which to export the prediction file. It can
                        take ``.csv.bz2``, ``.feather`` or ``.csv``.
        model_metadata: The model metadata produce while the model was trained.

    Export: A file that contains the predictions.
    """
    if model_metadata is not None:
        print('Model metadata:')
        pprint_json(model_metadata)

    print('Model parameters:')
    pprint_json(model.get_params())

    if not isinstance(df, pd.DataFrame):
        raise TypeError("input_df must be a pandas DataFrame")

    # ----------------------------------
    click.secho(f"\nComputing model predictions...", bold=True)
    # ----------------------------------

    start = time.time()
    predictions = model.predict(df)
    elapsed_time = time.time() - start
    print('Prediction computed in {:.0f} seconds.'.format(elapsed_time))

    if output_name is None:
        output_file_path = os.path.join(output_dir, "predictions" + output_format)
    else:
        output_file_path = os.path.join(output_dir, output_name + output_format)
    # ----------------------------------
    click.secho(f"\nExporting predictions into {output_file_path}...", bold=True)
    # ----------------------------------

    os.makedirs(output_dir, exist_ok=True)

    output_predictions = pd.DataFrame({
        "HOME_ID": df['HOME_ID'],
        "Date": df['DATE_TX'],
        "estimation": predictions
    })

    # Compute fork
    output_predictions_fork = fork(output_predictions, median_errors, geo)

    # Compute confidence score
    output_predictions_conf = confidence_score(output_predictions_fork,
                                    thresholds =  [0.05, 0.07, 0.09, 0.1, 0.11])


    # Export
    if output_format == ".csv.bz2":
        output_predictions_conf.to_csv(output_file_path, compression='bz2', index=False)
    elif output_format == ".feather":
        output_predictions_conf.to_feather(output_file_path)
    elif output_format == ".csv":
        output_predictions_conf.to_csv(output_file_path, index = False)
    else:
        raise ValueError("Unknown output format: " + output_format)

    click.secho(f"Predictions: {output_file_path}\n", bold=True)

