# -*- coding: utf-8 -*-

import pandas as pd

from collections import OrderedDict
from lightgbm import LGBMRegressor
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import FunctionTransformer, Imputer

from preprocessing.feature_engineering import (AddSimpleFeatures, GeoKNN,
                                               GroupByImputer, MaxCap,
                                               PrevPrice, SelectColumns,
                                               UnitConverter)
from models.median_multilevel import MedianByCityYear

_numerical_features = ['ANNEE_CONST', 'SUPERFICIE', 'CODE_UTILISATION', 'IMMEUBLE_DANS_EVIA',
                        'NB_LOGEMENTS', 'N_COMMERCES','N_ETAGES', 'LOT_SUPERFICIE',
                       'CUR_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT',
                       'FIRST_PREV_EVAL_VALEUR_BATISSE', 'FIRST_PREV_EVAL_VALEUR_LOT',
                        'LONGITUDE_FRESH', 'LATTITUDE_FRESH',
                        'PREC_PRICE', 'PREC_DELTA_DATE',
                        'PREC_LIE', 'PREC_TRANSMISSION', 'PREC_REPRISE', 'PREC_GARANTIE',
                        'PREC_NOUVELLE_CONSTRUCTION', 'CONSTRUCTION_NEUVE',
                        'VENTE_MULTIPLE', 'VENTE_LIE', 'median_by_dr']

# Additional numerical features created by transformers (ex: AddSimpleFeatures)
_num_feats_eng = ['TX_YEAR', 'CUR_DATE_EVAL_YEAR']

_knn_cols = ['SUPERFICIE', 'LOT_SUPERFICIE', 'ANNEE_CONST',
             'N_ETAGES', 'NB_LOGEMENTS',
             'FIRST_PREV_EVAL_VALEUR_BATISSE', 'FIRST_PREV_EVAL_VALEUR_LOT',
             'CUR_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT']

_categorical_features = OrderedDict([
    ('TX_EXCLUE', 2),
    ('CATEGORIE_BATIMENT', 10),
    ('VILLE', 10),
    ('ARRONDISSEMENT_NOM', 10)
])

_number_of_features = len(_numerical_features) + 2

_strategy = 'mean'

_regressor = LGBMRegressor(objective='regression',
                           num_leaves=63,
                           learning_rate=0.1,
                           n_estimators=700,
                           min_data_per_leaf = 5,
                           feature_fraction = 0.8,
                           max_depth = -1,
                           silent=False,
                           random_state=1234, n_jobs=-1)

def to_codes(df):
    return df.apply(lambda col: col.cat.codes)


def Imputer_df(df: pd.DataFrame, cols=None, strategy='mean'):
    # wrapper around Imputer allowing use with pandas.DataFrame
    df = df.copy()
    imp = Imputer(strategy=strategy)
    if cols is None:
        cols = list(df.columns)  # BEWARE: if nothing passed, apply on ALL columns
    df_array = imp.fit(df[cols]).transform(df[cols])
    df.loc[:, cols] = df_array
    return df


_knn = Pipeline([
    ('imputeLatLong_byCity', GroupByImputer(
        strategy='median',
        by='VILLE_BSQ',
        cols_to_impute=_knn_cols + ['LATTITUDE_FRESH', 'LONGITUDE_FRESH'])),
    ('reimpute_latlong', FunctionTransformer(
        Imputer_df,
        kw_args={'cols': _knn_cols + ['LATTITUDE_FRESH', 'LONGITUDE_FRESH'],
                 'strategy': _strategy},
        validate=False)),
    (f'NN_{_strategy}', GeoKNN(k=30,
                               latlong=['LATTITUDE_FRESH', 'LONGITUDE_FRESH'],
                               metric='l2',
                               weights='uniform',
                               target=_knn_cols))
])


model = Pipeline([
    ('converting_unit', FunctionTransformer(
        UnitConverter,
        kw_args={'cols_to_convert': ['LOT_SUPERFICIE'],
                 'unit_col': ['UNITE_AIRE'],
                 'unit': 'PI2'},
        validate=False)
     ),
    ('lot_superf_cap', MaxCap(cols_to_cap=['LOT_SUPERFICIE'], max_cap=10000)),
    ('median_by_dr', MedianByCityYear()),
    ('union', FeatureUnion([
        ('cat_features', Pipeline([
            ('select_cat', SelectColumns(_categorical_features)),
            ('cat_to_str', FunctionTransformer(to_codes, validate=False)),
        ])),
        ('numerical_cols', Pipeline([
            ('simple_features', AddSimpleFeatures()),
            ('basic_numerical_features', SelectColumns(_numerical_features + _num_feats_eng))]
        )),
        ('pipe_latlong', _knn)#,
        #('add_median_by_dr', Pipeline([('median_by_dr', MedianByCityYear)]))
    ])),
    ('regression', _regressor)
])

params = {
    'log_regression': True,
    'fit_params': {
        'regression__categorical_feature': list(range(len(_categorical_features))),
    }
}
