from collections import OrderedDict

import numpy as np
import pandas as pd
from numpy import nan
from pandas import Timestamp

from sklearn.preprocessing import FunctionTransformer

from ..preprocessing.feature_engineering import (DummyEncodeCategoricals,
                                                 GroupByImputer,
                                                 MaxCap,
                                                 UnitConverter,
                                                 PrevPrice)

sample_df = pd.DataFrame({'Date_Tx': {0: Timestamp('1993-09-01 00:00:00'),
                                      1: Timestamp('2009-02-17 00:00:00'),
                                      2: Timestamp('2005-02-21 00:00:00'),
                                      3: Timestamp('2014-10-16 00:00:00'),
                                      4: Timestamp('2005-05-19 00:00:00'),
                                      5: Timestamp('1998-06-18 00:00:00'),
                                      6: Timestamp('2011-04-08 00:00:00'),
                                      7: Timestamp('2012-12-21 00:00:00'),
                                      8: Timestamp('1985-05-08 00:00:00'),
                                      9: Timestamp('1995-03-24 00:00:00'),
                                      10: Timestamp('2005-09-22 00:00:00'),
                                      11: Timestamp('1996-04-26 00:00:00'),
                                      12: Timestamp('2004-03-03 00:00:00'),
                                      13: Timestamp('2011-10-24 00:00:00'),
                                      14: Timestamp('2002-07-03 00:00:00'),
                                      15: Timestamp('2010-12-07 00:00:00'),
                                      16: Timestamp('2006-11-27 00:00:00'),
                                      17: Timestamp('2008-10-31 00:00:00'),
                                      18: Timestamp('2008-11-04 00:00:00'),
                                      19: Timestamp('2013-06-05 00:00:00')},
                          'Home_ID': {0: 13188769,
                                      1: 13189140,
                                      2: 13189169,
                                      3: 13189882,
                                      4: 13190163,
                                      5: 13190867,
                                      6: 13191158,
                                      7: 13191158,
                                      8: 13191158,
                                      9: 13192250,
                                      10: 13192843,
                                      11: 13193286,
                                      12: 13193310,
                                      13: 13195348,
                                      14: 13195549,
                                      15: 13196789,
                                      16: 13196789,
                                      17: 13198831,
                                      18: 13201119,
                                      19: 13201119},
                          'Lat': {0: 1,
                                  1: 2,
                                  2: 3,
                                  3: nan,
                                  4: 6,
                                  5: 8,
                                  6: 10,
                                  7: nan,
                                  8: 15,
                                  9: 23,
                                  10: nan,
                                  11: 8,
                                  12: 9,
                                  13: 8,
                                  14: 7,
                                  15: nan,
                                  16: 6,
                                  17: nan,
                                  18: 5,
                                  19: nan},
                          'Long': {0: 11,
                                   1: 12,
                                   2: 13,
                                   3: nan,
                                   4: 14,
                                   5: 15,
                                   6: 16,
                                   7: nan,
                                   8: 17,
                                   9: 58,
                                   10: nan,
                                   11: 18,
                                   12: 19,
                                   13: 18,
                                   14: 17,
                                   15: nan,
                                   16: 16,
                                   17: nan,
                                   18: 15,
                                   19: nan},
                          'Ville': {0: 'ST-LAMBERT',
                                    1: 'ST-LAMBERT',
                                    2: 'ST-LAMBERT',
                                    3: 'ST-LAMBERT',
                                    4: 'ST-LAMBERT',
                                    5: 'ST-LAMBERT',
                                    6: 'ST-SEVERIN (MAURICIE)',
                                    7: 'ST-SEVERIN (MAURICIE)',
                                    8: 'ST-SEVERIN (MAURICIE)',
                                    9: 'ST-SEVERIN (MAURICIE)',
                                    10: 'MONT-TREMBLANT',
                                    11: 'MONT-TREMBLANT',
                                    12: 'MONT-TREMBLANT',
                                    13: 'MONT-TREMBLANT',
                                    14: 'MONT-TREMBLANT',
                                    15: 'ST-JEAN-SUR-RICHELIEU',
                                    16: 'ST-JEAN-SUR-RICHELIEU',
                                    17: 'ST-JEAN-SUR-RICHELIEU',
                                    18: 'STE-CLOTILDE-DE-HORTON',
                                    19: 'STE-CLOTILDE-DE-HORTON'}
                          }
                         )


def test_unit_converter():
    df = sample_df.copy(deep=True)
    df['vals'] = range(len(df))
    df['units'] = ['PI2', 'm2']*10
    transf = FunctionTransformer(UnitConverter,
                                 kw_args={'cols_to_convert': 'vals',
                                          'unit_col': 'units',
                                          'unit': 'PI2'},
                                 validate=False)
    df_transf = transf.fit_transform(df)
    assert df_transf.shape == df.shape
    assert np.allclose(df_transf['vals'].values[:5], [0, 1, 2/10.7639, 3, 4/10.7639])


def test_col_capping_with_int():
    df = sample_df.copy(deep=True)
    df['vals'] = range(len(df))
    capped_df = MaxCap(cols_to_cap='vals', max_cap=10).fit_transform(df)

    assert capped_df.shape == df.shape
    assert np.allclose(capped_df['vals'].values[:10], df['vals'].values[:10])
    assert np.allclose(capped_df['vals'].values[10:], [10]*(len(sample_df)-10))


def test_col_capping_with_percentile():
    df = sample_df.copy(deep=True)
    df['vals'] = range(len(df))
    capped_df = MaxCap(cols_to_cap='vals', percentile=90).fit_transform(df)

    assert capped_df.shape == df.shape
    assert np.allclose(capped_df['vals'].values[:10], df['vals'].values[:10])
    assert np.allclose(capped_df['vals'].values[-2], [17.1, 17.1])


def test_GroupByImputer_median():
    cols = ['Lat', 'Long']
    by = 'Ville'
    df = sample_df.copy()
    i_nans = df.loc[df[cols[0]].isna(), by]

    # generate the Imputer()
    imputer = GroupByImputer(strategy='median', by=by, cols_to_impute=cols)
    imputer.fit(df)
    median_dict = imputer.agg_
    assert len(median_dict.keys()) == len(cols), "Internal Dict must have a key for each column to Impute"

    # applying the transformer
    df_t = imputer.transform(df)
    for c in cols:
        assert df_t[c].isna().sum() == 0, "There should not be any Nan left"

    assert np.allclose(df_t.loc[i_nans.index, cols[0]].values, [3, 15, 8, 6, 6, 5]
                       ), "median of [1, 2, 3,nan, 6, 8], [10, 15, nan, 23] etc"


def test_GroupByImputer_mean():
    cols = ['Lat', 'Long']
    by = 'Ville'
    df = sample_df.copy()
    i_nans = df.loc[df[cols[0]].isna(), by]
    imputer = GroupByImputer(strategy='mean', by=by, cols_to_impute=cols)
    imputer.fit(df)

    # applying the transformation
    df_t = imputer.transform(df)
    assert df_t.Lat.isna().sum() == 0
    assert df_t.Long.isna().sum() == 0
    assert np.allclose(df_t.loc[i_nans.index, cols[0]].values, [4, 16, 8, 6, 6, 5]), "mean of [1, 2, 3,nan, 6, 8]"


def test_DummyEncodeCategoricals():

    df = sample_df.copy(deep=True)

    # 4 categories, one not represented, 2 NaN values
    df['cat1'] = pd.Categorical(['a', 'c', 'c', nan, 'b', 'c', 'a', 'b', 'c', 'a',
                                 'b', 'c', 'a',  nan, 'c', 'a', 'b', 'c', 'a', 'b'],
                                categories=['a', 'b', 'c', 'd'])

    df['Ville_cat'] = df['Ville'].astype('category')

    # cat2 is exactly the same as cat1 but we will limit the number of encoded categories.
    df['cat2'] = df['cat1'].copy()

    # cat3 is cat1 encoded as strings. Note: `.astype('str')` replace missing values by 'nan's.
    df['cat3'] = df['cat1'].copy().astype('str')

    encoder = DummyEncodeCategoricals(columns=OrderedDict([
        ('cat1', None),
        ('cat3', None),
        ('Ville_cat', None),
        ('cat2', 2),  # top 2 categories only
    ]))

    encoded = encoder.fit_transform(df)

    assert list(encoder._categories.keys()) == ['cat1', 'cat3', 'Ville_cat', 'cat2']
    assert list(encoder._categories['cat1']) == ['a', 'b', 'c', 'd', 'other-NA']
    assert list(encoder._categories['cat3']) == ['a', 'b', 'c', 'nan', 'other-NA']  # cat. as they appear in the series
    assert list(encoder._categories['cat2']) == ['a', 'c', 'other-NA']

    assert isinstance(encoded, np.ndarray)

    # cat1 - a
    assert np.array_equal(encoded[:, 0], np.array([1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0]))
    # cat1 - b
    assert np.array_equal(encoded[:, 1], np.array([0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1]))
    # cat1 - c
    assert np.array_equal(encoded[:, 2], np.array([0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0]))
    # cat1 - d
    assert np.array_equal(encoded[:, 3], np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))
    # cat1 - other-NA
    assert np.array_equal(encoded[:, 4], np.array([0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]))
    # cat1 - sum of flags
    assert np.array_equal(encoded[:, :5].sum(axis=1), np.ones((len(df))))

    # check if cat3 encoding is identical to cat1 encoding (for 'a', 'b' and 'c' categories)
    assert np.array_equal(encoded[:, :3], encoded[:, 5:8])

    # cat2 - a
    assert np.array_equal(encoded[:, -3], np.array([1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0]))
    # cat2 - c
    assert np.array_equal(encoded[:, -2], np.array([0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0]))
    # cat2 - other-NA
    assert np.array_equal(encoded[:, -1], np.array([0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1]))
    # cat1 - sum of flags
    assert np.array_equal(encoded[:, -3:].sum(axis=1), np.ones((len(df))))


def test_PrevPrice():

    np.random.seed(1234)
    X = sample_df.copy(deep=True)

    y = np.array(range(len(X)))  # Use row indices as prices:

    prev_prices_transform = PrevPrice(convert_to_log_price=False)

    X_prev_prices = prev_prices_transform.fit_transform(X, y)

    prev_prices = X_prev_prices[:, 0]
    days_since_last_transaction = X_prev_prices[:, 1]

    # There are 3 Home IDs with prevous prices:

    # 13191158: 8 - 6 - 7
    assert prev_prices[6] == y[8]
    assert days_since_last_transaction[6] == 9466

    assert prev_prices[7] == y[6]
    assert days_since_last_transaction[7] == 623

    # 13196789: 16 -15
    assert prev_prices[15] == y[16]
    assert days_since_last_transaction[15] == 1471

    # 13201119: 18 -19
    assert prev_prices[19] == y[18]
    assert days_since_last_transaction[19] == 1674

    # Check if other rows are NaN:
    assert np.all(np.isnan(np.delete(X_prev_prices, [6, 7, 15, 19], axis=0)))
