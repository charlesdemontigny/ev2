import numpy as np
import pandas as pd

from ..eval.metrics import RMSE
from ..eval.metrics import RMSLE
from ..eval.metrics import MedAPE
from ..eval.metrics import MAPE
from ..eval.metrics import MAE

df = pd.DataFrame({'X': np.random.randn(50), 'y_true': np.random.ranf(50)*300000+200000})
y_true = df.y_true.values
y_pred = y_true


def test_RMSE():
    assert RMSE(y_true, y_true) == 0
    assert np.isclose(RMSE(np.array([100, 100]), np.array([104, 105])), 4.5277)  # error == 4, 5 --> 5,


def test_RMSLE():
    assert RMSLE(y_true, y_true) == 0
    assert np.isclose(RMSLE(np.array([10]), [100]), 2.21722)


def test_MedAPE():
    assert MedAPE(y_true, y_true) == 0
    assert MedAPE(np.array([2, 200, 2000]), np.array([2, 202, 5000])) == 0.01


def test_MAPE():
    assert MAPE(y_true, y_true) == 0
    assert MAPE(np.array([2, 200, 2000]), np.array([2, 202, 2100])) == .02  # 0%, 1%, 5% --> 2%


def test_MAE():
    assert MAE(y_true, y_true) == 0
    assert MAE([10000, 10000, 10000], [10000, 20000, 30000]) == 10000
