import pandas as pd
import numpy as np

from ..eval.model_selection import FreqBasedEventSplit


def _gen_df(islice=slice(None)):
    df = pd.DataFrame({'Date_col': ['2015-01-02',
                                    '2015-02-02',
                                    '2014-03-02',
                                    '2014-01-02',
                                    '2015-05-02',
                                    '2015-07-02',
                                    '2015-07-31'],
                       'col1': [1, 2, 3, 4, 5, 6, 7],
                       'col2': [6, 7, 6, 7, 3, 4, 5]
                       })
    df['Date_col'] = pd.to_datetime(df.Date_col)

    if islice is None:
        return df.head(0)  # just empty df
    elif isinstance(islice, list):
        if len(islice) >= 2:
            islice = slice(islice[0], islice[1])
        elif len(islice) == 1:
            islice = slice(islice[0])
    else:
        islice = slice(None)  # defaults to all
    return df.iloc[islice]


def test_FreqBasedEventSplit_late_split():
    df = _gen_df()
    # EventSplit starts post latest date in df
    cv = FreqBasedEventSplit(df.Date_col.max()
                             + pd.Timedelta(1, unit='d'), '1MS', 'Date_col')
    assert cv._get_date_test_ranges(df) == [df.Date_col.max() + pd.Timedelta(1, unit='d')]  # Maybe suspect behavior

    assert cv.get_n_splits(df) == 0
    # check generator generates StopIterator Error
    for xi, yi in cv.split(df, df.col2):
        assert xi == []
        assert yi == []


def test_FreqBasedEventSplit_Expected_input():
    df = _gen_df()
    cv = FreqBasedEventSplit('2015-01-01', '1MS', 'Date_col')
    assert len(cv._get_date_test_ranges(df)) == 8  # list of test-start-dates + [end]
    assert cv.get_n_splits(df) == 7  # expect seven folds

    # expected returned indices for a synthetic dataset:
    # [2 3] [0]
    # [0 2 3] [1]
    # [0 1 2 3] []
    # [0 1 2 3] []
    # [0 1 2 3] [4]
    # [0 1 2 3 4] []
    # [0 1 2 3 4] [5]
    indices = [(xi, yi) for xi, yi in cv.split(df, df.col2)]
    assert np.all(indices[0][0] == np.array([2, 3]))
    assert np.all(indices[0][1] == np.array([0]))

    # if no sample in test set, get
    assert np.all(indices[2][0] == np.array([0, 1, 2, 3]))
    assert np.all(indices[2][1] == np.array([]))

    # if no sample in test set, get
    assert np.all(indices[-1][0] == np.array([0, 1, 2, 3, 4]))
    assert np.all(indices[-1][1] == np.array([5, 6]))
