﻿# -*- coding: utf-8 -*-

# Import module
import datetime
import cx_Oracle
import importlib
import json
import os
import gc
import click
import re
import pickle
import time
import numpy as np
import pandas as pd
from pathlib import Path, PurePosixPath

from run_eval import run_cross_validation
from dataset import filter_dataset, load_dataset, sample_dataset
from make_dataset import make_dataset, create_output_path
from train_model import train_model, pprint_json
from batch_predict import batch_predict
from fork import median_errors

# Main function
@click.command()
@click.option('-m', '--model_name', default= "lgbm_final")
@click.option('-s', '--sampling_prob', default = None, type = float)
@click.option('-t', '--test_split', default = None)
@click.option('-j', '--n_jobs', default = 1)
@click.option('-e', '--env', default = 'dev')
def make_predictions(model_name, sampling_prob, test_split, n_jobs, env):
    """Evaluate the model, train it on the whole dataset and predict new data
    points.

    Args:
        model_name: str
            The name of the model to load from the config folder.
        sampling_prob: float
            The sampling probability to apply on the dataset. It must be [0, 1].
        test_split: str
            A string in the format ('yyyy-mm-dd') representing the date where to
            split the dataset for testing.
        n_jobs: int
            Number of CPU cores to use for cross validation.
        env: str
            The environment in which the model in run, dev or prod.

    Export:
        predictions: CSV
            A CSV file that contains date, id, montant, lowerbound, upperbound and
            confidence score.
    """

    # config
    assert (env == 'dev') or (env == 'prod'), 'env must be either dev or prod'
    if env == 'dev':
        db_name = 'jlrorap1relv'
        output_dir = 'data/test/'
    else:
        db_name = 'azuoraprod'
        output_dir = "//azuorap3/chargement/"

    # Set timer
    time0 = time.time()

    # --------------------------------------------------------------------------
    click.secho("\nLoad data from Oracle...\n", bold = True)
    # --------------------------------------------------------------------------

    #Make train dataset
    connection_train = ['jlr_website', '1qaz2wsx', db_name]
    df = make_dataset('input_train', connection_train, sampling_prob)

    # Make geographic dataset
    connection_geo = ['jlrprod', 'rlj', db_name]
    geo = make_dataset('geo', connection_geo)

    # --------------------------------------------------------------------------
    click.secho("Evaluation of the model in past transactions...\n", bold = True)
    # --------------------------------------------------------------------------

    # run evaluation
    test_pred = run_cross_validation(df, geo, model_name, output_dir, test_split,
                             n_jobs = n_jobs)
    test_pred.to_csv(output_dir + 'ev2_backtesting.csv', index = False)
    # Compute best measure error for every properties
    median_errors_df = median_errors(test_pred, 'IMM_CAT_BAT')

    del test_pred
    gc.collect()

    # --------------------------------------------------------------------------
    click.secho("Training of the model on the whole trasactionnal dataset...\n", bold = True)
    # --------------------------------------------------------------------------

    # Train model on dataset
    model, metadata = train_model(df, model_name, sampling_prob)

    del df
    gc.collect()
    # --------------------------------------------------------------------------
    click.secho("Predict the value of all building in the dataset...\n", bold = True)
    # --------------------------------------------------------------------------

    # Prepare input
    input_df = make_dataset('input_pred', connection_train, sampling_prob)

    # Predict
    batch_predict(df = input_df, model = model, median_errors = median_errors_df, geo = geo,
     output_dir = output_dir, output_format = ".csv", model_metadata = metadata)

    # SQL Procedure
    if env == 'prod':
        connection_export = cx_Oracle.connect('jlr_website', '1qaz2wsx', db_name)
        proc = cx_Oracle.Cursor(connection_export)
        proc.callproc('jlr_website.INSERT_ev2_prediction')

    # Compute running time and print it
    time1 = time.time()
    delta = np.round(time1 - time0, decimals = 1)
    click.secho('Terminé! Le code a pris : %s secondes' % delta, bold = True)

if __name__ == '__main__':
    make_predictions()
