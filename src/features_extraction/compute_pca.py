import os
import argparse
import torch
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torch.autograd import Variable
import fnmatch
import pickle as pkl
import bz2
import time

import matplotlib.pyplot as plt
import numpy as np
import datetime


APPLICATION_DESCRIPTION = 'Compute PCA on features'


DEFAULT_FEATURES_BASE_DIRECTORY = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/raw/vision/features_RESNET152_3x3'
DEFAULT_PCA_FILE_PATH = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/raw/vision/pca/resnet152.pkl'
MATCH_PATTTERN = 'features_batch_000000_RESNET152_3x3.pkl.bz2'
DEFAULT_GRID_XSIZE =3
DEFAULT_GRID_YSIZE = 3
DEFAULT_FEATURES_SIZE = 2048



def parse_args():
    """
    Parse the arguments of the main
    :return:
    """
    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--match_pattern', type=str,
                        default=MATCH_PATTTERN,
                        help='Filter match pattern')
    parser.add_argument('--features_datasets_directory', type=str, default=DEFAULT_FEATURES_BASE_DIRECTORY,
                        help='Directory base features dataset')
    parser.add_argument('--save_dir', type=str, default=DEFAULT_FEATURES_BASE_DIRECTORY,
                        help='Output directory')
    parser.add_argument('--bshow', action='store_true', help='Show results')
    parser.add_argument('--shuffle', action='store_true', help='Shuffle the data')
    parser.add_argument('--pca_file_path', type=str, default=DEFAULT_PCA_FILE_PATH, help='File path to the pkl file of U, S, V matrices')
    parser.add_argument('--grid_xsize', type=int, default=DEFAULT_GRID_XSIZE, help='Horizontal number of rois')
    parser.add_argument('--grid_ysize', type=int, default=DEFAULT_GRID_YSIZE, help='Vertical number of rois')
    parser.add_argument('--feature_size', type=int, default=DEFAULT_FEATURES_SIZE, help='Base feature size')
    parser.add_argument('--proportion_used', type=float, default=1.0,
                        help='Proportion of features used to compute the PCA')
    parser.add_argument('--gpu_mode', action='store_true', help='Use gpu')
    parser.add_argument('--num_workers', type=int, default=16, help='Number of threads used for data loading')
    parser.add_argument('--batch_size', type=int, default=64, help='Batch size processing')

    return parser.parse_args()


class DatasetsFeaturesFromDirectories(Dataset):
    """
    Dataset for managing the input of deep features
    """
    def __init__(self, parameters):
        super(DatasetsFeaturesFromDirectories, self).__init__()

        file_paths_list = []
        for directory_path, dirs, file_paths in os.walk(parameters['features_datasets_directory']):
            for file_path in file_paths:
                if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                    file_paths_list.append(os.path.join(directory_path,file_path ))

        n_files = len(file_paths_list)
        print('Number of files: %i' % n_files)

        if not n_files:
            print("No files!")
            exit(0)

        self.filenames = file_paths_list

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        return fn

    def __len__(self):
        return len(self.filenames)


def load_datasets_features_dataset(parameters):
    """
    Creates the dataloader associated to the features files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the features files path list
    """
    ds = DatasetsFeaturesFromDirectories(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])


def compute_pca(parameters):

    """
    List features directories and compute a PCA decomposition
    :param parameters:
    :return:
    """
    print(parameters)

    feature_size = parameters['feature_size']
    grid_xsize = parameters['grid_xsize']
    grid_ysize = parameters['grid_ysize']
    use_gpu = parameters['gpu_mode']

    datasets = load_datasets_features_dataset(parameters)

    step_size = grid_ysize * grid_xsize
    data_list = []

    for iter, (fns) in enumerate(datasets):

        for fn in fns:
            print("Loading features : %s" % fn)
            dataset = pkl.load(bz2.open(fn, 'rb'))
            for data in dataset:
                feature_0 = torch.from_numpy(data[2])
                feature_1 = torch.from_numpy(data[3])

                for chk in torch.chunk(feature_0, step_size):
                    data_list.append(chk)

                for chk in torch.chunk(feature_1, step_size):
                    data_list.append(chk)

    print("Create features stack")
    X = Variable(torch.stack(data_list))
    print(X.shape)
    data_list = []
    print("Computing PCA")

    X_mean = torch.mean(X, dim=0)
    X = (X - X_mean.expand_as(X))

    if use_gpu:
        X = Variable(X).gpu()

    U, S, V = torch.svd(torch.t(X))

    ofn = parameters['pca_file_path']
    ofdir = os.path.dirname(ofn)
    force_create_directory(ofdir)
    print("Saving PCA : %s" % ofn)
    torch.save([U.cpu(), S.cpu(), X_mean.cpu()], ofn)

    # Save matplotlib graph
    basename = os.path.basename(ofn)
    s = S.cpu().data.numpy()
    sum = np.sum(s)
    s_norm = s / sum
    s_norm_cum = np.cumsum(s_norm)

    fig, ax = plt.subplots()
    ax.bar(range(feature_size), s / s[0], color='r', edgecolor='r', label='Eigen Values')
    ax.plot(range(feature_size), s_norm_cum, label='Cumulative values')

    plt.ylabel('Eigen values')
    plt.xlabel('Features')
    legend = ax.legend(loc='center right', shadow=True)
    plt.show()

    gofn = os.path.join(ofdir, '%s.png' % basename)
    fig.savefig(gofn)
    plt.close(fig)
    print(gofn)


# Main function
if __name__ == '__main__':
    start = time.time()
    print('----- %s' % APPLICATION_DESCRIPTION)
    args = parse_args()
    parameters = vars(args)
    ######################################
    compute_pca(parameters)
    ######################################
    end = time.time()
    print
    datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    print('----- Elapsed time: %f secs' % (end - start))
