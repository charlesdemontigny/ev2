import torch
from torch.autograd import Variable
import numpy as np

def pca_reproject(features_array, cut_idx, U, X_mean):
    """
    Reprojects an array with the pca vector
    :param features_array: Input array
    :param cut_idx: max index of the the reprojection vectors
    :param U: Base vector on which the features array is reprojected
    :param X_mean: Mean of the data
    :return: Returns the new features array
    """
    X = Variable(torch.from_numpy(features_array)) - X_mean.expand_as(features_array)
    features_array = torch.mm(X, U[:, :cut_idx]).data.numpy()
    return features_array


def load_pca_data(pca_file_path, variance_cutoff=0.65):
    """
    Load a pca pickle file and return The U, S and V matrices and the index corresponding to the cumulative
        variance.
    :param pca_file_path:
    :param variance_cutoff:
    :return: Returns U, S, V and the cut_idx
    """

    U, S, V, X_Mean = torch.load(pca_file_path)
    s = S.data.numpy()
    sum = np.sum(s)
    s_cum = np.cumsum(s)
    s_norm_cum = s_cum/sum
    s_norm_cum_cut = s_norm_cum[s_norm_cum <= variance_cutoff]
    cut_idx = len(s_norm_cum_cut)
    return U, S, V, X_Mean, cut_idx


def compute_pca(features_array, use_gpu=False):
    """
    Compute the pca decomposition of the features array
    :param features_array: Features array (Row feature)
    :param use_gpu: Do computation on GPU (Needs a lot of memory)
    :return: Returns the U, S, V matrices of the composition
    """

    X = torch.from_numpy(features_array)
    X_mean = torch.mean(X, dim=0)
    X = (X - X_mean.expand_as(X))

    if use_gpu:
        X = Variable(X).gpu()

    U, S, V = torch.svd(torch.t(X))

    return U.cpu(), S.cpu(), V.cpu()





