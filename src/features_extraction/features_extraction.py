import os
import argparse
import numpy as np
import pickle as pkl
import bz2
import time
import cv2
import torch
import torchvision.models as models
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torchvision.transforms import *
import pandas as pd
from PIL import Image
import datetime


APPLICATION_DESCRIPTION = 'Features extraction from pretrained CNN'


def parse_args():
    """
    Parse the arguments of the main
    :return:
    """
    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--model_id', type=str, default='RESNET34',
                        choices=['RESNET34', 'RESNET152', 'VGG11', 'VGG13', 'VGG16', 'VGG19'],
                        help='Pretrained model id')

    parser.add_argument('--input_images_path_datasets', nargs='+', type=str,
                        help='Relative path to input images')
    parser.add_argument('--images_base_directory', type=str, default='',
                        help='Base directory where images are stocked')

    parser.add_argument('--num_workers', type=int, default=16,
                        help='Number of threads used for data loading')
    parser.add_argument('--batch_size', type=int, default=12,
                        help='Batch size processing')
    parser.add_argument('--shuffle', action='store_true',
                        help='Shuffle data')
    parser.add_argument('--gpu_mode', action='store_true',
                        help='Use gpu')
    parser.add_argument('--save_dir', type=str, default='',
                        help='Output directory')

    parser.add_argument('--grid_xsize', type=int, default=3,
                        help='Horizontal number of rois')
    parser.add_argument('--grid_ysize', type=int, default=3,
                        help='Vertical number of rois')
    parser.add_argument('--bottom_cropping', type=int, default=20,
                        help='Number of pixels to remove at the base of the images')

    return parser.parse_args()


def load_img(file_path):
    """
    Loads an image with the PIL library in RGB format
    :param file_path:
    :return: Returns the PIL image object
    """
    # HOTFIX! resize image to make up for uneven input image sizes:
    img = Image.open(file_path).resize((300, 225), Image.LANCZOS).convert('RGB')
    return img


def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path


def print_network(net):
    """
    print the architecture of the network and the number of parammeters
    :param net: input model
    :return:
    """
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


def cvresize(img, dsize, interpolation=cv2.INTER_LINEAR):
    """
    Resize an image
    :param img: image array
    :param dsize: resize size
    :param interpolation: interpolation algorithm
    :return: Returns the new resized image
    """
    out_img = np.zeros((img.shape[0], dsize[1], dsize[0]))
    for k in range(img.shape[0]):
        out_img[k] = cv2.resize(img[k], dsize=dsize, interpolation=interpolation)
    return out_img


def get_naming_from_path(file_path):
    """
    Returns the last directory name and the base name of the file
    :param file_path:
    :return:
    """
    basename = os.path.basename(file_path).split('.')[0]
    subdir = os.path.basename(os.path.dirname(file_path))
    return subdir, basename


def is_image_file(file_path):
    """
    Checks only if the extension is of type of the major image format.
    :param file_path: Path to a file
    :return: It return True if the extension is in the extensions list, otherwise False.
    """
    return any(file_path.endswith(extension) for extension in [".png", ".jpg", ".jpeg", ".bmp", "tif"])


class DatasetFromCSV(Dataset):
    """
        Dataset for managing the input of images
    """

    def __init__(self, parameters):
        super(DatasetFromCSV, self).__init__()

        input_images_path_dataset = parameters['input_images_path_dataset']
        images_base_directory = parameters['images_base_directory']

        df = pd.read_csv(filepath_or_buffer=input_images_path_dataset, usecols=['IMAGE_ID', 'SUBDIR'],
                         dtype={'IMAGE_ID': object, 'SUBDIR': object})
        self.df_size = df.shape[0]
        print(df.shape)

        self.image_filenames = []
        image_filenames = []
        for row in df.iterrows():
            fn = os.path.join(images_base_directory, '%s/%s.jpg' % (row[1].SUBDIR, row[1].IMAGE_ID))
            image_filenames.append(fn)

        # image_filenames = image_filenames[0:10]
        b_check = parameters['check_image_type_ok']
        if b_check:
            for filepath in image_filenames:
                if is_image_file(filepath):
                    self.image_filenames.append(filepath)
        else:
            self.image_filenames = image_filenames

        transform_list = [ToTensor(), Normalize(parameters['mean'], parameters['std'])]

        self.transform = Compose(transform_list)

    def __getitem__(self, index):
        # load image
        fn = self.image_filenames[index]

        img = load_img(fn)
        img_flip = img.transpose(Image.FLIP_LEFT_RIGHT)
        img_flip = self.transform(img_flip)

        img = self.transform(img)

        return img, fn, img_flip

    def __len__(self):
        return len(self.image_filenames)


def load_dataset_csv(parameters):
    """
    Creates the dataloader associated to image files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the images files path list
    """
    ds = DatasetFromCSV(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])


def get_model_data(model_id):
    """
    Selects the model an returns the modified pretrained model and the size of the output feature.  The possible models
    are : VGG11, VGG13, VGG16, VGG19, RESNET34, RESNET152.
    :param model_id:
    :return: Returns the modified model and the output feature size
    """
    pretrained_net = None
    feature_size = -1
    if model_id == 'VGG11':
        pretrained_net = models.vgg16(pretrained=True)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        # from the end
        trunc_idx = -3
        feature_size = 4096
    elif model_id == 'VGG13':
        pretrained_net = models.vgg16(pretrained=True)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        # from the end
        trunc_idx = -3
        feature_size = 4096
    elif model_id == 'VGG16':
        pretrained_net = models.vgg16(pretrained=True)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        # from the end
        trunc_idx = -3
        feature_size = 4096
    elif model_id == 'VGG19':
        pretrained_net = models.vgg19(pretrained=True)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        # from the end
        trunc_idx = -3
        feature_size = 4096
    elif model_id == 'RESNET152':
        pretrained_net = models.resnet152(pretrained=True)
        print_network(pretrained_net)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        parameters['std'] = [1.0, 1.0, 1.0]  # ??
        # from the end
        trunc_idx = -1
        feature_size = 2048
    elif model_id == 'RESNET34':
        pretrained_net = models.resnet34(pretrained=True)
        print_network(pretrained_net)
        parameters['model_input_size'] = 224
        parameters['mean'] = [0.485, 0.456, 0.406]
        parameters['std'] = [0.229, 0.224, 0.225]
        parameters['std'] = [1.0, 1.0, 1.0]  # ??
        # from the end
        trunc_idx = -1
        feature_size = 512
    else:
        raise ValueError(f"Unknown model: '{model_id}'")

    pretrained_net = torch.nn.Sequential(*list(pretrained_net.children())[:trunc_idx])

    return pretrained_net, feature_size


def stack_data(images, rois_rects):

    rois_list = []
    n = images.shape[0]

    for k in range(n):
        for y0, x0, y1, x1 in rois_rects:
            roi = images[k, :, y0:y1, x0:x1].numpy()

            roi0 = np.zeros(
                (1, roi.shape[0], parameters['model_input_size'], parameters['model_input_size']))
            roi0[0] = cvresize(roi, dsize=(parameters['model_input_size'], parameters['model_input_size']))

            roi0 = torch.from_numpy(roi0.astype('float32'))

            rois_list.append(roi0)

    rois_list = np.vstack(rois_list)
    return rois_list


def _feature_extraction(images, rois_rects, pretrained_net, n_features):

    features_array_0 = stack_data(images, rois_rects)

    if parameters['gpu_mode']:
        features_array_0 = Variable(torch.from_numpy(features_array_0)).cuda()
    else:
        features_array_0 = Variable(torch.from_numpy(features_array_0)).cpu()

    ########################################################
    features = pretrained_net.forward(features_array_0)
    #######################################################

    features = np.squeeze(features.cpu().data.numpy())

    n = len(rois_rects)
    features_array = []
    for k in range(n_features):
        features_array.append(features[k * n: (k + 1) * n, :].flatten())

    return features_array


def features_extraction(parameters):
    """
    Compute deepfeatures from pretrained CNN
    :param parameters:
    :return:
    """

    if parameters['gpu_mode'] and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without")
    elif parameters['gpu_mode']:
        print('Using GPU')
    else:
        print('Using CPU')

    print(parameters)

    # Setting the grid size
    grid_xsize = parameters['grid_xsize']
    grid_ysize = parameters['grid_ysize']

    grid_name = '%ix%i' % (grid_xsize, grid_ysize)

    model_name = parameters['model_id']

    # Returns the truncked pre-trained model with the features size
    pretrained_net, feature_size = get_model_data(parameters['model_id'])

    # Print the the model
    print_network(pretrained_net)

    # loss function
    if parameters['gpu_mode']:
        pretrained_net.cuda()
    else:
        pretrained_net.cpu()

    # Model mode
    pretrained_net.eval()

    for input_images_path_dataset in parameters['input_images_path_datasets']:

        start0 = time.time()

        parameters['input_images_path_dataset'] = input_images_path_dataset
        print("Processing : %s" % input_images_path_dataset)

        # Setting the dataloader
        parameters['check_image_type_ok'] = False
        data_loader = load_dataset_csv(parameters=parameters)

        # Iterate over all images
        features_array = []
        for iter, (images_0, files_path, images_1) in enumerate(data_loader):

            # Small crop to remove the bottom lines containing the acquisition timestamp
            images_0 = images_0[:, :, 0:-parameters['bottom_cropping'], :]
            images_1 = images_1[:, :, 0:-parameters['bottom_cropping'], :]

            print('batch %i/%i (%i) : %s' % (iter, len(data_loader),  len(files_path),  files_path))

            x_size = images_0.shape[3]
            y_size = images_0.shape[2]

            x_stepsize = int(x_size / grid_xsize)
            y_stepsize = int(y_size / grid_ysize)

            # Compute the rois coordinates (top, bottom)
            rois_rects = []
            for y in range(grid_ysize):
                y0 = y * y_stepsize
                y1 = (y + 1) * y_stepsize
                for x in range(grid_xsize):
                    x0 = x * x_stepsize
                    x1 = (x + 1) * x_stepsize
                    rois_rects.append((y0, x0, y1, x1))

            n_features = len(files_path)

            # Compute the features
            features_array_0 = _feature_extraction(images_0, rois_rects, pretrained_net, n_features)

            features_array_1 = _feature_extraction(images_1, rois_rects, pretrained_net, n_features)

            for fn, feature_0, feature_1 in zip(files_path, features_array_0, features_array_1):
                subdir, basename = get_naming_from_path(fn)
                features_array.append((basename, subdir, feature_0, feature_1))

        # Save features dataset
        ofn = os.path.basename(parameters['input_images_path_dataset']).split('.')[0]
        ofn = ofn.replace('images', 'features')
        force_create_directory(parameters['save_dir'])
        fn = os.path.join(parameters['save_dir'], '%s_%s_%s.pkl.bz2' % (ofn, model_name, grid_name))
        pkl.dump(features_array, bz2.open(fn, 'wb'))
        print("Save: %s" % fn)
        end0 = time.time()
        print('----- Batch : Elapsed time: %f secs' % (end0 - start0))


# Main function
if __name__ == '__main__':
    start = time.time()
    print('----- %s' % APPLICATION_DESCRIPTION)
    args = parse_args()
    parameters = vars(args)
    ######################################
    features_extraction(parameters)
    ######################################
    end = time.time()
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
    print('----- Elapsed time: %f secs' % (end - start))
