from dataset import DatasetFromImageList
from dataset import DatasetFromCSV
from dataset import FeaturesFromFileList
from dataset import SimilaritiesFromFileList
from torch.utils.data import DataLoader


def load_dataset(parameters):
    """
    Creates the dataloader associated to image files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the images files path list
    """
    ds = DatasetFromImageList(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])

def load_dataset_csv(parameters):
    """
    Creates the dataloader associated to image files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the images files path list
    """
    ds = DatasetFromCSV(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])


def load_features_dataset(parameters):
    """
    Creates the dataloader associated to the features files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the features files path list
    """
    ds = FeaturesFromFileList(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])




def load_similarities_dataset(parameters):
    """
    Creates the dataloader associated to the similarity features files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the similarity files path list
    """
    ds = SimilaritiesFromFileList(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=False)
