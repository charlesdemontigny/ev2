import os
import argparse
import torch
from torch.autograd import Variable
import fnmatch
import pickle as pkl
import bz2
import time
from torch.utils.data import DataLoader
import torch.utils.data as data
import matplotlib.pyplot as plt
import numpy as np
import datetime


APPLICATION_DESCRIPTION = 'Apply PCA on features'


def load_pca_data(pca_file_path, variance_cutoff=0.65):
    """
    Load a pca pickle file and return The U, S and V matrices and the index corresponding to the cumulative
        variance.
    :param pca_file_path:
    :param variance_cutoff:
    :return: Returns U, S, V and the cut_idx
    """

    U, S, X_Mean = torch.load(pca_file_path)
    s = S.data.numpy()
    sum = np.sum(s)
    s_cum = np.cumsum(s)
    s_norm_cum = s_cum/sum
    s_norm_cum_cut = s_norm_cum[s_norm_cum <= variance_cutoff]
    cut_idx = len(s_norm_cum_cut)
    return U, S, X_Mean, cut_idx


MATCH_PATTTERN = 'features_batch_000000_RESNET152_3x3.pkl.bz2'
DEFAULT_FEATURES_BASE_DIRECTORY = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/raw/vision/features_RESNET152_3x3'
DEFAULT_SAVE_DIR = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/raw/vision/features_RESNET152_3x3_pca'
DEFAULT_PCA_FILE_PATH = '/misc/data18/projets/multi/jrl01/data/processed/pca/RESNET152.pkl'

DEFAULT_GRID_XSIZE = 3
DEFAULT_GRID_YSIZE = 3
DEFAULT_FEATURES_SIZE = 2048


def parse_args():
    """
    Parse the arguments of the main
    :return:
    """
    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--match_pattern', type=str, default=MATCH_PATTTERN,
                        help='Filter match pattern')
    parser.add_argument('--features_datasets_directory', type=str, default=DEFAULT_FEATURES_BASE_DIRECTORY,
                        help='Input base features directory')
    parser.add_argument('--save_dir', type=str, default=DEFAULT_SAVE_DIR,
                        help='Output directory')
    parser.add_argument('--bshow', action='store_true',
                        help='Show results')
    parser.add_argument('--pca_file_path', type=str, default=DEFAULT_PCA_FILE_PATH,
                        help='File path to the pkl file of U, S, V matrices')
    parser.add_argument('--grid_xsize', type=int, default=DEFAULT_GRID_XSIZE,
                        help='Horizontal number of rois')
    parser.add_argument('--grid_ysize', type=int, default=DEFAULT_GRID_YSIZE,
                        help='Vertical number of rois')
    parser.add_argument('--feature_size', type=int, default=DEFAULT_FEATURES_SIZE,
                        help='Base feature size')
    parser.add_argument('--variance_cutoff', type=float, default=1.0,
                        help='Proportion of variance used to compute the PCA')
    parser.add_argument('--num_workers', type=int, default=16,
                        help='Number of threads used for data loading')
    parser.add_argument('--batch_size', type=int, default=1,
                        help='Batch size processing')
    parser.add_argument('--gpu_mode', action='store_true',
                        help='Use gpu')
    parser.add_argument('--shuffle', action='store_true',
                        help='Shuffle data')
    parser.add_argument('--normalize', action='store_true',
                        help='Normalise feature')

    return parser.parse_args()


def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path


class DatasetsFeaturesFromDirectories(data.Dataset):
    """
    Dataset for managing the input of deep features
    """
    def __init__(self, parameters):
        super(DatasetsFeaturesFromDirectories, self).__init__()

        file_paths_list = []
        for directory_path, dirs, file_paths in os.walk(parameters['features_datasets_directory']):
            for file_path in file_paths:
                if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                    file_paths_list.append(os.path.join(directory_path, file_path))

        n_files = len(file_paths_list)
        print('Number of files: %i' % n_files)

        if not n_files:
            print("No files!")
            exit(0)

        self.filenames = file_paths_list

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        return fn

    def __len__(self):
        return len(self.filenames)


def load_datasets_features_dataset(parameters):
    """
    Creates the dataloader associated to the features files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the features files path list
    """
    ds = DatasetsFeaturesFromDirectories(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])


def show(feature_before, feature_after):
    fig = plt.figure()
    a = fig.add_subplot(2, 1, 1)
    plt.plot(np.arange(feature_before.shape[0]), feature_before)
    a = fig.add_subplot(2, 1, 2)
    plt.plot(np.arange(feature_before.shape[0]), feature_after)
    plt.show()


def reduce_dimensionality_pca(parameters):

    print(parameters)

    grid_xsize = parameters['grid_xsize']
    grid_ysize = parameters['grid_ysize']
    variance_cutoff = parameters['variance_cutoff']
    pca_file_path = parameters['pca_file_path']

    U, S, X_Mean, cut_idx = load_pca_data(pca_file_path, variance_cutoff=variance_cutoff)

    sum = torch.sum(S)
    S_Norm = S/sum

    datasets = load_datasets_features_dataset(parameters)

    # Iterate over all features
    step_size = grid_ysize * grid_xsize

    feature_before=None
    feature_after=None

    for iter, (fns) in enumerate(datasets):

        features_array = []
        for fn in fns:
            b_once = True
            print("Loading: %s" % fn)
            basename_parts = os.path.basename(fn).split('.')
            output_name = '%s_%s=%i.%s.%s' % (basename_parts[0], 'pca', cut_idx*step_size, basename_parts[1],
                                              basename_parts[2])
            dataset = pkl.load(bz2.open(fn, 'rb'))

            for data in dataset:
                feature_0 = torch.from_numpy(data[2])
                feature_1 = torch.from_numpy(data[3])

                if parameters['bshow'] and b_once:
                    feature_before = np.copy(data[2])

                for chk in torch.chunk(feature_0, step_size):
                    features_array.append(chk)

                for chk in torch.chunk(feature_1, step_size):
                    features_array.append(chk)

            print("Create features stack")
            X = Variable(torch.stack(features_array))
            print(X.shape)
            features_array = []
            print("Reproject with PCA")

            X = X - X_Mean.expand_as(X)
            X_repoj = torch.mm(X, U[:, :cut_idx])

            if parameters['normalize']:
                S_Norm2 = S_Norm[:cut_idx].expand_as(X_repoj)
                X_repoj = X_repoj*S_Norm2

            print(X_repoj.shape)
            k = 0
            features_array = []

            for data in dataset:
                feature_0 = X_repoj[k * step_size: (k + 1) * step_size, :].data.numpy().flatten()
                feature_1 = X_repoj[(k + 1) * step_size: (k + 2) * step_size, :].data.numpy().flatten()
                features_array.append((data[0], data[1], feature_0, feature_1))

                if parameters['bshow'] and b_once:
                    feature_after = np.copy(feature_0)

                    show(feature_before, feature_after)
                    b_once = False
                k += 2

            force_create_directory(parameters['save_dir'])
            fn = os.path.join(parameters['save_dir'], output_name)
            print('Save file: %s' % fn)
            pkl.dump(features_array, bz2.open(fn, 'wb'))


# Main function
if __name__ == '__main__':
    start = time.time()
    print('----- %s' % APPLICATION_DESCRIPTION)
    args = parse_args()
    parameters = vars(args)
    ######################################
    reduce_dimensionality_pca(parameters)
    ######################################
    end = time.time()
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
    print('----- Elapsed time: %f secs' % (end - start))
