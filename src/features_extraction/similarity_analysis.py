import glob
import os
import pandas as pd
import numpy as np
import matplotlib as plt


subdirs = ['cutoff=0.020','cutoff=0.050','cutoff=0.125','cutoff=0.250','cutoff=0.500','cutoff=0.750','cutoff=0.950',
           'cutoff=1.000']

BASE_DIRECTORY = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/interim/similarity/RESNET152/3x3'

all_files = []
for subdir in subdirs:

    idir = os.path.join(BASE_DIRECTORY, '%s/%s' % (subdir, '000000'))
    pth = os.path.join(idir, "0000000002*.csv")
    all_files.append(glob.glob(pth))


all_files = [item for sublist in all_files for item in sublist]

print(all_files)

df_from_each_file = (pd.read_csv(f)[['idxs_topn']] for f in all_files)

concatenated_df = pd.concat(df_from_each_file, ignore_index=False, axis=1)



print(concatenated_df.to_string())

n_cols = len(concatenated_df.columns)

print(n_cols)

max_idx = 4
base_col = concatenated_df.iloc[:, -1][0:max_idx]

n_data = float(len(base_col.values))

print(base_col.to_string())

for idx in range(n_cols):
    data_col = concatenated_df.iloc[:, idx]

    #print(data_col.to_string())

    n = 0
    for i in data_col:
        if np.isin(i, base_col.values):
            n+=1

    sames = np.sum((base_col == data_col[0:max_idx]).values)


    print(n/n_data, sames/n_data)

    toto = 0









toto = 0