import os
import argparse
import torch
import numpy as np
import pickle as pkl
import bz2
import time
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torch.autograd import Variable
from torch.nn.functional import cosine_similarity
from torchvision.utils import make_grid
from torchvision.transforms import *
from PIL import Image
import fnmatch
import pandas as pd


APPLICATION_DESCRIPTION = 'Compute similarities between features'


def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

MATCH_PATTTERN_0 = 'features_batch_000000_RESNET152_3x3_pca=18432.pkl.bz2'
MATCH_PATTTERN_1 = 'features_batch_000100_RESNET152_3x3_pca=18432.pkl.bz2'


MATCH_PATTTERN_0 = 'features_batch_000000_RESNET152_3x3*'
MATCH_PATTTERN_1 = 'features_batch_000000_RESNET152_3x3*'

DEFAULT_FEATURES_BASE_DIRECTORY = '/misc/data18/projets/multi/jrl01/nobackup/develop/jlr1/data/raw/vision/features_RESNET152_3x3_pca'
IMAGE_BASE_DIRECTORY = '/misc/data18/projets/multi/jrl01/nobackup/raw/images/vicky'

SAVE_DIR = os.path.join(DEFAULT_FEATURES_BASE_DIRECTORY, 'features_batch_RESNET152_3x3_pca=18432_test3')

def parse_args():
    """
    Parse the arguments of the main
    :return:
    """
    parser = argparse.ArgumentParser(description=APPLICATION_DESCRIPTION)
    parser.add_argument('--match_pattern_0', type=str,
                        default=MATCH_PATTTERN_0,
                        help='Filter match pattern')

    parser.add_argument('--match_pattern_1', type=str,
                        default=MATCH_PATTTERN_1,
                        help='Filter match pattern')

    parser.add_argument('--features_datasets_directory', type=str, default=DEFAULT_FEATURES_BASE_DIRECTORY, help='Input base features directory')

    parser.add_argument('--images_base_directory', type=str, default=IMAGE_BASE_DIRECTORY, help='Path to input images')

    parser.add_argument('--save_dir', type=str, default=SAVE_DIR, help='Output directory')
    parser.add_argument('--bsave', action='store_true', help='Save data')
    parser.add_argument('--bshow', action='store_true', help='Show results')
    parser.add_argument('--num_workers', type=int, default=16, help='Number of threads used for data loading')
    parser.add_argument('--batch_size', type=int, default=1, help='Batch size processing')
    parser.add_argument('--gpu_mode', action='store_true', help='Use gpu')
    parser.add_argument('--shuffle', action='store_true', help='Shuffle data')
    return parser.parse_args()

def show(img_0, img):
    nimg_0 = img_0.numpy()
    npimg = img.numpy()
    fig = plt.figure()
    a = fig.add_subplot(2, 1, 1)
    plt.imshow(np.transpose(nimg_0, (1, 2, 0)), interpolation='nearest')
    a = fig.add_subplot(2, 1, 2)
    plt.imshow(np.transpose(npimg, (1,2,0)), interpolation='nearest')


def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path


class DatasetsFeaturesFromDirectories(Dataset):
    """
    Dataset for managing the input of deep features
    """
    def __init__(self, parameters):
        super(DatasetsFeaturesFromDirectories, self).__init__()

        file_paths_list = []
        for directory_path, dirs, file_paths in os.walk(parameters['features_datasets_directory']):
            for file_path in file_paths:
                if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                    file_paths_list.append(os.path.join(directory_path,file_path ))

        n_files = len(file_paths_list)
        print('Number of files: %i' % n_files)

        self.filenames = file_paths_list

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        return fn

    def __len__(self):
        return len(self.filenames)


def load_datasets_features_dataset(parameters):
    """
    Creates the dataloader associated to the features files path list
    :param parameters: Input parameters
    :return: Returns the dataloader associated with the features files path list
    """
    ds = DatasetsFeaturesFromDirectories(parameters)
    return DataLoader(dataset=ds, num_workers=parameters['num_workers'], batch_size=parameters['batch_size'],
                      shuffle=parameters['shuffle'])


def similar_images(parameters):

    print(parameters)

    base_feature_size = 2048
    max_feature_idx = 2048
    min_feature_idx = 0
    n_subfeatures = 9
    feature_filter = np.zeros(base_feature_size, dtype=np.float32)
    feature_filter[min_feature_idx:max_feature_idx] = 1.0

    weights = [0.25, 1.0, 0.25,
               1.0, 1.0, 1.0,
               0.25, 1.0, 0.25]
    filter = []
    for k in range(n_subfeatures):
        feature_filter = np.zeros(base_feature_size, dtype=np.float32)
        feature_filter[min_feature_idx:max_feature_idx] = 1.0*weights[k]
        filter.append(feature_filter)

    filter = np.hstack(filter)

    use_gpu = parameters['gpu_mode']

    images_base_directory = parameters['images_base_directory']

    top_n = 128

    if use_gpu and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda")

    parameters['match_pattern'] = parameters['match_pattern_1']
    datasets1 = load_datasets_features_dataset(parameters)
    parameters['batch_size'] = 1

    parameters['match_pattern'] = parameters['match_pattern_0']
    datasets0 = load_datasets_features_dataset(parameters)

    data_list_0 = []
    data_list_1 = []
    images_file_path_1 = []
    for iter, (fns) in enumerate(datasets1):

        for fn in fns:
            print("Loading features : %s" % fn)
            dataset = pkl.load(bz2.open(fn, 'rb'))
            for data in dataset:
                data_list_0.append(torch.from_numpy(data[2]*filter))
                data_list_1.append(torch.from_numpy(data[3]*filter))
                images_file_path_1.append(os.path.join(images_base_directory, '%s/%s.jpg' % (data[1], data[0])))

    features1_0 = Variable(torch.stack(data_list_0))
    features1_1 = Variable(torch.stack(data_list_1))

    if use_gpu:
        features1_0 = features1_0.cuda()
        features1_1 = features1_1.cuda()

    for iter, (fns) in enumerate(datasets0):

        for fn in fns:
            print("Loading features : %s" % fn)
            dataset = pkl.load(bz2.open(fn, 'rb'))
            for data in dataset:
                feature0_0 = torch.from_numpy(data[2])
                feature0_1 = torch.from_numpy(data[3])
                fn_0 = os.path.join(images_base_directory, '%s/%s.jpg' % (data[1], data[0]))

                feature0_0 = Variable(feature0_0.expand_as(features1_0))
                feature0_1 = Variable(feature0_1.expand_as(features1_1))

                if use_gpu:
                    feature0_0 = feature0_0.cuda()
                    feature0_1 = feature0_1.cuda()

                dist_00_10 = cosine_similarity(feature0_0, features1_0)
                dist_01_10 = cosine_similarity(feature0_1, features1_0)
                dist_00_11 = cosine_similarity(feature0_0, features1_1)
                dist_01_11 = cosine_similarity(feature0_1, features1_1)

                dists = torch.stack([dist_00_10, dist_01_10, dist_00_11, dist_01_11], 1)
                dists_max, idxs = torch.max(dists, 1)
                dists_max, idxs = torch.sort(dists_max, descending=True)

                dist_topn = dists_max[0:top_n].cpu().data.numpy()
                idxs_topn = idxs[0:top_n].cpu().data.numpy()

                transform = Compose([ToTensor()])

                images_list = []
                images_paths = []

                base_image = transform(Image.open(fn_0).convert('RGB'))
                for idx, dist in zip(idxs_topn, dist_topn):
                    img = Image.open(images_file_path_1[idx]).convert('RGB')
                    images_list.append(transform(img))
                    images_paths.append(images_file_path_1[idx])
                    #print(images_file_path_1[idx], dist)

                output_data = dict()

                output_data['idxs_topn'] = idxs_topn
                output_data['dist_topn'] = dist_topn
                output_data['images_paths'] = images_paths

                # print(output_data)

                top_images = make_grid(images_list, nrow=16)

                show(base_image, top_images)

                if parameters['bsave']:
                    df = pd.DataFrame.from_dict(output_data)
                    force_create_directory(parameters['save_dir'])
                    ofn = os.path.join(parameters['save_dir'], '%s.csv' % data[0])
                    df.to_csv(ofn)
                    print(ofn)
                    ofn = os.path.join(parameters['save_dir'], '%s.jpg' % data[0])
                    plt.savefig(ofn, dpi=300)
                    print(ofn)

                if parameters['bshow']:
                    plt.show()

                plt.gcf().clear()


# Main function
if __name__ == '__main__':
    start = time.time()
    print('----- %s' % APPLICATION_DESCRIPTION)
    args = parse_args()
    parameters = vars(args)
    ######################################
    similar_images(parameters)
    ######################################
    end = time.time()
    print('----- Elapsed time: %f secs' % (end - start))