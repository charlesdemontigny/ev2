
clear all;
close all;


%charger les feature calculer par le script genere_MAC_features.m
load('features_JLR_rmacvLw_7188.mat');



if ismac
    % Code to run on Mac plaform
elseif isunix
    % Code to run on Linux plaform
    chem= './raw/images/vicky_0.8%_sample/';
elseif ispc
    % Code to run on Windows platform
    chem= './raw/images/vicky_0.8%_sample/';
else
    disp('Platform not supported')
end

image_requete = '0000000009.jpg';


idx = find(strcmp(im_names, image_requete));

vec = features(idx,:);


m_best=128;


% calcul les similarités de l'image requete avec toutes les les features calculés des autres images
sim = features*vec';
[sim, ranks] = sort(sim, 'descend');


figure;
h1=axes;
set(h1, 'Ydir', 'reverse')
title(['Image requete (coin haut-gauche)' image_requete ])
hold on
for j=1: size(ranks,2)
    
    
    for i=1: m_best %size(ranks,1)
        r = ranks(i,j);
        nom =im_names{(r)};
        im = imresize((imread([chem nom(1:6) '/' nom])),0.9);
  
        imagesc([mod(i-1,8) mod(i-1,8)+1],[ floor((i-1)/8) floor((i-1)/8)+1],im)
      
      
        if((r)~= idx(j) )
              text(mod(i-1,8)+0.25,floor((i-1)/8)+0.5,num2str(round(sim(i,j),2)),'Color','yellow','FontSize',9)
        else
              text(mod(i-1,8)+0.25,floor((i-1)/8)+0.5,[num2str(round(sim(i,j),2)) '*'],'Color','yellow','FontSize',9)
        end
    end
end
      




     