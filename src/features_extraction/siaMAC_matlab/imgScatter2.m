function imgScatter2(Y, chem, im_names, mask, imgW, imgH, m_best)
% IMGSCATTER - Scatter plot with images
%   
% SYNTAX
%

 
%   IMGSCATTER( YDATA, IMG )
%   IMGSCATTER( YDATA, IMG, MASK )
%   IMGSCATTER( YDATA, IMG, MASK, IMGW, IMGH )
%
% INPUT
%
%   YDATA       2-D points for scatter plot             [n-by-2]
%   IMG         Images for each point                   [p-by-r-by-n]
% 
% OPTIONAL
% 
%   MASK        Mask to plot subset of image            [n-by-1]
%               {default: ones(n,1)}
%   IMGW        Width of each image                     [scalar]
%               {default: 0.05}
%   IMGH        Height of each image                    [scalar]
%               {default: 0.05}
%
% DESCRIPTION
%
%   IMGSCATTER( YDATA, IMG ) plots the 2D points given in matrix
%   YDATA with the corresponding images in matrix IMG.
%
% See also      scatter
%
  
  if ~exist('mask', 'var') || isempty(mask)
    mask = ones(length(Y), 1);
  end
  
  if ~exist('imgW', 'var') || isempty(imgW)
    imgW = 0.05;
  end
  
  if ~exist('imgH', 'var') || isempty(imgH)
    imgH = 0.05;
  end
  
  n = size(Y, 1);
  
  figure
  colormap default;
  scatter(Y(:, 1), Y(:, 2), '.');
  axis equal tight
  title('Scatter plot of image similarities')
  hold on
  
  for i = 1:n
    
    if mask(i) == 1
      %im = I(:,:,i);
      nom=im_names{i};
      %im = imresize(rgb2gray(imread([chem nom(1:6) '/' nom])),0.9);
      im = imresize((imread([chem nom(1:6) '/' nom])),0.9);
      
      ys=linspace(Y(i, 2) - imgW/2, Y(i, 2) + imgW/2, size(im, 1) );
      xs=linspace(Y(i, 1) - imgH/2, Y(i, 1) + imgH/2, size(im, 2) );
      
      %colormap gray;
      %imagesc(xs,ys,imrotate(im',90))
      if(length(size(im))==3)
           imagesc(xs,ys,imrotate(permute(im,[2 1 3]),90))
      else
           imagesc(xs,ys,imrotate(im',90))
      end
      
      if(rem(i-1,m_best)==0)
         rectangle('Position',[min(xs) min(ys) max(xs)-min(xs) max(ys)-min(ys) ], 'EdgeColor','r')
      end
      
    end
      
  end % 
  
  
end


%%------------------------------------------------------------
%
% AUTHORS
%
%   Kostas Mylonakis                        mylonakk@auth.gr
%   Dimitris Floros                         fcdimitr@auth.gr
%
% VERSION
%
%   1.0 - May 25, 2017
%
% CHANGELOG
%
%   1.0 (May 25, 2017) - Dimitris
%       * added mask to show subset of images
%       * fixed minor bug (image rotation)
%   0.1 (May 24, 2017) - Dimitris
%       * initial implementation
%
% ------------------------------------------------------------
