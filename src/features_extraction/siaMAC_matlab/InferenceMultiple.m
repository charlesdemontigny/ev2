%
%
%Ce script affiche les images les plus similaires visuellement  à une requete formulée par
%un ensemble de n images.

clear all;
close all;


%charger les features calculés par le script genere_MAC_features.m
load('features_JLR_rmacvLw_7188.mat');


%chemin ou se trouve les dossiers (batchs) des images
if ismac
    % Code to run on Mac plaform
elseif isunix
    % Code to run on Linux plaform
    chem= './raw/images/vicky_0.8%_sample/';
elseif ispc
    % Code to run on Windows platform
    chem= './raw/images/vicky_0.8%_sample/';
else
    disp('Platform not supported')
end


%choisir une requete de 10 images au hazard
n=10;

rng(1000)
idx = randperm(size(features,1));
idx=idx(1:n);


vec = features(idx,:);

% trouve les 15 meilleures images incuant l'image requete elle-même
m_best=15;


%calcul de similarité basé sur le cos
sim = features*vec';
[sim, ranks] = sort(sim, 'descend');

figure;
title('La ligne en bas 10 images requetes, chaque col.= 14 images les +similaires');
hold on
for j=1: size(ranks,2)
    
    
    for i=1: m_best
        r = ranks(i,j);
        nom=im_names{(r)};
        
        im = imresize((imread([chem nom(1:6) '/' nom])),0.9);
        
        if(length(size(im))==3)
           imagesc([j j+1],[i i+1],imrotate(permute(im,[2 1 3]),90))
        else
           imagesc([j j+1],[i i+1],imrotate(im',90))
        end
    
        if(i~=1)
           if((r)~= idx(j) )
                text(j+0.25,i+0.5,num2str(round(sim(i,j),2)),'Color','yellow','FontSize',9)
           else
              text(j+0.25,i+0.5,[num2str(round(sim(i,j),2)) '*'],'Color','yellow','FontSize',9)
           end
        end
    end
end
      
hold off      

best_ranks  = ranks(1:m_best,:);


D = pdist(single(features(best_ranks,:)),'cosine');
p=2;
[Y,e] = cmdscale(double(D),p);

%affiche la disposition des images dans un espace 2d, les distances
%reflètent le maximum possible dans cet espace les similarités entre les images
imgScatter2(Y, chem, {im_names{best_ranks}}, ones(size(Y,1),1), 0.05, 0.05,m_best)





     