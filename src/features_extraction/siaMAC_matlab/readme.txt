Utilisation de features MAC de  
F. Radenovic, G. Tolias, O. Chum, CNN Image Retrieval Learns from BoW: Unsupervised Fine-Tuning with Hard Examples, ECCV 2016
Important: le code de ce répertoire est basé sur le code original de Filip Radenović: http://ptak.felk.cvut.cz/personal/radenfil/siamac/siaMAC_code.tar.gz

1. installer la librairie matconvnet : http://www.vlfeat.org/matconvnet/#obtaining-matconvnet

2. compiler matconvnet : http://www.vlfeat.org/matconvnet/install/#compiling

3. le script "genere_MAC_features.m" permet  de générer les features pour toutes les images dans le chemin spécifié
   exp. /misc/data18/projets/multi/jrl01/nobackup/raw/images/vicky/
   les feature seront stockés dans "features_JLR_rmacvLw_N.mat" comme suit: (N représente la taille du subset d'images considéré)
       features : les features MAC calculés
       im_names  : les noms d'images associés
   
4. utiliser inferenceMultiple.m ou inferenceOne.m pour visualiser les similarités.
