

%Calcul des features MAP à partir d'un CNN fine-tuned.
% Ref.
% F. Radenovic, G. Tolias, O. Chum, CNN Image Retrieval Learns from BoW: Unsupervised Fine-Tuning with Hard Examples, ECCV 2016


close all
clear all


addpath('../matconvnet/matlab')
addpath('../matconvnet/')
run ../matconvnet/matlab/vl_setupnn



USE_GPU = 1;%0;


% Feature extraction with our fine-tuned VGG
load('siaMAC_vgg.mat');
net = dagnn.DagNN.loadobj(net) ;





if(USE_GPU)
    net.move('gpu'); % extract on GPU
end



load('Lw_RMAC_vgg.mat');



 

 if ismac
    % Code to run on Mac plaform
elseif isunix
    % Code to run on Linux plaform
     
    chem= './raw/images/vicky_0.8%_sample/';
elseif ispc
    % Code to run on Windows platform
    chem= './raw/images/vicky_0.8%_sample/';
else
    disp('Platform not supported')
 end

dossiers = dir(chem);



%count_f = uint64(0);
im_names={};
count = uint64(0);




for i =1:length(dossiers)
    

    imagefiles = dir([chem dossiers(i).name '/*.jpg']);
    for j=1:length(imagefiles)
        count =count+1;
        im_names{count}  =  imagefiles(j).name;
        
        if(mod(count,5000 )==1)
           fprintf('%d..',count-1);
        end
    end
    
    
        
end


order = randperm(length(im_names));

n = length(im_names);%10000;

im_names = {im_names{order(1:n)}};
clear('order');

features = zeros(n,512,'single');


for i =1:n
   
     nom=im_names{i};
     
     rmacv = rmac([chem nom(1:6) '/' nom], net);
     rmacv_v = apply_Lw(rmacv, m, P);
     
     features(i,:)=single(rmacv_v);
        
     if(mod(i,n/10 )==0 || i==n)
            
        fprintf('%.2f ',100.*i / n);
        
     end
end



save(['features_JLR_rmacvLw_' num2str(n) '.mat'],'features','im_names');






