import os
import cv2
import numpy as np

def force_create_directory(directory_path):
    """
    forces the creation of a directory

    :param directory_path: absolute path to create
    :return: directory path
    """

    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print('Create directory: %s' % directory_path)

    return directory_path



def print_network(net):
    """
    print the architecture of the network and the number of parammeters
    :param net: input model
    :return:
    """
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


def cvt2cv(rgb):
    """
    Swap channel 0 and 2 of a tensor and convert to numpy.  This set the image
    in the convention of opencv
    :param rgb:
    :return:
    """
    rgb0 = rgb.numpy()
    return cv2.merge([rgb0[2], rgb0[1], rgb0[0]])


def cvt2cv2(rgb):
    """
    Swap channel 0 and 2 of a tensor and convert to numpy.  This set the image
    in the convention of opencv
    :param rgb:
    :return:
    """
    rgb0 = rgb.cpu().numpy()
    return cv2.merge([rgb0[2], rgb0[1], rgb0[0]])


def getnumpy(tensor):
    """
    Convert to numpy a tensor
    :param tensor:
    :return:
    """
    return tensor.numpy()


def get_naming_from_path(file_path):
    """
    Returns the last directory name and the base name of the file
    :param file_path:
    :return:
    """
    basename = os.path.basename(file_path).split('.')[0]
    subdir = os.path.basename(os.path.dirname(file_path))
    return subdir, basename


def cvresize(img, dsize, interpolation=cv2.INTER_LINEAR):
    """
    Resize an image
    :param img: image array
    :param dsize: resize size
    :return: Returns the new resized image
    """
    out_img = np.zeros((img.shape[0], dsize[1], dsize[0]))
    for k in range(img.shape[0]):
        out_img[k] = cv2.resize(img[k], dsize=dsize, interpolation=cv2.INTER_LINEAR)
    return out_img