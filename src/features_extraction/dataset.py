import os
import torch.utils.data as data
from torchvision.transforms import *
from PIL import Image
import pickle as pkl
import bz2
import torch
import fnmatch
import pandas as pd


def make_dataset(data_file_path, base_directory=None):
    """

    :param data_file_path: Path of the file that contains the paths of data used.  Those paths can be absolute
        or relative to the base_directory
    :param base_directory: If the parameter is not None, the path is concatenate with each element of the
        data_file_path
    :return: It returns the list of paths
    """
    data_path_list = []

    with open(data_file_path) as f:
        data_path_list = f.readlines()
    if base_directory is None:
        data_path_list = [x.strip() for x in data_path_list]
    else:
        data_path_list = [os.path.join(base_directory, x.strip()) for x in data_path_list]

    data_path_list.sort()

    return data_path_list


def make_dataset_from_directory(parameters):
    files_path_list = []
    for directory_path, dirs, file_paths in os.walk(parameters['input_directory_path']):
        for file_path in file_paths:
            if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                files_path_list.append(os.path.join(directory_path, file_path))
    return files_path_list


def is_image_file(file_path):
    """
    Checks only if the extension is of type of the major image format.
    :param file_path: Path to a file
    :return: It return True if the extension is in the extensions list, otherwise False.
    """
    return any(file_path.endswith(extension) for extension in [".png", ".jpg", ".jpeg", ".bmp", "tif"])


def load_img(file_path):
    """
    Loads an image with the PIL library in RGB format
    :param file_path:
    :return: Returns the PIL image object
    """
    img = Image.open(file_path).convert('RGB')
    return img


class FeaturesFromFileList(data.Dataset):
    """
    Dataset for managing the input of deep features
    """
    def __init__(self, parameters):
        super(FeaturesFromFileList, self).__init__()

        file_path = parameters['features_file_relative_path']
        filenames = make_dataset(file_path, base_directory=parameters['features_base_directory'])

        filenames_hflip = make_dataset(file_path, base_directory=parameters['features_hflip_base_directory'])

        if parameters['end'] == -1:
            parameters['end'] = len(filenames_hflip)

        self.filenames = filenames[parameters['start']:parameters['end']]
        self.filenames_hflip = filenames_hflip[parameters['start']:parameters['end']]

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        fnh = self.filenames_hflip[index]

        if not os.path.exists(fn):
            print("File does not exist: %s", fn)

        if not os.path.exists(fnh):
            print("File does not exist: %s", fnh)

        return torch.from_numpy(pkl.load(bz2.open(fn, 'rb')).flatten()), fn, \
               torch.from_numpy(pkl.load(bz2.open(fnh, 'rb')).flatten())

    def __len__(self):
        return len(self.filenames)


class FeaturesFromDirectories(data.Dataset):
    """
    Dataset for managing the input of deep features
    """
    def __init__(self, parameters):
        super(FeaturesFromDirectories, self).__init__()

        for directory_path, dirs, file_paths in os.walk(parameters['input_directory_path']):
            sub_directory_name = os.path.basename(directory_path)
            file_paths_list = []
            for file_path in file_paths:
                if fnmatch.fnmatch(file_path, parameters['match_pattern']):
                    relative_path = os.path.join(os.path.basename(directory_path), file_path)
                    print('%s (%s)' % (relative_path, parameters['output_directory_path']))
                    file_paths_list.append(relative_path + '\n')
            n_files = len(file_paths_list)
            print('Number of files: %i' % n_files)


        file_path = parameters['features_file_relative_path']
        filenames = make_dataset(file_path, base_directory=parameters['features_base_directory'])

        filenames_hflip = make_dataset(file_path, base_directory=parameters['features_hflip_base_directory'])

        if parameters['end'] == -1:
            parameters['end'] = len(filenames_hflip)

        self.filenames = filenames[parameters['start']:parameters['end']]
        self.filenames_hflip = filenames_hflip[parameters['start']:parameters['end']]

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        fn1 = self.filenames_hflip[index]
        return torch.from_numpy(pkl.load(bz2.open(fn, 'rb')).flatten()), fn, \
               torch.from_numpy(pkl.load(bz2.open(fn1, 'rb')).flatten()),

    def __len__(self):
        return len(self.filenames)






class SimilaritiesFromFileList(data.Dataset):
    """
    Dataset for managing the input of similarity features
    """
    def __init__(self, parameters):
        super(SimilaritiesFromFileList, self).__init__()

        file_path = parameters['similarities_file_relative_path']
        filenames = make_dataset(file_path, base_directory=parameters['similarities_base_directory'])
        filenames.sort()

        self.filenames = filenames[parameters['start']:parameters['end']]

    def __getitem__(self, index):
        # load image
        fn = self.filenames[index]
        return pkl.load(bz2.open(fn, 'rb'))

    def __len__(self):
        return len(self.filenames)


class DatasetFromImageList(data.Dataset):
    """
        Dataset for managing the input of images
    """
    def __init__(self, parameters):
        super(DatasetFromImageList, self).__init__()

        images_file_relative_path = parameters['images_file_relative_path']
        images_base_directory = parameters['images_base_directory']

        images_file_path = os.path.join(images_base_directory, images_file_relative_path)
        b_check = parameters['check_image_type_ok']

        self.image_filenames = []
        images_path = make_dataset(images_file_path, base_directory=images_base_directory)

        if b_check:
            for filepath in images_path:
                if is_image_file(filepath):
                    self.image_filenames.append(filepath)
        else:
            self.image_filenames = images_path

        transform_list = []

        transform_list.append(ToTensor())
        transform_list.append(Normalize(parameters['mean'], parameters['std']))

        self.transform = Compose(transform_list)

    def __getitem__(self, index):
        # load image
        fn = self.image_filenames[index]

        img = load_img(fn)
        img_flip = img.transpose(Image.FLIP_LEFT_RIGHT)
        img_flip = self.transform(img_flip)

        img = self.transform(img)

        return img, fn, img_flip

    def __len__(self):
        return len(self.image_filenames)



