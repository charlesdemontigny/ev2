# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from sklearn.base import clone, is_classifier
from sklearn.externals.joblib import Parallel, delayed
from sklearn.model_selection import BaseCrossValidator
from sklearn.model_selection._split import check_cv
from sklearn.model_selection._validation import _fit_and_predict
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import indexable


class FreqBasedEventSplit(BaseCrossValidator):
    """Event data cross-validator

    Provides train/test indices to split event data samples based on a
    specified freqency (eg: every months).

    In each split, test indices must be higher than before, and thus shuffling
    in this cross validator is inappropriate.

    Note that unlike standard cross-validation methods, successive
    training sets are supersets of those that come before them.

    This cross-validator is a variation of :class:`TimeSeriesSplit` that
    supports irregularly sampled observations (event data).

    Parameters
    ----------
    test_start_date : str or pd.DateTime
        Date specifing when to start the first train/test split.
        Splits are made, at regular intervals, from the start date, to
        the test_end_date.


    test_end_date : str or pd.DateTime or None
        Date specifing when the test period stops. If None, the date of the
        the last event in the dataset will be used.

    test_frequency : str
        Frequency in pandas format. Ex: 'D' for daily, '1MS' for every month start.
        (see the frequency aliases here:
        http://pandas.pydata.org/pandas-docs/stable/timeseries.html#offset-aliases).
        Important: this cross-validator requires frequency that are set to the begin
        of periods (i.e. use 'MS', 'YS', 'QS' instead of 'M', 'Y', 'Q').

    date_time_column : str
        Name of the datetime column to use in the input DataFrame
        to perform train/test splits.

    Note
    ----------
    This cross-validator requires an pandas DataFrame as input.
    """

    def __init__(self, test_start_date,  test_frequency, date_time_column, test_end_date=None):
        self.test_start_date = test_start_date
        self.test_end_date = test_end_date
        self.test_frequency = test_frequency
        self.date_time_column = date_time_column

    def _get_date_test_ranges(self, X):
        start = self.test_start_date
        if self.test_end_date:
            end = self.test_end_date + pd.Timedelta(value=1, unit='D')
        else:
            end = X[self.date_time_column].max() + pd.Timedelta(value=1, unit='D')
        ranges = list(pd.date_range(start=start, end=end, freq=self.test_frequency, closed='left'))
        return ranges + [end]

    def split(self, X, y=None, groups=None):

        ranges = self._get_date_test_ranges(X)

        for start, end in zip(ranges[:-1], ranges[1:]):

            train_indices, = np.where(X[self.date_time_column] < start)

            test_indices, = np.where((X[self.date_time_column] >= start)
                                     & (X[self.date_time_column] < end))

            yield (train_indices, test_indices)

    def get_n_splits(self, X, y=None, groups=None):
        """Returns the number of splitting iterations in the cross-validator
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples
            and n_features is the number of features.
        y : object
            Always ignored, exists for compatibility.
        groups : object
            Always ignored, exists for compatibility.
        Returns
        -------
        n_splits : int
            Returns the number of splitting iterations in the cross-validator.
        """
        if X is None:
            raise ValueError("The 'X' parameter should not be None.")
        return len(self._get_date_test_ranges(X)) - 1


def cross_val_predict_df(estimator, X, y=None, groups=None, cv=None, n_jobs=1,
                         verbose=0, fit_params=None, pre_dispatch='2*n_jobs',
                         method='predict'):
    """Generate cross-validated estimates for each input data point

    This function a patch of `sklearn.model_selection.cross_val_predict`
    function that supports TimeSeriesSplit and FreqBasedEventSplit cross-validators.
    See: https://github.com/scikit-learn/scikit-learn/issues/8043.

    Unlike the orignal function, this function returns a DataFrame with
    two columns, `pred` and `fold`, and an index `test_indices`.

    Parameters
    ----------
    estimator : estimator object implementing 'fit' and 'predict'
        The object to use to fit the data.
    X : pandas.DataFrame
        The data to fit.
    y : array-like, optional, default: None
        The target variable to try to predict in the case of
        supervised learning.
    groups : array-like, with shape (n_samples,), optional
        Group labels for the samples used while splitting the dataset into
        train/test set.
    cv : int, cross-validation generator or an iterable, optional
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:
        - None, to use the default 3-fold cross validation,
        - integer, to specify the number of folds in a `(Stratified)KFold`,
        - An object to be used as a cross-validation generator.
        - An iterable yielding train, test splits.
        For integer/None inputs, if the estimator is a classifier and ``y`` is
        either binary or multiclass, :class:`StratifiedKFold` is used. In all
        other cases, :class:`KFold` is used.
        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validation strategies that can be used here.
    n_jobs : integer, optional
        The number of CPUs to use to do the computation. -1 means
        'all CPUs'.
    verbose : integer, optional
        The verbosity level.
    fit_params : dict, optional
        Parameters to pass to the fit method of the estimator.
    pre_dispatch : int, or string, optional
        Controls the number of jobs that get dispatched during parallel
        execution. Reducing this number can be useful to avoid an
        explosion of memory consumption when more jobs get dispatched
        than CPUs can process. This parameter can be:
            - None, in which case all the jobs are immediately
              created and spawned. Use this for lightweight and
              fast-running jobs, to avoid delays due to on-demand
              spawning of the jobs
            - An int, giving the exact number of total jobs that are
              spawned
            - A string, giving an expression as a function of n_jobs,
              as in '2*n_jobs'
    method : string, optional, default: 'predict'
        Invokes the passed method name of the passed estimator. For
        method='predict_proba', the columns correspond to the classes
        in sorted order.
    Returns
    -------
    predictions : pandas.Dataframe

    """
    X, y, groups = indexable(X, y, groups)

    cv = check_cv(cv, y, classifier=is_classifier(estimator))

    if method in ['decision_function', 'predict_proba', 'predict_log_proba']:
        le = LabelEncoder()
        y = le.fit_transform(y)

    # We clone the estimator to make sure that all the folds are
    # independent, and that it is pickle-able.
    parallel = Parallel(n_jobs=n_jobs, verbose=verbose, pre_dispatch=pre_dispatch)

    prediction_blocks = parallel(delayed(_fit_and_predict)(
        clone(estimator), X, y, train, test, verbose, fit_params, method)
        for train, test in cv.split(X, y, groups))

    # Concatenate the predictions

    result = pd.concat([pd.DataFrame({'pred': pred_block_i, 'test_indices': indices_i, 'fold': i})
                        for i, (pred_block_i, indices_i) in enumerate(prediction_blocks)])

    if not result['test_indices'].is_unique:
        raise ValueError('Test indices are not uniques')

    return result.set_index('test_indices').sort_index()
