# -*- coding: utf-8 -*-

import numpy as np
from sklearn.metrics import mean_squared_log_error, mean_absolute_error, mean_squared_error


def RMSE(y_true, y_pred):
    '''Root mean squared error'''
    return np.sqrt(mean_squared_error(y_true, y_pred))


def RMSLE(y_true, y_pred):
    '''Root mean squared logarithmic error'''
    return np.sqrt(mean_squared_log_error(y_true, y_pred))


def MAPE(y_true, y_pred):
    '''Mean absolute percentage error'''

    if not (y_true != 0).all():
        raise ValueError("Mean absolute percentage error cannot be used when "
                         "targets contain zero values.")

    return np.mean(np.abs((y_true - y_pred) / (y_true)))


def MedAPE(y_true, y_pred):
    '''Median absolute percentage error'''

    if not (y_true != 0).all():
        raise ValueError("Median absolute percentage error cannot be used when "
                         "targets contain zero values.")

    return np.median(np.abs((y_true - y_pred) / (y_true)))


def MAE(y_true, y_pred):
    '''Mean absolute error'''
    return mean_absolute_error(y_true, y_pred)
