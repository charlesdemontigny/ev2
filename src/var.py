#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simple lists of values and Dict of column names used in make_dataset
Created on Wed Nov 29 12:42:52 2017
tstmp: 2017-12-06 11:30am
@author: savardmi
"""
ignored_dataset_stumps = ['historique_evia', 'evia_dispo', 'permis', 'geo_code',
                          'ind_exclusion', 'codesgeneraux', 'immeuble', 'transaction']

ignored_columns = {'immeuble': ['valid_code_util',
                                'valid_etages'],
                   'transaction': ['BALANCE', 'HYPOTHEQUE_ANCIENNE', 'TPS',
                                   'TVQ', 'RISTOURNE', 'AVOCAT1',
                                   'REVENU_NET', 'REVENU_BRUT', 'TAXE_TOTAL',
                                   'RISTOURNE_TPS', 'RISTOURNE_TVQ',
                                   'INSTITUTIO', 'CONTREPARTIE', 'MUTATION',
                                   'IMPOSITION', 'ORIGINE', 'HYPOTHEQUE'],
                   'transaction_merged': ['BALANCE', 'HYPOTHEQUE_ANCIENNE', 'TPS',
                                          'TVQ', 'RISTOURNE', 'AVOCAT1',
                                          'REVENU_NET', 'REVENU_BRUT', 'TAXE_TOTAL',
                                          'RISTOURNE_TPS', 'RISTOURNE_TVQ',
                                          'INSTITUTIO', 'CONTREPARTIE', 'MUTATION',
                                          'IMPOSITION', 'ORIGINE', 'HYPOTHEQUE'],
                   'evaluation': ['Date_modif'],
                   "annotation_photo": [],
                   "geo_codes": [],
                   'immeuble_merged': ["PRO_LOT_SUBD", "PRO_ABREV_LOT_ORIG_TYPE",
                                       'valid_code_util', 'valid_etages']}

rename_columns = {'transaction_merged':
                      {'ID_UEF': "Home_ID",
                       'MONTANT': 'Montant',
                       'DT_TRAN': "Date_Tx",
                       'DATE_MODIFIED': 'Date_Modified',
                       'DATE_CREATED': 'Date_Created',
                       'TRA_EXCLUE': 'tx_exclue',
                       'PRO_SECTEUR': "Secteur",
                       'NUMERO': "Tx_ID",
                       'COMPTANT': 'Comptant',
                       'T_TYPE': "Tx_Type",
                       'TRS_NOM': "Tx_Type_Desc",
                       'TRA_IND_VENTE_MULTIPLE': "Vente_Multiple",
                       'ACL_VENTE_LIE': "Vente_Intra_Famille",
                       'CODE_VENTE_TAXABLE': 'CODE_VENTE_TAXABLE',
                       'DESC_VENTE_TAXABLE': 'DESC_VENTE_TAXABLE',
                       'ACL_TYPE_ORIGINAL': 'Vente_Type',
                       'CONSTRUCTION_NEUVE': 'Construction_Neuve',
                       'ACHAT_SANS_HYP': 'Achat_Sans_Hypot'},

                  'immeuble_merged':
                      {'PRO_NO_ID_UEF': "Home_ID",
                       'EVIO_DISPO': 'Immeuble_dans_Evia',
                       'PRO_POSTAL': "Code_Postal",
                       'PRO_NBLOGRES': 'Nb_Logements',
                       'PRO_D_REEL': "Annee_Const",
                       'PRO_CUBF': 'Code_Utilisation',
                       'CUBF_DESC': "Code_Utilisation_txt",
                       'PRO_CAT_BAT': 'Categorie_Batiment',
                       'PRO_DIV_CAD': 'Cadastre',
                       'PAC_NOM': "Paroisse",
                       'PRO_LOT_RENOVE': 'Lot_Renove',
                       'PRO_LOT_ORIG': "Lot_ID",
                       'PRO_LOT_SUPE': "Lot_Superficie",
                       'PRO_LOT_FRON': "Lot_Largeur",
                       'PRO_LOT_PROF': "Lot_Profondeur",
                       'PRO_MATRICULE': "Matricule",
                       "PRO_BSQ": "Ville_BSQ",
                       'MUN_NOM_MUNICIPALITE': "Ville",
                       'PRO_NBETAGES': "N_etages",
                       'PRO_AUTRES_LOC': "N_commerces",
                       'PRO_SUPERF_BAT': "Superficie",
                       'PRO_UM': "Unite_Aire_code",
                       'UM_DESC': "Unite_Aire",
                       'PRO_X': "Longitude",
                       'PRO_Y': "Lattitude",
                       'PRO_ARV_ID': "Arrondissement_ID",
                       'ARV_DESCRIPTION': "Arrondissement_Nom",
                       'PRO_DATE_CREATED': "Date_Creation_enreg",
                       'PRO_DATE_MODIFIED': "Date_Modif_enreg"},
                  "annotation_photo": {"IDUEF": "Home_ID"},
                  "geo_codes": {"GEO_ID_UEF": "Home_ID",
                                'GEO_X_LNG': "Longitude_Fresh",
                                'GEO_Y_LAT': "Lattitude_Fresh"
                                },
                  "evaluation": {"HEV_ID_UEF": "Home_ID",
                                 "HEV_VALEUR_T": "Eval_Valeur_Lot",
                                 "HEV_VALEUR_B": "Eval_Valeur_Batisse",
                                 "HEV_ANNEE_DEB": "Annee_Debut",
                                 "HEV_ANNEE_FIN": "Annee_Fin",
                                 "HEV_DATE_CREATED": "Date_Cree",
                                 "HEV_DATE_MODIFIED": "Date_modif"},
                  "permis": {'PCO_ID_UEF': "Home_ID",
                             'PCO_NUMERO': "Code_Permis",
                             'PCO_MONTANT': "Montant_Permis",
                             'PCO_DATE_EMISSION': "Date_Emission",
                             'PCO_DATE_CREATION': "Date_Cree",
                             'PCO_NEQ': "PCO_NEQ"},
                  "codesgeneraux": {"CGE_NUMERO_TABLE": "Table_ID",
                                    "CGE_CODE": "Code",
                                    "CGE_DESCRIPTION": "Desc",
                                    "CGE_DESCRIPTION_AN": "Desc_an",
                                    "CGE_DATE_CREATED": "Date_cree",
                                    "CGE_DATE_MODIFIED": "Date_modif"},
                  "historique_evia": {}
                  }


# Variables for make_dataset fix_dtypes()
numeric_cols = ['HOME_ID', 'MONTANT', 'LONGITUDE', 'LATTITUDE', 'TX_TYPE',
                'TX_EXCLUE', 'TX_ID', 'VENTE_MULTIPLE',
                'VILLE_BSQ', 'LOT_SUPERFICIE',
                'NB_LOGEMENTS', 'UNITE_AIRE_CODE', 'N_ETAGES',
                'ANNEE_CONST', 'SUPERFICIE', 'LOT_LARGEUR',
                'LOT_PROFONDEUR',
                'N_COMMERCES', 'ARRONDISSEMENT_ID',
                'CONSTRUCTION_NEUVE', 'VENTE_INTRA_FAMILLE',
                'THIRD_PREV_EVAL_VALEUR_BATISSE', 'THIRD_PREV_EVAL_VALEUR_LOT',
                'SECOND_PREV_EVAL_VALEUR_BATISSE', 'SECOND_PREV_EVAL_VALEUR_LOT',
                'FIRST_PREV_EVAL_VALEUR_BATISSE', 'FIRST_PREV_EVAL_VALEUR_LOT',
                'CUR_EVAL_VALEUR_BATISSE', 'CUR_EVAL_VALEUR_LOT',
                'VENTE_INTRA_FAMILLE']

categorical_cols = ['ARRONDISSEMENT_NOM', 'TX_EXCLUE',
                    'VILLE', 'CODE_POSTAL',
                    'CATEGORIE_BATIMENT', 'UNITE_AIRE']

datetime_cols = {'DATE_TX':'%Y%m%d'}
