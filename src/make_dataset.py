# -*- coding: utf-8 -*-

# Import packages --------------------------------------------------------------
import pandas as pd
import numpy as np
import cx_Oracle
import os
from pathlib import Path
from datetime import datetime

from var import numeric_cols, categorical_cols, datetime_cols

# helper function --------------------------------------------------------------
def create_output_path(version = 'full_path'):
    import datetime

    # Create variables
    year = str(datetime.datetime.now().year)
    month = str(datetime.datetime.now().month)
    day = str(datetime.datetime.now().day)

    if len(month) == 1:
        month = "0" + month
    if len(day) == 1:
        day = "0" + day

    date = year + "-" + month + "-" + day

    if version == "full_path":
        return "./data/processed/{}/".format(Path(date).name)
    if version == "date":
        return date
    if version == "input":
        return "".join(['data/processed/', date, "/input/features_", date, ".feather"])
    if version == "model":
        return "".join(["models/", date])


def load_data_from_Oracle(connection, dataset, sampling_prob = None):
    """
    Load the dataset from the Oracle database and return data as pandas DataFrame.

    Args:
        connection: A list with
                                - user in position 0
                                - password in position 1
                                - database in position 2
        dataset: choice between input_train and input_pred
        sampling_prob: E[0, 1] sample of the main dataset
    """
    # Setup connection
    conn = cx_Oracle.connect(connection[0], connection[1], connection[2], encoding = 'utf-8')

    # Create query
    if dataset not in ['input_train', 'input_pred', 'geo']:
        raise ValueError("dataset must be either ``input_train``, ``input_pred`` or ``geo``.")

    if dataset is 'input_train':
        select_from = """
                select JLR_EVIA_DATASET.*, tra_mnt_transaction as prec_price,
                to_number(to_char(to_date(DATE_TX, 'yyyymmdd'), 'yyyy')) - to_number(to_char(tra_dt_publie, 'yyyy')) as prec_delta_date
                from JLR_EVIA_DATASET
                """
        options ="""
                left join tra_transaction on prec_no = tra_numero
                where Categorie_Batiment in ('2A', '3C', '2B', '2C')
                and TX_TYPE = 4
                and tx_exclue = 0
                """
        if sampling_prob is not None:
            sample = "sample({})".format(int(sampling_prob * 100))
            query = "\n".join([select_from, sample, options])
        else:
            query = "\n".join([select_from, options])

    if dataset is 'input_pred':
        select_from = """
                select JLR_EVIA_PREDINPUT.*, tra_mnt_transaction as prec_price,
                to_number(to_char(to_date(DATE_TX, 'yyyy-mm-dd'), 'yyyy')) - to_number(to_char(tra_dt_publie, 'yyyy')) as prec_delta_date
                from JLR_EVIA_PREDINPUT
                """
        options = """
                left join tra_transaction on prec_no = tra_numero
                where Categorie_Batiment in ('2A', '3C', '2B', '2C')
                """
        if sampling_prob is not None:
            sample = "sample({})".format(int(sampling_prob * 100))
            query = "\n".join([select_from, sample, options])
        else:
            query = "\n".join([select_from, options])

    if dataset is 'geo':
        query = """
                select IMM_NO_ID_UEF, IMM_CAT_BAT, MUN_NO_BSQ, DRIDU, MUN_NO_RA
                from imm_immeuble
                left join municipalites_statcan_bsq on imm_bsq = mun_no_bsq
                where imm_cat_bat in ('2A', '2B', '2C', '3C')
                """

    # Load dataset into df
    df = pd.read_sql(query.encode('utf-8'), con = conn)

    return df


def fix_dtypes(df, numeric_cols, categorical_cols, datetime_cols):
    """
    For each columns in numeric, categorical and datetime fix the dtype to be the
    one wanted.

    Args:
        df: A pandas DataFrame that contains the dataset.
        *_cols: A list of columns refering to a dtype.

    Raises:
        KeyError: All element in *_cols must be in columns.
    """
    not_in_df = []
    for list in [numeric_cols, categorical_cols, datetime_cols]:
        for col in list:
            if col not in df.columns:
                not_in_df.append(col)
    if len(not_in_df) > 0:
        raise KeyError("The variable(s) {} is not in the DataFrame.".format(not_in_df))

        # if any([col not in df.columns for col in list]):
        #     raise KeyError("A variable in *_cols is not in the pandas DataFrame columns.")

    df[numeric_cols] = df[numeric_cols].apply(pd.to_numeric, errors = 'raise')
    df[categorical_cols] = df[categorical_cols].apply(pd.Categorical)

    for col, date_format in datetime_cols.items():
        print(f'converting: {col}')
        if col in df.columns:
            df[col] = pd.to_datetime(df[col], format=date_format)

    return df


# make_dataset function --------------------------------------------------------
def make_dataset(dataset, connection, sampling_prob = None):
    """
    Load data from Oracle, preprocess them and write train and test set.

    Args:
        connection: A list with
                                - user in position 0
                                - password in position 1
                                - database in position 2
        dataset: choice between input_train and input_pred
        sampling_prob: E[0, 1] sample of the main dataset

    Returns: A pandas DataFrame.
    """

    # Load data ----------------------------------------------------------------
    df = load_data_from_Oracle(connection, dataset, sampling_prob)

    # Return directly geographic dataset ---------------------------------------
    if dataset is 'geo':
        return df

    # Preprocess ---------------------------------------------------------------
    print("Preprocess dataset...")
    # preprocess_transaction
    imputer = {'VENTE_INTRA_FAMILLE':-1,'VENTE_MULTIPLE':-1}
    df.fillna(value = imputer, inplace = True)
    df['ARRONDISSEMENT_ID'].fillna(value=pd.np.nan, inplace = True)

    # Fix le dtype -------------------------------------------------------------
    print("Modification of dtypes...")
    df = fix_dtypes(df, numeric_cols, categorical_cols, datetime_cols)

    # Create input_pred DataFrame ----------------------------------------------
    if dataset == 'input_pred':
        target_date = datetime.now()

        df = df.sort_values(['HOME_ID', 'DATE_TX'])
        df.drop_duplicates('HOME_ID', keep = 'last', inplace = True)

        df['DATE_TX'] = target_date
        df['TX_ID'] = -1
        df['VENTE_MULTIPLE'] = 0
        df['CODE_VENTE_TAXABLE'] = 0
        df['DESC_VENTE_TAXABLE'] = "NE S'APPLIQUE PAS"
        df['CONSTRUCTION_NEUVE'] = np.nan
        df['SECTEUR'] = np.nan

    # Train test split ---------------------------------------------------------
    print("DataFrame returned...")
    return df


if __name__ == "__main__":
    make_dataset()
